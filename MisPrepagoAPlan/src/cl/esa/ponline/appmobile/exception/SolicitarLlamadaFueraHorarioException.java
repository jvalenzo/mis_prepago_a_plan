package cl.esa.ponline.appmobile.exception;

/**
 * Created by everis on 26-02-2018.
 */
public class SolicitarLlamadaFueraHorarioException extends Exception {
    public SolicitarLlamadaFueraHorarioException(String mensaje) {
        super(mensaje);
    }
}
