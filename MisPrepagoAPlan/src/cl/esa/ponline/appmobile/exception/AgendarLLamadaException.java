package cl.esa.ponline.appmobile.exception;

/**
 * Created by everis on 23-02-2018.
 */
public class AgendarLLamadaException extends Exception {

    public AgendarLLamadaException(String mensaje) {
        super(mensaje);
    }
}
