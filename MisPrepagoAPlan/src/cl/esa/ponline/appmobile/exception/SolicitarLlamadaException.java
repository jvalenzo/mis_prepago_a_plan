package cl.esa.ponline.appmobile.exception;

public class SolicitarLlamadaException extends Exception{

    public SolicitarLlamadaException(String mensaje) {
        super(mensaje);
    }
}