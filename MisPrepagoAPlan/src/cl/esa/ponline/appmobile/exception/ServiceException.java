package cl.esa.ponline.appmobile.exception;

import org.apache.log4j.Logger;


public class ServiceException extends Exception {

    private Logger LOGGER = Logger.getLogger(ServiceException.class);

    private Exception error;

    public ServiceException() {

        super();
        this.error = new Exception("Ha ocurrido una Excepcion de llamada al servicios");
    }

    public ServiceException(Throwable throwable) {
        super(throwable);
        this.error = new Exception("Ha ocurrido una Excepcion de llama a servicios");
    }

    /**
     * Registrar un error interno de la ejecucion del proceso
     * @param mensaje
     * @param code
     */
    public ServiceException(String code, String mensaje) {
        super(mensaje);
        this.error = new Exception(mensaje);
        LOGGER.error("Codigo: " + code + " " + mensaje);
    }

    /**
     * Registrar un error interno de la ejecucion del proceso
     * @param mensaje
     */
    public ServiceException(String mensaje) {
        super(mensaje);
    }

    public Exception getError() {
        return error;
    }

    public void setError(Exception error) {
        this.error = error;
    }


}
