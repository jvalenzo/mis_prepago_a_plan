package cl.esa.ponline.appmobile.exception;

public class ConsultarHorarioAtencionException extends Exception{

    public ConsultarHorarioAtencionException(String mensaje) {
        super(mensaje);
    }
}