package cl.esa.ponline.appmobile.delegate;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.tempuri.ArrayOfCalle;
import org.tempuri.ArrayOfComuna;
import org.tempuri.ArrayOfDireccion;
import org.tempuri.ArrayOfRegion;
import com.epcs.clientes.evaluacion.types.EvaluacionComercialNecResponseType;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.Operator;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.ConsultaFactibilidadPortVO;
import com.esa.ponline.appmobile.web.bean.DataEmailVO;
import com.esa.ponline.appmobile.web.bean.DatosComercialesVO;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.esa.ponline.appmobile.web.bean.ReqConsultaTitularDeudaSyncVO;
import com.esa.ponline.appmobile.web.bean.ReqGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.ReqValidarDatosPortabilidadVO;
import com.esa.ponline.appmobile.web.bean.ResGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.ResTicketSGAVO;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosClientesSIMMAUTVO;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosPortabilidadVO;
import com.esa.ponline.appmobile.web.bean.RespuestaC2CType;
import com.esa.ponline.appmobile.web.bean.RespuestaGenerarCapVO;
import com.esa.ponline.appmobile.web.bean.RespuestaType;
import com.esa.ponline.appmobile.web.bean.TicketSGAVO;
import com.esa.ponline.appmobile.web.bean.ValidaDatosClientesVO;
import com.esa.ponline.appmobile.ws.FactoryWSDAO;

import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.RespuestaConsultaFactPortacionSync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.RespuestaConsultaTitularDeudaSync;

public class DelegateServices {

	public EvaluacionComercialNecResponseType obtenerEvaluacionComercial(String nRut, String dRut, String nserie, DatosComercialesVO datosComercialCliente)throws Exception{
		return FactoryWSDAO.getServices().obtenerEvaluacionComercial(nRut, dRut, nserie, datosComercialCliente);
	}	
	public ResValidarDatosClientesSIMMAUTVO validarDatosClientesSimAut(ValidaDatosClientesVO validaDatosClientes)throws Exception{
		return FactoryWSDAO.getServices().validarDatosClientesSimAut(validaDatosClientes);
	}	
	public RespuestaConsultaTitularDeudaSync consultaTitularDeudaSyncService(ReqConsultaTitularDeudaSyncVO parametrosVO)throws Exception{
		return FactoryWSDAO.getServices().consultaTitularDeudaSyncService(parametrosVO);
	}	
	public RespuestaConsultaFactPortacionSync consultaFactibilidadPortabilidad(ConsultaFactibilidadPortVO consultaFactibilidadPort)throws Exception{
		return FactoryWSDAO.getServices().consultaFactibilidadPortabilidad(consultaFactibilidadPort);
	}
	public Operator obtieneCompania(String msisdnFull) throws Exception{
		return FactoryWSDAO.getServices().obtieneCompania(msisdnFull);
	}
	public RespuestaGenerarCapVO generarCap(String ANI, String idd) throws Exception{
		return FactoryWSDAO.getServices().generarCap(ANI, idd);
	}
	public ResGeneraCapSuscripcionVO generarCapSuscripcion(ReqGeneraCapSuscripcionVO datosCapS) throws Exception{
		return FactoryWSDAO.getServices().generarCapSuscripcion(datosCapS);
	}	
	public ResValidarDatosPortabilidadVO validarDatosPortabilidad(ReqValidarDatosPortabilidadVO datosPortabilidad) throws Exception{
		return FactoryWSDAO.getServices().validarDatosPortabilidad(datosPortabilidad);
	}
	public String enviaEmail(String to, String from, String subject, String body, List<DataEmailVO> adjuntos) throws Exception{
		return FactoryWSDAO.getServices().enviaEmail(to, from, subject, body, adjuntos);
	}
	public ResTicketSGAVO ticketSGA(TicketSGAVO ticket) throws Exception{
		return FactoryWSDAO.getServices().ticketSGA(ticket);
	}
	public ArrayOfRegion obtenerListaRegiones()throws Exception{
		return  FactoryWSDAO.getServices().obtenerListaRegiones();
	}
	public ArrayOfComuna obtenerListaComunas(String idRegion)throws Exception{
		return  FactoryWSDAO.getServices().obtenerListaComunas(idRegion);
	}
	public ArrayOfCalle obtenerListaCalles(String idRegion, String idComuna, String strcalle)throws Exception{
		return  FactoryWSDAO.getServices().obtenerListaCalles(idRegion, idComuna, strcalle);
	}
	public ArrayOfDireccion obtenerListaDirecciones(String idCalle)throws Exception{
		return  FactoryWSDAO.getServices().obtenerListaDireccion(idCalle);
	}
	
	public DatosComercialesVO obtenerDatosCliente(String rut) throws Exception{
		return FactoryWSDAO.getServices().obtenerDatosCliente(rut);	
	}
	
	public Boolean isClienteTDE(String rut)throws Exception{
		try{
			JSONObject obj = FactoryWSDAO.getServices().obtenerEstadoTDE(rut.toUpperCase());
			JSONArray arrayCustomerAccount = obj.getJSONArray("CustomerAccount");
			for (int i=0; i<arrayCustomerAccount.length(); i++) 
			{ 
				JSONObject respArray = arrayCustomerAccount.optJSONObject(i);
				JSONObject billingAccount = respArray.getJSONObject("BillingAccount");
				JSONObject contract = respArray.getJSONObject("Asset").getJSONObject("Contract");
				
				String status = billingAccount.getString("status");
				String planType=contract.getString("planType");
				if( (planType.equals("CC")||planType.equals("SS")) && (status.equals("MIG") || status.equals("ACT") || status.equals("WIP") ) ){
					return true;
				}
			}
		}catch(Exception e){
			//Cliente nuevo
			return false;
		}
		return false;
	}
	
	public Boolean consultarHorarioC2C()throws Exception{
		return FactoryWSDAO.getServices().getHorarioAtencion();
	}
	
	public RespuestaC2CType SolicitarLlamadaC2C(Cliente clienteType, String numContact, Plan plan)throws Exception{
		return  FactoryWSDAO.getServices().solicitarLlamado(clienteType, numContact, plan);
	}
	
	public RespuestaType validarNumeroSerie(Cliente clienteType)throws Exception{
		return  FactoryWSDAO.getServices().validarSerie(clienteType);
	}
}