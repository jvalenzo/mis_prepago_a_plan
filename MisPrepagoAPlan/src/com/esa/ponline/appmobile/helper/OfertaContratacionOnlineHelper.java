package com.esa.ponline.appmobile.helper;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.utils.FormatoMovilUtil;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.actions.Constantes;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

public class OfertaContratacionOnlineHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4875225230868593954L;
	private static final Logger LOGGER      = Logger.getLogger(OfertaContratacionOnlineHelper.class);
	
	public static List<String> cargarTerminosyDebesSaber(String idTermino, HashMap<String, String> hashMapTerminos, String keyString){
		LOGGER.info(Utils.cabeceraLog() + "cargarTerminosyDebesSaber >>> Execute");
		LOGGER.info(Utils.cabeceraLog() + "Tamanio del debes Saber padre >>> "+ hashMapTerminos.size());

		HashMap<String, String> hasgMapRespuesta = new LinkedHashMap<String, String>();
	    //Iteramos solo sobre los elementos key4
		LOGGER.info(Utils.cabeceraLog() + "Key para busqueda de terminos >>> "+ idTermino+keyString);

		boolean iterar = true;
		int i = 0;
		//String key : map.keySet()
		while (iterar) {
			String key = idTermino+keyString+i;
			if(null != hashMapTerminos.get(key)){
				hasgMapRespuesta.put(key,hashMapTerminos.get(key));
			}else{
				iterar=false;
			}
			i++;
		}
	    
	    List<String> list = new ArrayList<String>(hasgMapRespuesta.values());
	    LOGGER.info(Utils.cabeceraLog() + "Tamanioo de TerminosyDebesSaber respuesta >>> "+ hasgMapRespuesta.size());
		return list;
	}
	
	public static void obtenerDetallePlan(Plan planCO, String idPlan) throws Exception{
		String cargoDcto="";
		String porcentajeDescuentoEspecifico="";
		boolean descuentoEspecifico=false;
		String infoPlan = Config.getAppProperty("plan." + idPlan);
		List datosPlan;
		
		if(null != infoPlan){
			datosPlan = Arrays.asList(Config.getAppProperty("plan." + idPlan).split("\\|"));
		}else{
			LOGGER.info("Error al leer parametros desde la URL");
			throw new Exception("Error al leer parametros desde la URL");
		}
		
		String codigoPlan = datosPlan.get(Constantes.DETALLE_PLAN_CODIGO).toString();
		String nombrePlan2 = datosPlan.get(Constantes.DETALLE_PLAN_NOMBRE).toString();
		String datos = datosPlan.get(Constantes.DETALLE_PLAN_DATOS).toString();
		String redesSociales = datosPlan.get(Constantes.DETALLE_PLAN_RRSS).toString();
		String minutos = datosPlan.get(Constantes.DETALLE_PLAN_MINUTOS).toString();
		String sms = datosPlan.get(Constantes.DETALLE_PLAN_SMS).toString();
		String cargoFijo = datosPlan.get(Constantes.DETALLE_PLAN_CARGO_FIJO).toString();
		String cargoFijoOferta = datosPlan.get(Constantes.DETALLE_PLAN_CARGO_FIJO_OFERTA).toString();
		boolean tieneDescuentoOnline= datosPlan.get(Constantes.DETALLE_PLAN_DESCUENTO_ONLINE).toString().equals("1")?true:false;
		if(planCO.isBuscarDescuentoEspecifico()){
			descuentoEspecifico = datosPlan.get(Constantes.DETALLE_PLAN_DESCUENTO_ESPECIFICO).toString().equals("1")?true:false;
			porcentajeDescuentoEspecifico = datosPlan.get(Constantes.DETALLE_PLAN_PORCENTAJE_DESCUENTO).toString();
		}
		
		
		
		planCO.getParamURL().setIdPlan("P"+codigoPlan);
		planCO.setIdPlan("P"+codigoPlan);
		
		
		/*Se obtiene el nombre del plan para mostrar en la vista*/
		String nombrePlan = Constantes.DETALLE_PLAN +" ";
		if(StringUtils.isNumeric(datos)){
			if(nombrePlan2.equals("")){
				nombrePlan += datos + " GB";
			}else{
				nombrePlan += nombrePlan2+" "+datos + " GB";
			}
    	}else{
    		nombrePlan += datos;
    	}
    	
		/*Se obtiene los sms para mostrar en la vista*/
		if(!sms.equals("")){
			sms = !NumberUtils.isNumber(sms) ? "SMS Libres" : FormatoMovilUtil.formatoTextoDinero(Double.valueOf(sms)) + " SMS";
		}
		
		
		
		SimpleDateFormat fechaVigenciaFormat = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy", new Locale("es","ES"));
		String str_fechaVigencia = Config.getAppProperty("vigencia.descuento.online");
		GregorianCalendar fechaVigencia = (GregorianCalendar) Calendar.getInstance();
		GregorianCalendar fechaActual = (GregorianCalendar) Calendar.getInstance();

        //Se convierte la fecha obtenida desde el properties a tipo date
        try {
            fechaVigencia.setTime(fechaVigenciaFormat.parse(str_fechaVigencia));
        } catch (ParseException e) {
            LOGGER.error("Error al parsear fecha vigencia oferta");
        }
        
        if(fechaActual.after(fechaVigencia)){
        	str_fechaVigencia = Config.getAppProperty("vigencia.descuento.online.new");;
        	fechaVigencia.setTime(fechaVigenciaFormat.parse(str_fechaVigencia));
        }
        
        fechaVigencia.add(Calendar.DAY_OF_MONTH, 1);
        //Si la fecha actual es igual a la vigencia + 1 d�a se oculta la oferta
        if(fechaActual.after(fechaVigencia)){
        	planCO.setMostrarOferta(false);
        }
        
        String descuentoOnline="0";
        String duracionOferta="0";
        if(tieneDescuentoOnline){
        	/*Se obtiene descuento online del plan*/
            descuentoOnline = Config.getAppProperty("descuento.contratacion.online.default");
            duracionOferta = Config.getAppProperty("duracion.descuento.contratacion.online.default");
            if(planCO.isMostrarOferta()){
            	descuentoOnline = Config.getAppProperty("descuento.contratacion.online");
            	duracionOferta = Config.getAppProperty("duracion.descuento.contratacion.online");
            }	
        }
        
        //Si tiene Oferta en el cargo fijo aplicamos el descuento a este valor
        if(Double.valueOf(cargoFijoOferta)>0){
        	cargoDcto = calculaDescuento(cargoFijoOferta, descuentoOnline);
		}else{
			cargoDcto = calculaDescuento(cargoFijo, descuentoOnline);
		}
        
        Double valorDescuentoOnline = (Double.valueOf(cargoFijo)*Double.valueOf(descuentoOnline))/100;
        
						
    	planCO.setNombrePlanCompleto(nombrePlan);
		planCO.setDatosPlan(datos);
		planCO.setRrssPlan(redesSociales);
		planCO.setSmsPlan(sms);
		planCO.setMinutosPlan(minutos);
		
		planCO.setCargoPlan(FormatoMovilUtil.formatoTextoDinero(Double.valueOf(cargoFijo)));
		planCO.setCargoPlanOferta(FormatoMovilUtil.formatoTextoDinero(Double.valueOf(cargoFijoOferta)));
		//Si tiene una oferta calculamos el descuento en $
		if(Double.valueOf(cargoFijoOferta)>0){
			Double descuentoCargoFijoOferta=Double.valueOf(cargoFijo)-Double.valueOf(cargoFijoOferta);
			planCO.setDescuentoPlanOferta(FormatoMovilUtil.formatoTextoDinero(Double.valueOf(descuentoCargoFijoOferta)));
		}
		planCO.setActionErrorC2C("$('#modalSeleccionFlujo').modal('show');");
		planCO.setCargoDcto(FormatoMovilUtil.formatoTextoDinero(Double.valueOf(cargoDcto)));
		
		if(tieneDescuentoOnline){
			planCO.setValorDelDescuento(FormatoMovilUtil.formatoTextoDinero(valorDescuentoOnline));
			planCO.setTieneDescuento(valorDescuentoOnline.intValue() > 0);
		}
		
		planCO.setCodigoPlan(codigoPlan);
		planCO.setNombrePlan2(nombrePlan2);
		planCO.setDuracionOferta(duracionOferta);
		planCO.setVigenciaOferta(str_fechaVigencia);
		planCO.setPorcentajeDsctoOferta(Integer.parseInt(descuentoOnline));
		
		planCO.setTieneDescuentoEspecifico(descuentoEspecifico);
	
		if(descuentoEspecifico){
			planCO.setPorcentajeDsctoEspecifico(Integer.parseInt(porcentajeDescuentoEspecifico));
			String descuentoTotal=String.valueOf(Integer.parseInt(descuentoOnline)+planCO.getPorcentajeDsctoEspecifico());
			Double valorDescuentoEspecifico=Double.valueOf((Integer.parseInt(cargoFijo)*planCO.getPorcentajeDsctoEspecifico())/100);
			planCO.setValorDctoEspecifico(FormatoMovilUtil.formatoTextoDinero(valorDescuentoEspecifico));
			cargoDcto = calculaDescuento(cargoFijo, descuentoTotal);
			planCO.setCargoDcto(FormatoMovilUtil.formatoTextoDinero(Double.valueOf(cargoDcto)));
			if(planCO.getTieneDescuento()){
				
				planCO.setCargoPlanMeses(FormatoMovilUtil.formatoTextoDinero(Double.valueOf(cargoFijo)-valorDescuentoEspecifico));
			}
		}
	}
	
	public static String calculaDescuento(String cargoFijo, String dcto){
		Double cargoDcto = null;
		//Se consulta descuento para planes $9990, $19990, $21990 y $25990
		if(Arrays.asList(Config.getAppProperty("Planes.SinDescuento").split(",")).contains(cargoFijo)){
		    cargoDcto = Double.valueOf(cargoFijo);
		} else {
			cargoDcto = Double.valueOf(cargoFijo) - (Double.valueOf(cargoFijo) * Integer.parseInt(dcto)/100);
		}		
		int cargo = cargoDcto.intValue();
		return String.valueOf(cargo);
	}
	
}
