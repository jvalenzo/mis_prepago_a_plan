package com.esa.ponline.appmobile.interfaces;


import java.text.ParseException;
import java.util.List;

import org.json.JSONObject;
import org.tempuri.ArrayOfCalle;
import org.tempuri.ArrayOfComuna;
import org.tempuri.ArrayOfDireccion;
import org.tempuri.ArrayOfRegion;
import com.epcs.clientes.evaluacion.types.EvaluacionComercialNecResponseType;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.Operator;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.ConsultaFactibilidadPortVO;
import com.esa.ponline.appmobile.web.bean.DataEmailVO;
import com.esa.ponline.appmobile.web.bean.DatosComercialesVO;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.esa.ponline.appmobile.web.bean.ReqConsultaTitularDeudaSyncVO;
import com.esa.ponline.appmobile.web.bean.ReqGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.ReqValidarDatosPortabilidadVO;
import com.esa.ponline.appmobile.web.bean.ResGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.ResTicketSGAVO;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosClientesSIMMAUTVO;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosPortabilidadVO;
import com.esa.ponline.appmobile.web.bean.RespuestaC2CType;
import com.esa.ponline.appmobile.web.bean.RespuestaGenerarCapVO;
import com.esa.ponline.appmobile.web.bean.RespuestaType;
import com.esa.ponline.appmobile.web.bean.TicketSGAVO;
import com.esa.ponline.appmobile.web.bean.ValidaDatosClientesVO;
import com.esa.ponline.appmobile.ws.EntelServicesLocatorException;

import cl.esa.ponline.appmobile.exception.AgendarLLamadaException;
import cl.esa.ponline.appmobile.exception.ConsultarHorarioAtencionException;
import cl.esa.ponline.appmobile.exception.ServiceException;
import cl.esa.ponline.appmobile.exception.SolicitarLlamadaException;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.RespuestaConsultaFactPortacionSync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.RespuestaConsultaTitularDeudaSync;

public interface IServicesInterfaces {
	
	EvaluacionComercialNecResponseType obtenerEvaluacionComercial(String nRut, String dRut, String nserie, DatosComercialesVO datosComercialCliente);
	ResValidarDatosClientesSIMMAUTVO validarDatosClientesSimAut(ValidaDatosClientesVO validaDatosClientes) throws Exception;
	RespuestaConsultaTitularDeudaSync consultaTitularDeudaSyncService(ReqConsultaTitularDeudaSyncVO parametrosVO);
	RespuestaConsultaFactPortacionSync consultaFactibilidadPortabilidad(ConsultaFactibilidadPortVO consultaFactibilidadPort) throws Exception;
	Operator obtieneCompania(String msisdnFull) throws EntelServicesLocatorException, Exception;
	RespuestaGenerarCapVO generarCap(String ANI, String idd) throws Exception;
	ResGeneraCapSuscripcionVO generarCapSuscripcion(ReqGeneraCapSuscripcionVO datosCapS) throws Exception;
	ResValidarDatosPortabilidadVO validarDatosPortabilidad(ReqValidarDatosPortabilidadVO datosPortabilidad) throws Exception;
	ResTicketSGAVO ticketSGA(TicketSGAVO ticket) throws Exception;
	String enviaEmail(String to, String from, String subject, String body, List<DataEmailVO> adjuntos);
	ArrayOfRegion obtenerListaRegiones();
	ArrayOfComuna obtenerListaComunas(String idRegion);
	ArrayOfCalle  obtenerListaCalles(String idRegion, String idComuna, String strcalle);
	ArrayOfDireccion obtenerListaDireccion(String idCalle);
	DatosComercialesVO obtenerDatosCliente(String rut) throws Exception;
	JSONObject obtenerEstadoTDE(String rut)throws Exception;
	Boolean getHorarioAtencion() throws ServiceException, ConsultarHorarioAtencionException, ParseException , AgendarLLamadaException;
    RespuestaC2CType solicitarLlamado(Cliente clienteType, String numContact, Plan plan)throws  ServiceException, SolicitarLlamadaException, ConsultarHorarioAtencionException, AgendarLLamadaException, Exception;
    RespuestaType validarSerie(Cliente clienteType)throws  ServiceException, Exception;
}
