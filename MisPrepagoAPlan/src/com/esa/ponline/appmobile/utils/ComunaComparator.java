package com.esa.ponline.appmobile.utils;

import java.util.Comparator;

import org.tempuri.Comuna;

public class ComunaComparator implements Comparator<Comuna>{

	@Override
	public int compare(Comuna c1, Comuna c2) {
		
		return c1.getNombreComuna().compareTo(c2.getNombreComuna());
	}
	
}