package com.esa.ponline.appmobile.utils;

import java.util.Map;

import javax.xml.ws.BindingProvider;

import com.sun.xml.internal.ws.client.BindingProviderProperties;
import com.sun.xml.internal.ws.developer.JAXWSProperties;

public class ServiceUtils {
	public static void setServiceTimeout(Object soapInterface) {
		Map<String, Object> requestContext = ((BindingProvider)soapInterface).getRequestContext();
		requestContext.put(JAXWSProperties.CONNECT_TIMEOUT, 1000 * 5);
		requestContext.put(JAXWSProperties.REQUEST_TIMEOUT, 1000 * 5);
	}
}
