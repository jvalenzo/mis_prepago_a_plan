package com.esa.ponline.appmobile.utils;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.tempuri.Region;

public class RegionComparator implements Comparator<Region>{

	List<String> predefinedOrder = Arrays.asList("13","15","1","2","3","4","5","6","7","16","8","9","14","10","11","12","14","15");
	
	@Override
	public int compare(Region r1, Region r2) {
		
		return predefinedOrder.indexOf(r1.getID()) - predefinedOrder.indexOf(r2.getID());
	}
	

}
