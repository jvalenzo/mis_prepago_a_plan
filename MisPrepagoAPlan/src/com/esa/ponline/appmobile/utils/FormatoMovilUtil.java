package com.esa.ponline.appmobile.utils;

import org.apache.log4j.Logger;

public class FormatoMovilUtil {
	private static final long serialVersionUID = 301700699833719557L;
	private static final Logger LOGGER = Logger.getLogger(FormatoMovilUtil.class);
	private static final String CODIGOPAIS = "56";
	private static final String CODIGOMOVIL = "9";
	public static final String FORMAT_yyyyMMdd_HHmmss = "yyyyMMdd HH:mm:ss";
	/**
	 * Este metodo pasa cualquier n�mero recibido a 9
	 * @param movil
	 * @param servicio
	 * @return
	 */
	public static String obtieneFormatoMovilPorLargo(String movil) {
		String movilFormateado = "";
		try{
			switch (movil.length()) {
			case 8:
					movilFormateado = CODIGOPAIS + CODIGOMOVIL + movil;
				break;
			case 9:
				movilFormateado = CODIGOPAIS + movil;
				break;
			case 11:
				movilFormateado = movil;

				break;

			default:
				break;
			}

		} catch (Exception e) {
			LOGGER.error("Ha ocurrido un problema en FormatMovilUtil.obtieneFormatoMovilPorLargo() "+ e);
		}

		return movilFormateado;
	}
	
    public static String formatoTextoDinero(Double valor) {
        java.text.DecimalFormat myFormatter = new java.text.DecimalFormat(
                "###,###.###");
        
        
        String ret = myFormatter.format(valor).replace(",", ".");
        System.out.println("Resultado = "+ ret);
        return ret;
    }
    

}
