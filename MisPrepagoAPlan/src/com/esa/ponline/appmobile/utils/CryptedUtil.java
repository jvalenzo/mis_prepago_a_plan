package com.esa.ponline.appmobile.utils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import org.apache.commons.codec.binary.Hex;

public class CryptedUtil {
	static Cipher cipher;
	
/*		public static String enCrypt(String plainText, SecretKey secretKey) throws NoSuchAlgorithmException, NoSuchPaddingException {
			byte[] plainTextByte = plainText.getBytes();
			
			try {
				cipher = Cipher.getInstance("AES");
				cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}
			
			byte[] encryptedByte = null;
			try {
				encryptedByte = cipher.doFinal(plainTextByte);
			} catch (IllegalBlockSizeException e) {
				e.printStackTrace();
			} catch (BadPaddingException e) {
				e.printStackTrace();
			}
			
			char[] encryptedHex = Hex.encodeHex(encryptedByte);
			String encryptedText = new String(encryptedHex);
			return encryptedText;
		}*/
		
	    private static String deCrypt(String encryptedText, SecretKey secretKey) throws Exception {
	    	byte[] encryptedTextByte = Hex.decodeHex(encryptedText.toCharArray());
	    	cipher.init(Cipher.DECRYPT_MODE, secretKey);
	    	byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
	    	String decryptedText = new String(decryptedByte);
	    	return decryptedText;
	    }
	    
/*	    private static String GeneraURLPublica(String nombrePlan, String datosPlan, String RRSS, String minutosPlan, String cargoFijo, String paso) throws NoSuchAlgorithmException, NoSuchPaddingException{
			int ambiente = 1;
			String host;
			String url = "";
			
	    	//Nombre=MIPLAN_CONTROLADO&Datos=10&RRSS=5&Minutos=200&CargoFijo=25990&paso=1A
			String keyString = "C0ntr4t4c10n0nl1";
			SecretKey secretKey = new SecretKeySpec(keyString.getBytes(),"AES");
	    
			String nombrePlan_	= enCrypt(nombrePlan, secretKey);
			String datosPlan_	= enCrypt(datosPlan, secretKey);
			String RRSS_ 		= enCrypt(RRSS, secretKey);
			String minutosPlan_ = enCrypt(minutosPlan, secretKey);
			String cargoFijo_	= enCrypt(cargoFijo, secretKey);
			String paso_ 		= enCrypt(paso, secretKey);
			

			switch (ambiente) {
			case 0: //PROD
				host = "http://appswls.entel.cl";
				url = "/ContratacionOnLineMovil/paso1?Nombre="+ nombrePlan_ +"&Datos="+ datosPlan_ 
						+"&RRSS="+ RRSS_ +"&Minutos="+minutosPlan_+"&CargoFijo="+ cargoFijo_ +"&paso="+ paso_;
				System.out.println(host.concat(url));
				break;
			case 1: //PRE
				host = "http://appswlspre.entel.cl";
				url = "/ContratacionOnLineMovil/paso1?Nombre="+ nombrePlan_ +"&Datos="+ datosPlan_ 
						+"&RRSS="+ RRSS_ +"&Minutos="+minutosPlan_+"&CargoFijo="+ cargoFijo_ +"&paso="+ paso_;
				System.out.println(host.concat(url));
				break;
			case 2: //LOCALHOST
				host = "http://localhost:7001";
				url = "/ContratacionOnLineMovil/paso1?Nombre="+ nombrePlan_ +"&Datos="+ datosPlan_ 
						+"&RRSS="+ RRSS_ +"&Minutos="+minutosPlan_+"&CargoFijo="+ cargoFijo_ +"&paso="+ paso_;
				System.out.println(host.concat(url));
				break;
			default:
				break;
			}
			return url;
			
	    }*/
		
		public static void main(String[] args) throws Exception {
			String nombrePlan_	= "PLAN 35GB";
			String datosPlan_	= "35";
			String RRSS_ 		= "7";
			String minutosPlan_ = "350";
			String cargoFijo_	= "25990";
			String paso_ 		= "1A";
			
//			System.out.println(GeneraURLPublica(nombrePlan_, datosPlan_, RRSS_, minutosPlan_, cargoFijo_, paso_));
		}
}
