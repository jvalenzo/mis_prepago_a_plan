package com.esa.ponline.appmobile.utils;

import java.util.Map;

import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.opensymphony.xwork2.ActionContext;

public class PruebaUtil {
	private static Plan planCO;
	private static Cliente clienteCO;
	public static void creaDatosPrueba(){
		clienteCO = new Cliente();
		planCO = new Plan();
		
		clienteCO.setRut("16912953-3");
		clienteCO.setnSerie("A017699551");
		clienteCO.setEmail("ROBINSON.TOLEDO@VALORUNICO.CL");
		clienteCO.setTelefono("983105103");
		clienteCO.setNombre("JAIME ALEJANDRO");
		clienteCO.setApellidos("CABELLO VENEGAS");
		clienteCO.setNumeroNegocio("74500454");
		clienteCO.setNumeroPorta("983105103");
		//Parametros en sesion con nombre Cliente
		getSession().put("Cliente", clienteCO);
		
		planCO.setDatosPlan("8");
		planCO.setRrssPlan("5");
		planCO.setMinutosPlan("350");
		planCO.setCargoPlan("25990");
		planCO.setCargoDcto("23391");
		//Se deja en sesion el objeto PLAN
		
		getSession().put("Plan", planCO);
	}
	
	private static Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}
}
