package com.esa.ponline.appmobile.utils;

import static com.esa.ponline.appmobile.web.actions.Constantes.CODIGO_PLAN;
import static com.esa.ponline.appmobile.web.actions.Constantes.NOMBRE_PLAN;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.http.HttpSession;

import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.web.actions.Constantes;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.opensymphony.xwork2.ActionContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.codec.binary.Base64;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Utils {

	private static final int PREPAGO = 0;
	private static boolean planesCargados=false;
	private static Map<String,Integer> codigoPlanes=new HashMap<String,Integer>();
	private static Map<String,Integer> codigoPlanesPortabilidad=new HashMap<String,Integer>();
	
	public static Boolean comprobarGrupoCliente23(Cliente clienteCO, Plan plan, String grupoCliente){
		String cargoFijoSinFormat = plan.getCargoPlan().replaceAll("\\.", "");
		
		if(Arrays.asList(Config.getAppProperty("planes.multi").split(",")).contains(cargoFijoSinFormat)){
			
			if(null != clienteCO.getMercadoOrigen() &&  clienteCO.getMercadoOrigen() == PREPAGO){
				//se cambia grupop cliente en caso de ser una portabilidad de prepago a postpago
				clienteCO.setGrupoCliente("21");
				return true;
			}
			
			List datosPlan = Arrays.asList(Config.getAppProperty("plan." + cargoFijoSinFormat + ".multi").split("\\|"));
			
			plan.setCodigoPlan(datosPlan.get(CODIGO_PLAN).toString());
			plan.setNombrePlan2(datosPlan.get(NOMBRE_PLAN).toString());
			plan.setEsMiPrimerPlan(true);
			getSession().put("Plan", plan);
			
			clienteCO.setGrupoCliente(grupoCliente);
			return true;
		}else{
			//Se muestra mismo error que lineas no disponibles.
			plan.setIconoErrorC2C("error_fatal.svg");
			plan.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.sinLineasDispopnibles"));
			plan.setTipoError(Config.getAppProperty("url.tipo.error.sinLineasDispopnibles"));
        	plan.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
        	plan.setActionErrorC2C(Constantes.TIENDA.toString());
		}
		return false;
	}
	
	public static String obtenerKeyCaptcha(Boolean esProduccion){
		getSession().put("esProduccion", esProduccion);
		if(esProduccion){
			//PROD
			return "6LdzIG4UAAAAAAZKwEo0te51VCJTYSjnCFzuc2NT";
		}else{
			//TEST
			return "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe";
		}
	}
	
	public static boolean validarRecaptcha(Boolean esProduccion, String recapchaResponse, String secret) throws Exception{
	
		if(esProduccion){
			//PROD	
			return recapcha(recapchaResponse, secret);
		}else{
			//TEST
			return true;
		}
	}
		
	private static Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}
	
	public static boolean recapcha(String gRecaptchaResponse, String secret) throws Exception{
		boolean valida = false;
		HttpURLConnection conn = null;
		BufferedReader reader = null;
		try{
			if (gRecaptchaResponse != null || !"".equals(gRecaptchaResponse)) {

				String urlString = "https://www.google.com/recaptcha/api/siteverify?secret="+secret+"&response="+gRecaptchaResponse; 
				URL url = new URL(null, urlString, new sun.net.www.protocol.https.Handler());
				//URL url = new URL(urlString);
				conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				String line, outputString = "";
				reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	            while ((line = reader.readLine()) != null) {
	                outputString += line;
	            }
	            conn.disconnect();
		        reader.close();	           
				//parsea respuesta Json para sucess
				JsonReader jsonReader = Json.createReader(new StringReader(outputString));
				JsonObject jsonObject = jsonReader.readObject();
				jsonReader.close();
				
				valida = jsonObject.getBoolean("success");
			}
		}catch(Exception e){
			if(conn != null)
				conn.disconnect();
	        if(reader != null)
	        	reader.close();	
			
			return valida;
		}
		
		return valida;
	}
	
	public static Boolean validarDatosCliente(String indicadorValidacionCliente, Plan planCO){
		Boolean validaDatos = false;
		if(indicadorValidacionCliente.equals("O-000")){
    		validaDatos = true;
    	}else {    
    		if(indicadorValidacionCliente.equals("N-21199") || indicadorValidacionCliente.equals("N-21006")){
        		//error tiene deuda entel
    			planCO.setIconoErrorC2C("error_fatal.svg");
	        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.comercial"));
	        	planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.subMensajeComercial"));
	        	planCO.setTipoError(Config.getAppProperty("url.tipo.error.comercial"));
	        	planCO.setBtnErrorC2C("Conocer la m&aacute;s cercana");
	        	planCO.setActionErrorC2C(Constantes.TIENDA.toString());
	        	
    		}else if(indicadorValidacionCliente.equals("N-21063")){
    			//error no tiene lineas disponibles
    			planCO.setIconoErrorC2C("error_fatal.svg");
	        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.sinLineasDispopnibles"));
	        	planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.subMensajeComercial"));
	        	planCO.setTipoError(Config.getAppProperty("url.tipo.error.sinLineasDispopnibles"));
	        	planCO.setBtnErrorC2C("Conocer la m&aacute;s cercana");
	        	planCO.setActionErrorC2C(Constantes.TIENDA.toString());
    			
    		}else{
	        	//error al validar la serie
    			planCO.setIconoErrorC2C("error_confuso.svg");
	        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.validacionSerie"));
	        	planCO.setSubMensajeErrorC2C(Config.getAppProperty(""));
	        	planCO.setTipoError(Config.getAppProperty("url.tipo.error.validacionSerie"));
	        	planCO.setBtnErrorC2C("Entendido");
	        	planCO.setActionErrorC2C("location.href='"+planCO.getUrlOrigen()+"'");
    		}
        }
		return validaDatos;
	}
	
	public static String capitalizaString(String strStringACapitalizar){
	    	
	    	String strPalabraResultado = "";
	    	Boolean booPalabras = false;
	    	String strPrimeraLetra = "";
	    	String strRestoPalabra = "";
	    	
	    	String strPalabras[] = strStringACapitalizar.split(" ");
	    	
	        
	    	for (String element : strPalabras){
	
	            if (stringIsNullOrSpace(element) == false ){
	            	
	            	if (booPalabras){
	            		strPalabraResultado = strPalabraResultado + " ";	
	            	}
	
	                if (strPalabras.length > 1){
	                	booPalabras = true;
	                }            	
	            	
	                strPrimeraLetra = element.substring(0,1);
	                strRestoPalabra = element.substring(1);
	                strPrimeraLetra = strPrimeraLetra.toUpperCase();
	
	                if (stringIsNullOrSpace(strRestoPalabra) == false ){
	                	strRestoPalabra = strRestoPalabra.toLowerCase();
	                }
	
	                strPalabraResultado = strPalabraResultado + strPrimeraLetra+strRestoPalabra;
	
	                strPalabraResultado = strPalabraResultado.trim();
	
	            }    		
	    		
	    	}
	
	    	strPrimeraLetra = null;
	    	strRestoPalabra = null;
	    	
	        return strPalabraResultado;
	    }    	
	
	static public boolean stringIsNullOrSpace(String strTextoOriginal){

        if (stringIsNull(strTextoOriginal) == true ){
            return true;
        }

        if (stringIsTextNull(strTextoOriginal) == true ){
            return true;
        }

        return stringIsSpace(strTextoOriginal) == true;

    }
	
	static public boolean stringIsSpace(String strTextoOriginal){

        if (stringIsNull(strTextoOriginal) == true){
            return true;
        }

        strTextoOriginal = strTextoOriginal.trim();

        return strTextoOriginal.length() == 0;

    }
	
	static public boolean stringIsNull(String strTexto){

        if (strTexto == null){
            return true;
        }

        return stringIsTextNull(strTexto) == true;

    }

    static public boolean stringIsTextNull(String strTexto){

        return strTexto.compareTo("null") == 0;

    }
    
    static public String FormatearRUT(String rut) {
    	 
        int cont = 0;
        String format;
        rut = rut.replace(".", "");
        rut = rut.replace("-", "");
        format = "-" + rut.substring(rut.length() - 1);
        for (int i = rut.length() - 2; i >= 0; i--) {
            format = rut.substring(i, i + 1) + format;
            cont++;
            if (cont == 3 && i != 0) {
                format = "." + format;
                cont = 0;
            }
        }
        return format;
    }
    
    static public String cabeceraLog(){
    	String cabeceraLog = "[Rut:@rut|Plan:@plan] ";
    	try {
    		Plan planCO = (Plan)getSession().get("Plan");
        	Cliente clienteCO = (Cliente) getSession().get("Cliente");
        	
        	
        	try {   		
        		cabeceraLog = cabeceraLog.replace("@rut", clienteCO.getRut());
    		} catch (Exception e) {
    			cabeceraLog = cabeceraLog.replace("@rut", "");
    		}
        	
        	try {
        		cabeceraLog = cabeceraLog.replace("@plan", planCO.getCargoPlan());
    		} catch (Exception e) {
    			cabeceraLog = cabeceraLog.replace("@plan", "");
    		}
		} catch (Exception e) {
			cabeceraLog = cabeceraLog.replace("@plan", "");
			cabeceraLog = cabeceraLog.replace("@rut", "");
		}
    	
    	
    	return cabeceraLog;
    }
    
    static public String obtenerDetalleError(Exception e){
    	String detalleError = "";
    	try {
    		detalleError = " Motivo: " + e.getMessage();
    		detalleError += " Class: " + e.getStackTrace()[0].getClassName();
    		detalleError += " Linea: " + e.getStackTrace()[0].getLineNumber();
    		return detalleError;
		} catch (Exception e2) {
			return detalleError;
		}
    }
    
    static public String reemplazarCaracteresExtranos(String cadena){
    	cadena = cadena.replaceAll("[+^:<>,;*#$]","");
    	//cadena = cadena.replaceAll("[~`!@#$%^&*()+={}|[]:;<>,?]","");    
    	return cadena;
    }
    
    static public String reemplazarCaracteresExtranosEmail(String cadena){
    	cadena = cadena.replaceAll("[~`!#$%^&*()+={}|:;<>,?]","");
    	return cadena;
    }
    
    static public String primeraLetraMayus(String str){
        String retorno="";
        String[] palabras=str.split(" ");
        for (String palabra:palabras) {
            retorno+=" "+palabra.substring(0,1).toUpperCase()+palabra.substring(1).toLowerCase();
        }
        return retorno.trim();
    }
    
	    /*
	    Test AB
	    Para elegir Test AB de cada plan en razon 1:1
	    iniciarContadorPlanes() -> inicializa el contador en 0 para todos los planes consumidos desde propertie
	    incrementarContador();
	    seleccionarTest();
	 */
	public static void iniciarContadorPlanes(){
	        if(!planesCargados){
	            List codigosCC = Arrays.asList(Config.getAppProperty("planes.movil").split(","));
	            for (Object codigo : codigosCC) {
	                codigoPlanes.put(codigo.toString(),0);
	                codigoPlanesPortabilidad.put(codigo.toString(),0);
	            }
	            planesCargados=true;
	        }
	}
	
	public static int incrementarContador(String codigoPlan){
	    int contador=codigoPlanes.get(codigoPlan)+1;
	    codigoPlanes.put(codigoPlan,contador);
	    return codigoPlanes.get(codigoPlan);
	}
	public static int incrementarContadorPortabilidad(String codigoPlan){
	    int contador=codigoPlanesPortabilidad.get(codigoPlan)+1;
	    codigoPlanesPortabilidad.put(codigoPlan,contador);
	    return codigoPlanesPortabilidad.get(codigoPlan);
	}

	public static String seleccionarTest(int contador){
	    if(Config.getAppProperty("test.activo").equals("")){
	        if(contador%2!=0){
	            return "A";//Original
	        }else{
	            return "B";//Paso 1 sin rut ni telefono -> Se agregan al paso 3
	        }
	    }else{
	        return Config.getAppProperty("test.activo");
	    }
	}
	
	public static String numeroInbound(String origen){
		String palabras[]=origen.toLowerCase().split("\\_");
		String inboundOrigen=Config.getAppProperty(Constantes.NUMERO_INBOUND_ORIGEN.concat(palabras[0]).toString());
		String  inboundDefecto =Config.getAppProperty(Constantes.NUMERO_INBOUND_DEFECTO.toString());
		String numero=""; 
		if(inboundOrigen!=null)
		{	
			numero=inboundOrigen;
		}else {
			numero=inboundDefecto;						
		}
	return 	numero;
	}
	
	public static String[] parseToken( String token )
	{
		DateTime fechaRecepcion = new LocalDateTime().toDateTime();
		String desencriptado = "FAIL_FAIL";
		
		byte[] base64 = Base64.decodeBase64( token.getBytes() );
		String strBase64 = new String( base64 );	
		String decriptParam = Config.getAppProperty(Constantes.CLAVE_DESENCRIPTAR);
		desencriptado = ArcFour.getInstance().desencriptar( strBase64, decriptParam );
		String[] paramsToken = desencriptado.split("_");

		if(validateTimeStamp(fechaRecepcion,paramsToken[2])){
			return paramsToken;
		}else{
			return null;
		}	
	}
	
	public static Boolean validateTimeStamp( DateTime fechaRecepcion, String fechaTimeStamp )
	{
		String fmtFecha = "yyyy-MM-dd' 'HH:mm:ss";

		int segundos = 0;
		
		DateTime fechaEnviada;
		DateTime fechaRecepcionUTC;
		
		try
		{
			int limiteSegundos = Integer.parseInt(Config.getAppProperty(Constantes.DURACION_TOKEN));
			
			DateTimeFormatter dtP = DateTimeFormat.forPattern( fmtFecha );
						
			DateTime fechaEnviadaParsed = dtP.parseDateTime( fechaTimeStamp );
			fechaEnviada		= fechaEnviadaParsed.withZoneRetainFields( DateTimeZone.UTC );

			fechaRecepcionUTC	= fechaRecepcion.toDateTime( DateTimeZone.UTC );
			
			Seconds segs = Seconds.secondsBetween( fechaEnviada, fechaRecepcionUTC );
			
			segundos = segs.getSeconds();
			
			if ( segundos > limiteSegundos )
			{
				return false;
			}
			else if ( segundos < 0 )
			{
				return false;
			}
		}
		catch ( Exception e )
		{
			return false;
		}
		
		return true;
	}
	
	public static int diferenciaDiasVSActual(String fechaVencimiento){
		int dias=0;
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			Date fechaActual= new Date();
			Date fechaFinal=dateFormat.parse(fechaVencimiento);
			dias=(int) ((fechaActual.getTime()-fechaFinal.getTime())/86400000);
		}catch(Exception e){
			dias=-999;
		}
		return dias;
	}
	
	public static int diferenciaAniosVSActual(String fechaVencimiento){
		int anios=0;
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			Date fechaActual= new Date();
			Date fechaFinal=dateFormat.parse(fechaVencimiento);
			anios=(int) ((fechaActual.getTime()-fechaFinal.getTime())/31557600000L);
		}catch(Exception e){
			anios=-999;
		}
		return anios;
	}
	
	public static Boolean ajustesDesuentosEspecificos(String urlK){
		if(Config.getAppProperty(Constantes.OFERTA_ESPECIFICA_URL_CONTRATAR).equals(urlK)){		
			getSession().put("OfertaEspecifica", Config.getAppProperty(Constantes.OFERTA_ESPECIFICA_MENSAJE_CONTRATAR));
			return true;
		}else if(Config.getAppProperty(Constantes.OFERTA_ESPECIFICA_URL_CAMBIAR).equals(urlK)){
			getSession().put("OfertaEspecifica", Config.getAppProperty(Constantes.OFERTA_ESPECIFICA_MENSAJE_CAMBIAR));
			return true;
		}
		return false;
	}
	
	public static String urlOriginal(Plan planCo){
		if(null==getSession().get("activarC2C")){
			return "https://appswls.entel.cl/ContratacionOnLineMovil/nl_paso1?origen="+planCo.getOrigenOferta()+"&idPlan="+planCo.getIdPlan()+"&paso=1A";
		}else{
			return "https://appswls.entel.cl/ContratacionOnLineMovil/nl_paso1?origen="+planCo.getOrigenOferta()+"&idPlan="+planCo.getIdPlan()+"&paso=1A&c2=1";
		}
	}
	
	
	public static String secretKeyCaptcha(){
        String retorno="";
        if(Boolean.valueOf(Config.getAppProperty(Constantes.IS_PRODUCCION))){
            retorno=Config.getAppProperty(Constantes.CAPTCHA_SECRET_PRO);
        }else{
            retorno=Config.getAppProperty(Constantes.CAPTCHA_SECRET_LOCAL);
        }
        return retorno;
    }
	
	public static String siteKeyCaptcha(){
        String retorno="";
        if(Boolean.valueOf(Config.getAppProperty(Constantes.IS_PRODUCCION))){
            retorno=Config.getAppProperty(Constantes.CAPTCHA_SITE_PRO);
        }else{
            retorno=Config.getAppProperty(Constantes.CAPTCHA_SITE_LOCAL);
        }
        return retorno;
    }
	
    static public StringBuilder anteponeCeroAMesDiaHoraMinuto(StringBuilder sblString){
    	return new StringBuilder( (sblString.length() < 2) ? sblString.insert(0, 0) : sblString);    	
    }
    
    static public String getFechaHoraMinutoSistema(){

    	Calendar calFechaHoraMinuto = Calendar.getInstance();
    	calFechaHoraMinuto.setTimeZone(TimeZone.getTimeZone("America/Santiago"));

        StringBuilder sblFecha = new StringBuilder(calFechaHoraMinuto.get(Calendar.YEAR)+"");
        StringBuilder sblMes = new StringBuilder((calFechaHoraMinuto.get(Calendar.MONTH)+1)+"");
        StringBuilder sblDia = new StringBuilder(calFechaHoraMinuto.get(Calendar.DATE)+"");
        StringBuilder sblHora = new StringBuilder(calFechaHoraMinuto.get(Calendar.HOUR_OF_DAY)+"");
        StringBuilder sblMinuto = new StringBuilder(calFechaHoraMinuto.get(Calendar.MINUTE)+"");

        sblMinuto = anteponeCeroAMesDiaHoraMinuto(sblMinuto);
        sblHora = anteponeCeroAMesDiaHoraMinuto(sblHora);
        sblDia = anteponeCeroAMesDiaHoraMinuto(sblDia);
        sblMes = anteponeCeroAMesDiaHoraMinuto(sblMes);
        
        sblFecha = new StringBuilder(sblHora.append(":").append(sblMinuto.append(" ").append(sblDia.append("/").append(sblMes).append("/").append(sblFecha))));

        sblMes = null;
        sblDia = null;
        sblHora = null;
        sblMinuto = null;        
        
        return sblFecha.toString();

    }
    
    static public String getFechaFormateada(){
    	String patron = "dd-MM-yyyy HH:mm";
    	SimpleDateFormat formato = new SimpleDateFormat(patron);
    	String fechaFormateada = formato.format(new Date());
    	fechaFormateada = fechaFormateada.replaceAll("\\s+","");
        return fechaFormateada;
    }


	static public String getSubOperacion(String origen){
    	String key=Constantes.SUBOPERACION_SGA_ECOMMERCE_DEFECTO;
		if(!origen.equals("")){
			List origenesPrivado = Arrays.asList(Config.getAppProperty(Constantes.SUBOPERACION_SGA_ECOMMERCE_ORIGENES_PRIVADO).split("\\|"));
			if(origenesPrivado.size()>0){
				for (Object origenPropertie : origenesPrivado) {
	            	if(origen.toLowerCase().contains(origenPropertie.toString())){
	            		key=Constantes.SUBOPERACION_SGA_ECOMMERCE_PRIVADO;
	            		break;
	            	}
	            }
			}
		}	
		return Config.getAppProperty(key);
    }
    
    public static HttpSession activacionC2C(HttpSession session,String c2){
       boolean activarC2C = false;
       boolean inactividadC2C = false;


        try{
            //Separador de flujo
            if(String.valueOf(c2.charAt(0)).equals("1")){
            	activarC2C = true;
            }
            //Ayuda
            if(String.valueOf(c2.charAt(1)).equals("1")){
            	inactividadC2C = true;
            }
        }catch (Exception e){
     
        }
        
        session.setAttribute("activarC2C", activarC2C);
        session.setAttribute("inactividadC2C", inactividadC2C);

        return session;
    }
}
