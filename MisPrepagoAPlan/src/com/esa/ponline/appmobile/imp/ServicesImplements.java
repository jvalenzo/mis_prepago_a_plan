package com.esa.ponline.appmobile.imp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.tempuri.ArrayOfCalle;
import org.tempuri.ArrayOfComuna;
import org.tempuri.ArrayOfDireccion;
import org.tempuri.ArrayOfRegion;
import org.tempuri.ObtenerListaCallesPt;
import org.tempuri.ObtenerListaCallesService;
import org.tempuri.ObtenerListaComunasPt;
import org.tempuri.ObtenerListaComunasService;
import org.tempuri.ObtenerListaDireccionesPt;
import org.tempuri.ObtenerListaDireccionesService;
import org.tempuri.ObtenerListaRegionesPt;
import org.tempuri.ObtenerListaRegionesService;
import org.tempuri.Usuario;

import com.epcs.clientes.evaluacion.EvaluacionComercialNecFaultMessage;
import com.epcs.clientes.evaluacion.EvaluacionComercialNecPortType;
import com.epcs.clientes.evaluacion.EvaluacionComercialNecService;
import com.epcs.clientes.evaluacion.types.EvaluacionComercialNecRequestType;
import com.epcs.clientes.evaluacion.types.EvaluacionComercialNecResponseType;
import com.epcs.integracionit.flujotrabajo.EnviarMailService;
import com.epcs.integracionit.flujotrabajo.beans.ListMailBeanLiteral;
import com.epcs.integracionit.flujotrabajo.beans.MailBean;
import com.esa.billing.billingacco.t.consultardatosfacturacioncliente.ConsultarDatosFacturacionCliente;
import com.esa.billing.billingacco.t.consultardatosfacturacioncliente.request.ConsultarDatosFacturacionClienteRequestType;
import com.esa.billing.billingacco.t.consultardatosfacturacioncliente.request.RequestType;
import com.esa.billing.billingacco.t.consultardatosfacturacioncliente.response.ConsultarDatosFacturacionClienteResponseType;
import com.esa.cliente.contacto.sga.TicketSGAService;
import com.esa.cliente.contacto.sga.types.CreaTicketSgaRequestType;
import com.esa.cliente.contacto.sga.types.CrearTicketSgaResponseType;
import com.esa.crm.crmcustomerinterface.t.consultarhorarioatencion.request.ConsultarHorarioAtencionReq;
import com.esa.crm.crmcustomerinterface.t.consultarhorarioatencion.response.ConsultarHorarioAtencionRes;
import com.esa.crm.crmcustomerinterface.t.registraraccionsolicitudllamada.request.RegistrarAccionSolicitudLlamadaReq;
import com.esa.crm.crmcustomerinterface.t.registraraccionsolicitudllamada.response.RegistrarAccionSolicitudLlamadaRes;
import com.esa.crm.crmloyalty.t.verificaseriecanalonline.request.VerificaSerieCanalOnlineReq;
import com.esa.crm.crmloyalty.t.verificaseriecanalonline.response.VerificaSerieCanalOnlineRes;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.Operator;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.ServiceException_Exception;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.ValidaMsisdn;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.ValidaMsisdnService;
import com.esa.marketsales.salesmassive.n.enviarcapinternosuscripcion.EnviarCapInternoSuscripcion;
import com.esa.marketsales.salesmassive.n.enviarcapinternosuscripcion.request.EnviarCapInternoSuscripcionRequest;
import com.esa.marketsales.salesmassive.n.enviarcapinternosuscripcion.response.EnviarCapInternoSuscripcionResponse;
import com.esa.marketsales.salesmassive.n.validardatosclientesimaut.ValidarDatosClienteSIMAUT;
import com.esa.marketsales.salesmassive.n.validardatosclientesimaut.ValidarDatosClienteSIMAUTFaultMessage;
import com.esa.marketsales.salesmassive.n.validardatosclientesimaut.request.ValidarDatosClienteSIMAUTRequest;
import com.esa.marketsales.salesmassive.n.validardatosclientesimaut.response.ValidarDatosClienteSIMAUTResponse;
import com.esa.marketsales.salesmassive.n.validardatosportabilidadsimaut.ValidarDatosPortabilidadSIMAUT;
import com.esa.marketsales.salesmassive.n.validardatosportabilidadsimaut.request.ValidarDatosPortabilidadSIMAUTRequest;
import com.esa.marketsales.salesmassive.n.validardatosportabilidadsimaut.response.ValidarDatosPortabilidadSIMAUTResponse;
import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.interfaces.IServicesInterfaces;
import com.esa.ponline.appmobile.utils.ServiceUtils;
import com.esa.ponline.appmobile.utils.TimeWatch;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.actions.Constantes;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.ConsultaFactibilidadPortVO;
import com.esa.ponline.appmobile.web.bean.DataEmailVO;
import com.esa.ponline.appmobile.web.bean.DatosComercialesVO;
import com.esa.ponline.appmobile.web.bean.HorarioAtencionType;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.esa.ponline.appmobile.web.bean.ReqConsultaTitularDeudaSyncVO;
import com.esa.ponline.appmobile.web.bean.ReqGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.ReqValidarDatosPortabilidadVO;
import com.esa.ponline.appmobile.web.bean.ResGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.ResTicketSGAVO;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosClientesSIMMAUTVO;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosPortabilidadVO;
import com.esa.ponline.appmobile.web.bean.RespuestaC2CType;
import com.esa.ponline.appmobile.web.bean.RespuestaGenerarCapVO;
import com.esa.ponline.appmobile.web.bean.RespuestaType;
import com.esa.ponline.appmobile.web.bean.TicketSGAVO;
import com.esa.ponline.appmobile.web.bean.ValidaDatosClientesVO;
import com.esa.ponline.appmobile.ws.EntelServices;
import com.esa.ponline.appmobile.ws.EntelServicesLocatorException;
import com.opensymphony.xwork2.ActionContext;
import cl.esa.ponline.appmobile.exception.AgendarLLamadaException;
import cl.esa.ponline.appmobile.exception.ConsultarHorarioAtencionException;
import cl.esa.ponline.appmobile.exception.ServiceException;
import cl.esa.ponline.appmobile.exception.SolicitarLlamadaException;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.NumeroTelefonoDetalle;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.ParamConsultaFactPortacionSync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.RespuestaConsultaFactPortacionSync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.SSGPConsultaFactPortacionSyncService;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.NumeroTelefono;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.ParamConsultaTitularDeudaSync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.RespuestaConsultaTitularDeudaSync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.SSGPConsultaTitularDeudaSyncException_Exception;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.SSGPConsultaTitularDeudaSyncService;
import cl.iecisa.gateway.ws.integrator.services.ssgp_solicitacapasync.ParamSolicitaCAPAsync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_solicitacapasync.RespuestaSolicitaCAPAsync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_solicitacapasync.SSGPSolicitaCAPAsyncService;

public class ServicesImplements implements IServicesInterfaces {
	private static final Logger LOGGER 	= Logger.getLogger(ServicesImplements.class);
	private static final Logger	logPerformance	= Logger.getLogger("Performance");
	private static String IDPAIS = "56";
	private boolean FUERA_DE_HORARIO;
	
	private static final String SERVICIO_VALIDA_SERIE_RETORNO_EXITOSO = "valida.serie.retorno.exitoso";
    private static final String SERVICIO_VALIDA_SERIE_RETORNO_FALLIDO = "valida.serie.retorno.fallido";
    private static final String SERVICIO_VALIDA_SERIE_RETORNO_BLOQUEADO = "valida.serie.retorno.boqueado";
	
	private static final String APPLICATION_CODE = "WEB-PUB";
	private static final String COUNTRY_CODE = "CHL";
	//private static final Logger log 	= Logger.getLogger("login");
	@Override
	public EvaluacionComercialNecResponseType obtenerEvaluacionComercial(String nRut, String dRut, String nserie ,DatosComercialesVO datosComercialCliente) {
		LOGGER.info(Utils.cabeceraLog() + "obtenerEvaluacionComercial");
		String codiTipoEvaluacion = "0";
		String plataforma = "FNL";
		EvaluacionComercialNecPortType port = null;
		EvaluacionComercialNecRequestType request;
		EvaluacionComercialNecResponseType response = null;
		try {
			LOGGER.info(Utils.cabeceraLog() + "Instanciamos locator --> Execute");
			EvaluacionComercialNecService locator = EntelServices.getEvaluacionComercial();
			LOGGER.info(Utils.cabeceraLog() + "Instanciamos el port --> Execute");
			TimeWatch watch = TimeWatch.start();
			port = locator.getEvaluacionComercialNecServicePort();
			LOGGER.info(Utils.cabeceraLog() + "Llenamos el request --> Execute");
			request = new EvaluacionComercialNecRequestType();
			request.setNrutCliente(nRut);
			request.setDrutCliente(dRut);
			request.setCodiTipoEvaluacion(codiTipoEvaluacion);
			request.setPlataforma(plataforma);
			request.setCodiInstitucion("");
			request.setCodiInstitucionSIBF("");
			request.setCodiPuntoVenta("");
			request.setCodiSerieRutCliente(nserie);
			request.setDescIPv4("");
			request.setDescNombreCliente("");
			request.setFlagCheque("");
			request.setMntoAcreditacion("");
			request.setNmroCheque("");
			request.setNmroCtaCte("");
			
			if(datosComercialCliente.isEsCliente()){
				request.setCodiGrupoCliente(datosComercialCliente.getGrupoCliente());
				request.setCodiTipoCliente(datosComercialCliente.getTipoCliente());
				request.setCodiSegmentoCliente(datosComercialCliente.getSegmentoCliente());
			}else{
				request.setCodiGrupoCliente("");
				request.setCodiTipoCliente("");
				request.setCodiSegmentoCliente("");
			}
			
			LOGGER.info(Utils.cabeceraLog() + "Se consulta el servicio --> Execute");
			response = new EvaluacionComercialNecResponseType();
			response = port.evaluacionComercial(request);
			logPerformance.info("[RUT "+nRut +"-"+dRut+"]|[ObtenerEvaluacionComercial]|[CODIGO "+response.getCodigoSalida()+"]|[TIEMPO "+watch.time()+"]");
			LOGGER.info(Utils.cabeceraLog() + "Respuesta Evaluacion Comercial Cliente TIEMPO: "+ watch.time());
			LOGGER.info(Utils.cabeceraLog() + "Respuesta Evaluacion Comercial Codigo Salida: " + response.getCodigoSalida());
			LOGGER.info(Utils.cabeceraLog() + "Respuesta Evaluacion Comercial Lineas disponibles: " + response.getNmroLineasDisponibles());	
			LOGGER.info(Utils.cabeceraLog() + "Respuesta Evaluacion Comercial Grupo cliente: " + response.getCodiGrupoCliente());	
		
		} catch (EntelServicesLocatorException e) {
			LOGGER.error("Error al consultar servicio Evaluacion Comercial: " + Utils.obtenerDetalleError(e) );
			e.printStackTrace();
		} catch (EvaluacionComercialNecFaultMessage e) {
			LOGGER.error("Error en la NEC al consultar servicio Evaluacion Comercial: " +  Utils.obtenerDetalleError(e) );
			e.printStackTrace();
		}
		
		return response;
	}
	@Override
	public ResValidarDatosClientesSIMMAUTVO validarDatosClientesSimAut(ValidaDatosClientesVO validaDatosClientes) throws Exception{
		ResValidarDatosClientesSIMMAUTVO res = new ResValidarDatosClientesSIMMAUTVO();
		ValidarDatosClienteSIMAUTRequest req = new ValidarDatosClienteSIMAUTRequest();
		req.setTipoNegocio(validaDatosClientes.getTipoNegocio());
		req.setNumeroMovilVirtual(validaDatosClientes.getNumeroMovilVirtual());
		req.setNRutCliente(validaDatosClientes.getRutClienteSinDV());
		req.setDRutCliente(validaDatosClientes.getDigitoVerificador());
		req.setCodPlataforma(validaDatosClientes.getCodPlataforma());
		req.setCodiSerieRutCliente(validaDatosClientes.getNumeroSerie());
		TimeWatch watch = TimeWatch.start();
		try{
			LOGGER.info(Utils.cabeceraLog() + "Inicio Valdiacion Datos Cliente");
			ValidarDatosClienteSIMAUT port = EntelServices.getValidaDatosClienteSIMAUT();
			res.setRespuestaValidarCliente( port.getValidarDatosClienteSIMAUTPort().validarDatosClienteSIMAUT(req) );
			logPerformance.info("[MOVIL "+validaDatosClientes.getMsisdn()+"]|[ValidarDatosClientesSimAut]|[CODIGO "+res.getRespuestaValidarCliente().getIndicadorExito()+"]|[TIEMPO "+watch.time()+"]");
			LOGGER.info(Utils.cabeceraLog() + "Request: [rut:"+ req.getNRutCliente()+"|Digito verificadr:"+ req.getDRutCliente() + "|N� Serie: "+req.getCodiSerieRutCliente()+
					"|Numero Movil Virtual: "+ req.getNumeroMovilVirtual()+"|Plataforma: "+ req.getCodPlataforma()+ "|Tipo Negocio: "+req.getTipoNegocio());
			LOGGER.info(Utils.cabeceraLog() + "Respuesta Valdiacion Datos Cliente TIEMPO: "+ watch.time());
			LOGGER.info(Utils.cabeceraLog() + "Respuesta Valdiacion Datos Cliente Codigo: " + res.getRespuestaValidarCliente().getIndicadorExito());
			LOGGER.info(Utils.cabeceraLog() + "Respuesta Valdiacion Datos Cliente Mensaje: " + res.getRespuestaValidarCliente().getDescription());	
			
		}
		catch(ValidarDatosClienteSIMAUTFaultMessage e){
			LOGGER.error("No es posible obtener el Servicio Valdiacion Datos Cliente",e);
			LOGGER.error("TIEMPO "+watch.time());
			logPerformance.error("[MOVIL "+validaDatosClientes.getMsisdn()+"]|[ValidarDatosClientesSimAut]|[TIEMPO "+watch.time()+"]");
			res.setRespuestaValidarCliente( new ValidarDatosClienteSIMAUTResponse() );
			res.setErrorValidarCliente( e.getFaultInfo() );
		}
		catch(Exception e){
			LOGGER.error("Error inesperado al obtener la Valdiacion Datos Cliente: "+ req.getNRutCliente() , e);
			LOGGER.error("TIEMPO "+watch.time());
			logPerformance.error("[MOVIL "+validaDatosClientes.getMsisdn()+"]|[ValidarDatosClientesSimAut]|[TIEMPO "+watch.time()+"]");
			throw new Exception(e);
		}
		return res;	
	}
	@Override
	public RespuestaConsultaTitularDeudaSync consultaTitularDeudaSyncService(ReqConsultaTitularDeudaSyncVO parametrosVO){
		
        RespuestaConsultaTitularDeudaSync res = new RespuestaConsultaTitularDeudaSync();
		ParamConsultaTitularDeudaSync req     = new ParamConsultaTitularDeudaSync();
        NumeroTelefono numeroTelefono         = new NumeroTelefono();

		numeroTelefono.setANI(parametrosVO.getMsisdn());
		
        req.setRutSolicitante(parametrosVO.getRutSolicitante());
		req.setTipoServicioTo(parametrosVO.getTipoServicioTo());
		req.setTipoServicio(parametrosVO.getTipoServicio());
		req.setRutTitular(parametrosVO.getRutTitular());
		req.setModalidad(parametrosVO.getModalidad());
		req.getListaAnis().add(numeroTelefono);
		req.setIdd(parametrosVO.getIdd());

		LOGGER.info(parametrosVO.getRutSolicitante());
		LOGGER.info(parametrosVO.getTipoServicioTo());
		LOGGER.info(parametrosVO.getTipoServicio());
		LOGGER.info(parametrosVO.getRutTitular());
		LOGGER.info(parametrosVO.getModalidad());
		LOGGER.info(numeroTelefono);
		LOGGER.info(parametrosVO.getIdd());
		
		TimeWatch watch = TimeWatch.start();
		
		try {
			LOGGER.info(Utils.cabeceraLog() + "Inicio ConsultaTitularDeuda");
			
			SSGPConsultaTitularDeudaSyncService servicioConsultaDeuda = EntelServices.getConsultaTitularDeudaSyncService();
			
			res = servicioConsultaDeuda.getSSGPConsultaTitularDeudaSyncPort().consultaTitularDeudaSync(req);
			
			logPerformance.info("[MOVIL "+parametrosVO.getMsisdn()+"]|[EliminarNegocioHeader]|[CODIGO "+res.getErrorCode()+"]|[TIEMPO "+watch.time()+"]");
			
            LOGGER.info(Utils.cabeceraLog() + "Respuesta ConsultaTitularDeuda TIEMPO: "          + watch.time());
            LOGGER.info(Utils.cabeceraLog() + "Respuesta ConsultaTitularDeuda Codigo: "          + res.getErrorCode());
            LOGGER.info(Utils.cabeceraLog() + "Respuesta ConsultaTitularDeuda Descripcion: "     + res.getErrorDescription());
            LOGGER.info(Utils.cabeceraLog() + "Respuesta ConsultaTitularDeuda Fecha REspuesta: " + res.getFechaRespuesta());
            LOGGER.info(Utils.cabeceraLog() + "Respuesta ConsultaTitularDeuda ID Consulta OAP: " + res.getIdConsultaOAP());
            LOGGER.info(Utils.cabeceraLog() + "Respuesta ConsultaTitularDeuda ID Consulta GW: "  + res.getIdConsultaGW());
            LOGGER.info(Utils.cabeceraLog() + "Respuesta ConsultaTitularDeuda Respuesta ANI: "   + res.getRespuestasANI());
            
		} catch (SSGPConsultaTitularDeudaSyncException_Exception e) {
			LOGGER.error("No es posible obtener el Consulta Titular Deuda",e);
			LOGGER.error("TIEMPO "+watch.time());
			logPerformance.error("[MOVIL "+parametrosVO.getMsisdn()+"]|[ConsultaTitularDeudaSync]|[TIEMPO "+watch.time()+"]");
		}
		
		return res;
	}
	@Override
	public RespuestaConsultaFactPortacionSync consultaFactibilidadPortabilidad(ConsultaFactibilidadPortVO consultaFactibilidadPort) throws Exception{
    RespuestaConsultaFactPortacionSync res = new RespuestaConsultaFactPortacionSync();
    ParamConsultaFactPortacionSync req     = new ParamConsultaFactPortacionSync();
	
	req.setIdd(consultaFactibilidadPort.getIdd());
	req.setTipoServicio(consultaFactibilidadPort.getTipoServicio());
	req.setTipoServicioTo(consultaFactibilidadPort.getTipoServicioTo());
	
	NumeroTelefonoDetalle numeroTelefonoDetalle = new NumeroTelefonoDetalle();
	
	numeroTelefonoDetalle.setANI(consultaFactibilidadPort.getAni());
	numeroTelefonoDetalle.setIMEI(consultaFactibilidadPort.getImei());
	
	req.getListaAnis().add(numeroTelefonoDetalle);
	
	TimeWatch watch = TimeWatch.start();
	
	try{
		LOGGER.info(Utils.cabeceraLog() + "Inicio Consulta Factibilidad Portabilidad");
        SSGPConsultaFactPortacionSyncService port = EntelServices.getConsultaFactPortacion();
        res                                       = port.getSSGPConsultaFactPortacionSyncPort().consultaFactPortacionSync(req);
        logPerformance.info("[MOVIL "+consultaFactibilidadPort.getMsisdn()+"]|[RespuestaConsultaFactPortacionSync]|[CODIGO "+res.getErrorCode()+"]|[TIEMPO "+watch.time()+"]");
        LOGGER.info(Utils.cabeceraLog() + "Respuesta Consulta Factibilidad Portabilidad TIEMPO: "  + watch.time());
        LOGGER.info(Utils.cabeceraLog() + "Respuesta Consulta Factibilidad Portabilidad Codigo: "  + res.getErrorCode());
        LOGGER.info(Utils.cabeceraLog() + "Respuesta Consulta Factibilidad Portabilidad Mensaje: " + res.getErrorDescription());
	}
	catch(Exception e){
		LOGGER.error("No es posible obtener el Servicio Consulta Factibilidad Portabilidad",e);
		LOGGER.error("TIEMPO "+watch.time());
		logPerformance.error("[MOVIL "+consultaFactibilidadPort.getMsisdn()+"]|[RespuestaConsultaFactPortacionSync]|[TIEMPO "+watch.time()+"]");
		throw new Exception(e.getMessage());
	}
	
	return res;	
}
	@Override
	public Operator obtieneCompania(String msisdnFull) throws Exception {
		Operator resCompania = new Operator();
		TimeWatch watch = TimeWatch.start();
		try {
			ValidaMsisdn port = EntelServices.validaCompania().getValidaMsisdnPort();
			ServiceUtils.setServiceTimeout(port);
			
			resCompania = port.validaMisdn(msisdnFull);
			msisdnFull = (null != msisdnFull) ? msisdnFull : "NULO";
			logPerformance.info("[MOVIL " + msisdnFull + "]|[validaMsisdnService.validaMisdn]|[TIEMPO " + watch.time() + "]");
			LOGGER.info(Utils.cabeceraLog() + "Respuesta Obtiene Compania TIEMPO: "+ watch.time());
			LOGGER.info(Utils.cabeceraLog() + "Respuesta Evaluacion Comercial Operador ID: " + resCompania.getOperatorId());
		} catch (ServiceException_Exception e) {
			LOGGER.error("Problema en servicio " + e.getMessage());
			msisdnFull = (null != msisdnFull) ? msisdnFull : "NULO";
			logPerformance.error("[MOVIL " + msisdnFull + "]|[validaMsisdnService.validaMisdn]|[TIEMPO " + watch.time() + "]|[ERROR" + e);
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + ValidaMsisdnService.class, e);
			msisdnFull = (null != msisdnFull) ? msisdnFull : "NULO";
			logPerformance.error("[MOVIL " + msisdnFull + "]|[validaMsisdnService.validaMisdn]|[TIEMPO " + watch.time() + "]|[ERROR" + e);
			throw new Exception(e.getMessage());
			
		}

		if (resCompania.getOperatorId() != null) {
			return resCompania;
		} else {
			return null;
		}
	}
	@Override
	public RespuestaGenerarCapVO generarCap(String ANI, String idd) throws Exception {
		ParamSolicitaCAPAsync req = new ParamSolicitaCAPAsync();
		RespuestaSolicitaCAPAsync res = new RespuestaSolicitaCAPAsync();
		RespuestaGenerarCapVO respuesta = new RespuestaGenerarCapVO();
				//se genera el request del servicio segun parametros entregados
				req.setANI(ANI);
				req.setIdd(idd);
				TimeWatch watch = TimeWatch.start();
				try{			
					LOGGER.info(Utils.cabeceraLog() + "***** LLAMADO A GENERA CAP  *****");
					SSGPSolicitaCAPAsyncService solicitaCap = EntelServices.generaCap();
					res = solicitaCap.getSSGPSolicitaCAPAsyncPort().solicitaCAPAsync(req);
					logPerformance.info("[lineaClase :"+31+"]|[nombreMetodo : solicitaCAPAsync]|[ANI "+ ANI +"]|[SSGPSolicitaCAPAsyncService]|[TIEMPO "+watch.time()+"]|");
					LOGGER.info(Utils.cabeceraLog() + "[ANI : "+ ANI +"]|[SSGPSolicitaCAPAsyncService][TIEMPO "+watch.time()+"]");
					respuesta.setErrorCode(String.valueOf(res.getErrorCode()));
					respuesta.setErrorDescription(res.getErrorDescription());
					respuesta.setIdSolicitudGW(res.getIdSolicitudGW());
					LOGGER.info(Utils.cabeceraLog() + "Respuesta Genera CAP TIEMPO: "  + watch.time());
			        LOGGER.info(Utils.cabeceraLog() + "Respuesta Genera CAP Codigo: "  + res.getErrorCode());
			        LOGGER.info(Utils.cabeceraLog() + "Respuesta Genera CAP Mensaje: " + res.getErrorDescription());
				}catch (Exception e) {
					 LOGGER.error("No es posible obtener el servicio Generar Cap "
							    + e.getMessage());				   
					 logPerformance.error("[ANI : "+ ANI +"]|[SSGPSolicitaCAPAsyncService][TIEMPO "+watch.time()+"]"+e);
					 throw new Exception(e.getMessage());
				}
				
	
		return respuesta;
	}
	@Override
	public ResGeneraCapSuscripcionVO generarCapSuscripcion(ReqGeneraCapSuscripcionVO datosCapS) throws Exception {
		EnviarCapInternoSuscripcionRequest req = new EnviarCapInternoSuscripcionRequest();
		EnviarCapInternoSuscripcionResponse res = new EnviarCapInternoSuscripcionResponse();
		//Se reutiliza esta clase ya que devuelve el mismo request que donde se implementa
		ResGeneraCapSuscripcionVO respuesta = new ResGeneraCapSuscripcionVO();
		req.setCodPlataforma(datosCapS.getCodPlataforma());
		req.setNumeroMovil(Long.valueOf(datosCapS.getNumeroMovil()));
		TimeWatch watch = TimeWatch.start();
	
		try{			
			LOGGER.info(Utils.cabeceraLog() + "***** LLAMADO A GENERA CAP SUSCRIPCION  *****");
			EnviarCapInternoSuscripcion generaCapSuscripcion = EntelServices.generaCapSuscripcion();
			res = generaCapSuscripcion.getEnviarCapInternoSuscripcionPort().enviarCapInternoSuscripcion(req);
			logPerformance.info("[lineaClase :"+97+"]|[nombreMetodo : enviarCapInternoSuscripcion]|[Numero Movil : "+ datosCapS.getNumeroMovil()+"]|[EnviarCapInternoSuscripcion]|[TIEMPO "+watch.time()+"]|");
			LOGGER.info(Utils.cabeceraLog() + "[Numero Movil  : "+ datosCapS.getNumeroMovil() +"]|[EnviarCapInternoSuscripcion][TIEMPO "+watch.time()+"]");
			respuesta.setDescripcion(res.getDescription());
			respuesta.setIndicadorExito(res.getIndicadorExito());
			LOGGER.info(Utils.cabeceraLog() + "Respuesta Genera CAP Suscripcion TIEMPO: "  + watch.time());
	        LOGGER.info(Utils.cabeceraLog() + "Respuesta Genera CAP Suscripcion Codigo: "  + res.getIndicadorExito());
	        LOGGER.info(Utils.cabeceraLog() + "Respuesta Genera CAP Suscripcion Mensaje: " + res.getDescription());
		}catch (Exception e) {
			 LOGGER.error("No es posible obtener el servicio Genera Cap Suscripcion "
					    + e.getMessage());				   
			 logPerformance.error("[Numero Movil : "+ datosCapS.getNumeroMovil() +"]|[EnviarCapInternoSuscripcion][TIEMPO "+watch.time()+"]"+e);
			 throw new Exception(e.getMessage());
		}
		return respuesta;
	}
	@Override
	public ResValidarDatosPortabilidadVO validarDatosPortabilidad(ReqValidarDatosPortabilidadVO datosPortabilidad) throws Exception {
		ValidarDatosPortabilidadSIMAUTRequest req  = new ValidarDatosPortabilidadSIMAUTRequest();
		ValidarDatosPortabilidadSIMAUTResponse res = new ValidarDatosPortabilidadSIMAUTResponse();
		ResValidarDatosPortabilidadVO respuesta    = new ResValidarDatosPortabilidadVO();
		TimeWatch watch                            = TimeWatch.start();

		req.setCap(datosPortabilidad.getCap());
		req.setCodMercadoOrigen(Integer.valueOf(datosPortabilidad.getCodMercadoOrigen()));
		req.setCodPlataforma(datosPortabilidad.getCodPlataforma());
		req.setImei(datosPortabilidad.getImei());
		req.setNumeroMovil(datosPortabilidad.getNumeroMovil());
		req.setNumeroNegocio(datosPortabilidad.getNumeroNegocio());

		try{			
			LOGGER.info(Utils.cabeceraLog() + "***** LLAMADO A VALIDAR DATOS PORTABILIDAD  *****");
			ValidarDatosPortabilidadSIMAUT validarDatosPortabilidad = EntelServices.validarDatosPortabilidad();
			res                                                     = validarDatosPortabilidad.getValidarDatosPortabilidadSIMAUTPort().validarDatosPortabilidadSIMAUT(req);

			logPerformance.info("[lineaClase :"+66+"]|[nombreMetodo : ValidarDatosPortabilidadSIMAUT]|[Numero Movil "+ datosPortabilidad.getNumeroMovil() +"]|[ValidarDatosPortabilidadSIMAUT]|[TIEMPO "+watch.time()+"]|");
			LOGGER.info(Utils.cabeceraLog() + "[Numero Movil  : "+ datosPortabilidad.getNumeroMovil() +"]|[ValidarDatosPortabilidadSIMAUT][TIEMPO "+watch.time()+"]");

			respuesta.setDescripcion(res.getDescription());
			respuesta.setIndicadorExito(res.getIndicadorExito());
			LOGGER.info(Utils.cabeceraLog() + "Respuesta Validar Datos Portabilidad TIEMPO: "  + watch.time());
	        LOGGER.info(Utils.cabeceraLog() + "Respuesta Validar Datos Portabilidad Codigo: "  + res.getIndicadorExito());
	        LOGGER.info(Utils.cabeceraLog() + "Respuesta Validar Datos Portabilidad Mensaje: " + res.getDescription());

		}catch (Exception e) {
			LOGGER.error("No es posible obtener el servicio Validar Datos Portabilidad " + e.getMessage());				   
			logPerformance.error("[Numero Movil : "+ datosPortabilidad.getNumeroMovil() +"]|[ValidarDatosPortabilidadSIMAUT][TIEMPO "+watch.time()+"]"+e);
			throw new Exception(e.getMessage());
		}
		return respuesta;
	}
	
	@Override
	public String enviaEmail(String to, String from, String subject, String body, List<DataEmailVO> adjuntos) {
		Cliente cliente = (Cliente) getSession().get("Cliente");
		String respuesta = "";
		TimeWatch watch = TimeWatch.start();
		LOGGER.info(Utils.cabeceraLog() + "to   : " + to);
		LOGGER.info(Utils.cabeceraLog() + "from   : " + from);
		LOGGER.info(Utils.cabeceraLog() + "subject           : " + subject);
		LOGGER.info(Utils.cabeceraLog() + "body           : " + body);
		for (DataEmailVO parametros : adjuntos) {
			LOGGER.info(Utils.cabeceraLog() + "DATA : data           : " + parametros.getData());
			LOGGER.info(Utils.cabeceraLog() + "DATA : nombre         : " + parametros.getNombre());
			LOGGER.info(Utils.cabeceraLog() + "DATA : tipo           : " + parametros.getTipo());
		}

		try {

			ListMailBeanLiteral listaEmail = new ListMailBeanLiteral();
			for (DataEmailVO parametros : adjuntos) {
				MailBean mail = new MailBean();

				mail.setData(parametros.getData().toString());
				mail.setName(parametros.getNombre().toString());
				mail.setType(parametros.getTipo().toString());
				listaEmail.getMailBean().add(mail);
			}
			EnviarMailService servicio = EntelServices.getRespuestaEmailInstanceService();
			respuesta = servicio.getEnviarMailSoapPort().enviarMail(to, from, subject, body, listaEmail);
			LOGGER.info(Utils.cabeceraLog() + "Tiempo:" + watch.time());
			LOGGER.info(Utils.cabeceraLog() + "Respuesta Email : " + respuesta);
			logPerformance.info("[lineaClase :"+414+"]|[nombreMetodo : EnviarMail]|[RUT "+ cliente.getRut() +"]|[EnviarMail]|[TIEMPO "+watch.time()+"]|");

		} catch (Exception e) {

			LOGGER.info(Utils.cabeceraLog() + "No se pudo hacer la llamada al servicio : EnviarMailService, Input:" + "[ to: " + to
					+ " ] [ from: " + from + "] [ subject: " + subject + "] " + "[ body: " + body + " ] "
					+ e.getMessage());
			respuesta = "no se realizo el envio del email";
			logPerformance.error("|[RUT "+cliente.getRut()+"]|[EnviarMail]|[ERROR SERVICIO ENVIAMAIL]|"
					+ "[DETALLE "+e.getMessage()+"]|[TIEMPO "+watch.time()+"]");
		} finally {
			LOGGER.info(Utils.cabeceraLog() + "Fin consultando Disponibilidad");
		}

		return respuesta;

	}
	@Override
	public ResTicketSGAVO ticketSGA(TicketSGAVO ticket) throws Exception {
		Cliente cliente = (Cliente) getSession().get("Cliente");
        ResTicketSGAVO respuesta       = new ResTicketSGAVO();
        CreaTicketSgaRequestType req   = new CreaTicketSgaRequestType();
        CrearTicketSgaResponseType res = new CrearTicketSgaResponseType();
		
		//se genera el request del servicio segun parametros entregados
        req.setSolicitud("R");
        req.setValor(cliente.getRut().replace("-", ""));
        req.setPlataforma("Portal MiEntel");
        req.setMundo("Ventas Campanas Beneficios");
        req.setSuboperacion(ticket.getSuboperacion());
        req.setTipo("Solicitud");
        req.setMotivo(ticket.getMotivo());
        req.setWorkflow("1");
        req.setCelularReclamado("");
		req.setNroSolicitud("");
		req.setRequerimientoCliente(ticket.getRequerimientoCliente());

		LOGGER.info(Utils.cabeceraLog() + "Motivo "+ req.getMotivo());
		LOGGER.info(Utils.cabeceraLog() + "CelularReclamado "+ req.getCelularReclamado());
		LOGGER.info(Utils.cabeceraLog() + "RUT "+ req.getValor());
		LOGGER.info(Utils.cabeceraLog() + "Requerimiento "+ req.getRequerimientoCliente());


		TimeWatch watch = TimeWatch.start();
		
		try{			
			LOGGER.info(Utils.cabeceraLog() + "***** LLAMADO A TICKET SGA  *****");
            TicketSGAService ticketSGA = EntelServices.getTicketSGAService();
            res                        = ticketSGA.getTicketSGAServiceSOAPPortType().crearTicketSga(req);
            
			LOGGER.info(Utils.cabeceraLog() + "Respuesta del servicio "+ res.getMensaje() + " cod: " + res.getCodigo() );
			logPerformance.info("|[MOVIL "+cliente.getTelefono()+"]|"
					+ "[lineaClase :"+301+"]|"
							+ "[nombreMetodo : creaTicketSGA]|"
							+ "[msisdn "+req.getCelularReclamado()+"]|"
									+ "[TicketSGAService]|[TIEMPO "+watch.time()+"]|"
											+ "[codigoRespuesta :"+res.getCodigo()+"]|");
			LOGGER.info(Utils.cabeceraLog() + "[Numero Movil "+req.getCelularReclamado()+"]|[TicketSGAService][TIEMPO "+watch.time()+"]");
			
			respuesta.setCodigo(res.getCodigo());
			respuesta.setMensaje(res.getMensaje());		
			
		}catch (Exception e) {
			LOGGER.error("No es posible obtener el servicio TicketSGA " + e.getMessage());	
			e.printStackTrace();
			LOGGER.error("[MOVIL " + req.getCelularReclamado() + "]|[Ticket SGA]|[TIEMPO " + watch.time() + "]|" + e);
			throw new Exception(e.getMessage());
		}

		return respuesta;
	}

	@Override
	public ArrayOfRegion obtenerListaRegiones() {
		Cliente cliente = (Cliente) getSession().get("Cliente");
		ObtenerListaRegionesPt response;
		ArrayOfRegion arrayRegiones = null;
		
		TimeWatch watch = TimeWatch.start();
		try {
			Usuario usuario = new Usuario();;
			usuario.setIdUsuario("admin");
			usuario.setNombreUsuario("admin");
			usuario.setPass("admin");
			ObtenerListaRegionesService obtenerListaRegiones = EntelServices.getObtenerListaRegionesService();
			arrayRegiones = obtenerListaRegiones.getObtenerListaRegionesPort().getRegionesWS(usuario, IDPAIS);
			
			logPerformance.info("|[RUT "+cliente.getRut()+"]|[IDPAIS "+IDPAIS+"]|"
					+ "[lineaClase :"+490+"]|"
							+ "[nombreMetodo : obtenerListaRegiones]|"
							+ "[msisdn]|"
									+ "[ObtenerListaRegionesService]|[TIEMPO "+watch.time()+"]|"
											+ "[codigoRespuesta :"+arrayRegiones.getRegion().get(0).getResultado().getCodigo()+"]|");
			LOGGER.info(Utils.cabeceraLog() + "[idPais "+ IDPAIS +" ]|[ObtenerListaRegionesService][TIEMPO "+watch.time()+"]");
			
		} catch (EntelServicesLocatorException e) {
			LOGGER.error("No es posible obtener el servicio ObtenerListaRegiones " + e.getMessage());	
			logPerformance.error("|[RUT "+cliente.getRut()+"]|[ObtenerListaRegiones]|"
					+ "[DETALLE "+e.getMessage()+"]|[TIEMPO "+watch.time()+"]");		
		}
		
		return arrayRegiones;
	}
	
	@Override
	public ArrayOfComuna obtenerListaComunas(String idRegion) {
		Cliente cliente = (Cliente) getSession().get("Cliente");
		ObtenerListaComunasPt response;
		ArrayOfComuna arrayComunas = null;
		
		TimeWatch watch = TimeWatch.start();
		try {
			Usuario usuario = new Usuario();;
			usuario.setIdUsuario("admin");
			usuario.setNombreUsuario("admin");
			usuario.setPass("admin");
			ObtenerListaComunasService obtenerListaComunas = EntelServices.getObtenerListaComunasService();
			arrayComunas = obtenerListaComunas.getObtenerListaComunasPort().getComunasWS(usuario, IDPAIS, idRegion);
			logPerformance.info("|[RUT "+cliente.getRut()+"]|[IDREGION "+idRegion+"]|"
					+ "[lineaClase :"+520+"]|"
							+ "[nombreMetodo : obtenerListaComunas]|"
							+ "[msisdn]|"
									+ "[ObtenerListaComunasService]|[TIEMPO "+watch.time()+"]|"
											);
			LOGGER.info(Utils.cabeceraLog() + "[idRegion "+ idRegion +" ]|[ObtenerListaComunasService][TIEMPO "+watch.time()+"]");
		} catch (EntelServicesLocatorException e) {
			LOGGER.error("No es posible obtener el servicio ObtenerListaComunas " + e.getMessage());	
			logPerformance.error("|[RUT "+cliente.getRut()+"]|[ObtenerListaComunas]|[ERROR AL OBTENER SERVICIO DE COMUNAS]|"
					+ "[DETALLE "+e.getMessage()+"]|[TIEMPO "+watch.time()+"]");		
		}
		
		return arrayComunas;
	}
	
	@Override
	public ArrayOfCalle obtenerListaCalles(String idRegion, String idComuna, String strcalle) {
		Cliente cliente = (Cliente) getSession().get("Cliente");
		ObtenerListaCallesPt response;
		ArrayOfCalle arrayCalles = null;
		
		TimeWatch watch = TimeWatch.start();
		try {
			Usuario usuario = new Usuario();;
			usuario.setIdUsuario("admin");
			usuario.setNombreUsuario("admin");
			usuario.setPass("admin");
			ObtenerListaCallesService obtenerListaCalles = EntelServices.getObtenerListaCallesService();
			arrayCalles = obtenerListaCalles.getObtenerListaCallesPort().getCallesWS(usuario, IDPAIS, idRegion, idComuna, strcalle);
			logPerformance.info("|[RUT "+cliente.getRut()+"]|[IDCOMUNA "+idComuna+"]|"
					+ "[lineaClase :"+549+"]|"
							+ "[nombreMetodo : obtenerListaCalles]|"
							+ "[msisdn]|"
									+ "[ObtenerListaCallesService]|[TIEMPO "+watch.time()+"]|"
											);
			LOGGER.info(Utils.cabeceraLog() + "[IDCOMUNA "+ idComuna +" ]|[ObtenerListaCallesService][TIEMPO "+watch.time()+"]");
		} catch (EntelServicesLocatorException e) {
			LOGGER.error("No es posible obtener el servicio ObtenerListaCalle " + e.getMessage());	
			logPerformance.error("|[RUT "+cliente.getRut()+"]|[ObtenerListaCalle]|[ERROR AL OBTENER SERVICIO DE CALLES]|"
					+ "[DETALLE "+e.getMessage()+"]|[TIEMPO "+watch.time()+"]");
		
		}
		
		return arrayCalles;
	}
	
	@Override
	public ArrayOfDireccion obtenerListaDireccion(String idCalle) {
		Cliente cliente = (Cliente) getSession().get("Cliente");
		ObtenerListaDireccionesPt response;
		ArrayOfDireccion arrayDireccion = null;
		
		TimeWatch watch = TimeWatch.start();
		try {
			Usuario usuario = new Usuario();;
			usuario.setIdUsuario("admin");
			usuario.setNombreUsuario("admin");
			usuario.setPass("admin");
			ObtenerListaDireccionesService obtenerListaDirecciones = EntelServices.getObtenerListaDireccionesService();
			arrayDireccion = obtenerListaDirecciones.getObtenerListaDireccionesPort().getDireccionesWS(usuario, IDPAIS, idCalle);
			logPerformance.info("|[RUT "+cliente.getRut()+"]|[IDCALLE "+idCalle+"]|"
					+ "[lineaClase :"+577+"]|"
							+ "[nombreMetodo : obtenerListaDireccion]|"
							+ "[msisdn]|"
									+ "[ObtenerListaDireccionesService]|[TIEMPO "+watch.time()+"]|"
											);
			LOGGER.info(Utils.cabeceraLog() + "[IDCALLE "+ idCalle +" ]|[ObtenerListaDireccionesService][TIEMPO "+watch.time()+"]");
		} catch (EntelServicesLocatorException e) {
			LOGGER.error("No es posible obtener el servicio obtenerListaDireccion " + e.getMessage());	
			logPerformance.error("|[RUT "+cliente.getRut()+"]|[ObtenerListaDirecciones]|[ERROR AL OBTENER SERVICIO DE DIRECCIONES]|"
					+ "[DETALLE "+e.getMessage()+"]|[TIEMPO "+watch.time()+"]");
		
		}
		
		return arrayDireccion;
	}
	
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}
	
	@Override
	public DatosComercialesVO obtenerDatosCliente(String rut) throws Exception {
		DatosComercialesVO datosComercialesVO = new DatosComercialesVO();
		datosComercialesVO.setRut(rut);
		
		TimeWatch watch = TimeWatch.start();
		try {
			
			ConsultarDatosFacturacionClienteRequestType consultarDatosFacturacionClienteRequest = new ConsultarDatosFacturacionClienteRequestType();
			RequestType requesType = new RequestType();
			
			requesType.setRut(rut);
			requesType.setCodigoLineaSuscripcion("M");
			
			consultarDatosFacturacionClienteRequest.setRequest(requesType);
			
			ConsultarDatosFacturacionCliente datosCliente = EntelServices.consultarDatosFacturacionCliente();
			ConsultarDatosFacturacionClienteResponseType response = datosCliente.getConsultarDatosFacturacionClientePort().consultarDatosFacturacionCliente(consultarDatosFacturacionClienteRequest);
			
			logPerformance.info("|[RUT "+ rut +"]|"
					+ "[lineaClase :641]|"
					+ "[nombreMetodo : obtenerDatosCliente]|"
					+ "[msisdn]|"
					+ "[bil_t_px_consultardatosfacturacionclientepsEndPoint]|[TIEMPO "+watch.time()+"]|"
					+ "[CODIGO|"+ response.getResponse().getHeaderOut().getCodigo() +"]"
					
											);
			LOGGER.info(Utils.cabeceraLog() + "[REQUEST:[rut|"+ rut +"]]|[bil_t_px_consultardatosfacturacionclientepsEndPoint][TIEMPO "+watch.time()+"]");
			
			LOGGER.info(Utils.cabeceraLog() + "Respuesta servicio facturacion: " + response.getResponse().getHeaderOut().getCodigo());
			if(response.getResponse().getHeaderOut().getCodigo().equals("0000")){
				datosComercialesVO.setGrupoCliente(response.getResponse().getCuentas().getCuenta().get(0).getGrupoCliente());
				datosComercialesVO.setSegmentoCliente(response.getResponse().getCuentas().getCuenta().get(0).getSegmentoCliente());
				datosComercialesVO.setTipoCliente(response.getResponse().getCuentas().getCuenta().get(0).getTipoCliente());
				datosComercialesVO.setEsCliente(true);
				
				LOGGER.info(Utils.cabeceraLog() + "[FACTURACION] GrupoCliente: " + response.getResponse().getCuentas().getCuenta().get(0).getGrupoCliente());
				LOGGER.info(Utils.cabeceraLog() + "[FACTURACION] SegmentoCliente: " + response.getResponse().getCuentas().getCuenta().get(0).getSegmentoCliente());
				LOGGER.info(Utils.cabeceraLog() + "[FACTURACION] TipoCliente: " + response.getResponse().getCuentas().getCuenta().get(0).getTipoCliente());
			}			
			
		} catch (Exception e) {
			LOGGER.error("No es posible obtener el servicio bil_t_px_consultardatosfacturacionclientepsEndPoint " + e.getMessage());	
			logPerformance.error("|[RUT "+rut+"]|[bil_t_px_consultardatosfacturacionclientepsEndPoint]|[error al invocar al servicio bil_t_px_consultardatosfacturacionclientepsEndPoint]|"
					+ "[DETALLE "+e.getMessage()+"]|[TIEMPO "+watch.time()+"]");
			throw new Exception(e.getMessage());
		
		}
		
		
		return datosComercialesVO;
	}
	
	@Override
	public JSONObject obtenerEstadoTDE(String rutUsuario) throws Exception{
		System.setProperty("https.protocols", "TLSv1");
		String estadoTDEUsuario = "";
		TimeWatch watch = TimeWatch.start();
		JSONObject obj = new JSONObject();
		
		try{
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };
			
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			
//			 Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			URL url;
			String type = "RUT";
			//Cambiar por Properties
			String urlApi = EntelServices.customerInfo();
			
			url = new URL(null, urlApi + "?MSISDN=&number="+rutUsuario+"&type=" + type,
					new sun.net.www.protocol.https.Handler());
			HttpsURLConnection urlc = (HttpsURLConnection) url.openConnection();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ") {
				@Override
				public StringBuffer format(Date date, StringBuffer toAppendTo, java.text.FieldPosition pos) {
					StringBuffer toFix = super.format(date, toAppendTo, pos);
					return toFix.insert(toFix.length() - 2, ':');
				};
			};
			String timestamp = dateFormat.format(new Date()).toString();
			
			urlc.setDoOutput(true);
			urlc.setRequestMethod("GET");
			urlc.setRequestProperty("Content-Type", "application/json");
			urlc.setRequestProperty("Authorization", generarToken());
			urlc.setRequestProperty("consumerId", generateConsumerId());
			urlc.setRequestProperty("applicationCode", APPLICATION_CODE);
			urlc.setRequestProperty("countryCode", COUNTRY_CODE);
			urlc.setRequestProperty("requestTimestamp", timestamp);
			
			if (urlc.getResponseCode() == 200) {
				BufferedReader br = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;

				while ((line = br.readLine()) != null) {
					sb.append(line);
				}
				br.close();
				LOGGER.info(Utils.cabeceraLog() + "[StringBuilder|"+sb.toString()+"][customerInfo][TIEMPO "+watch.time()+"]");
				logPerformance.info(Utils.cabeceraLog()+"[customerInfo]|[TIEMPO "+watch.time()+"]");
				obj = new JSONObject(sb.toString());
			}else{
				LOGGER.error(Utils.cabeceraLog()+"No se ha procesado la informacion. Cod. Error : "+ urlc.getResponseCode());
				logPerformance.error(Utils.cabeceraLog()+"[customerInfo]|[ERROR AL OBTENER SERVICIO DE CUSTOMERINFO]|"
						+ "[TIEMPO "+watch.time()+"]");
			}
			urlc.disconnect();
			
		}catch(Exception e){
			LOGGER.error(Utils.cabeceraLog()+ e);
			logPerformance.error(Utils.cabeceraLog()+"[customerInfo]|[ERROR AL OBTENER SERVICIO DE CUSTOMERINFO]|"
					+ "[DETALLE "+e.getMessage()+"]|[TIEMPO "+watch.time()+"]");
		}finally {
			watch.reset();
		}
		
		return obj;
	}
	
	private String generarToken() {
		System.setProperty("https.protocols", "TLSv1");
		String tokenGenerado = "";
		TimeWatch watch = TimeWatch.start();
		
		try{
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			}
			};
			
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			URL url;
			
			String urlToken = EntelServices.token();
			
			url = new URL(null, urlToken, new sun.net.www.protocol.https.Handler());

			String clientId = Config.getCOServices("tokenClientID");
			String clientSecret = Config.getCOServices("tokenClientSecret");
			
			String query = "grant_type=client_credentials&client_id="+clientId+"&client_secret="+clientSecret;
			
			HttpsURLConnection urlc = (HttpsURLConnection) url.openConnection();
			urlc.setDoOutput(true);
			urlc.setRequestMethod("POST");
			urlc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
					
			OutputStream os = urlc.getOutputStream();
			os.write(query.getBytes("UTF-8"));
			os.close();
			
			if(urlc.getResponseCode() == 200){
				BufferedReader br = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while((line = br.readLine()) != null){
					sb.append(line);
				}
				br.close();
				LOGGER.info(Utils.cabeceraLog() + "[StringBuilder|"+sb.toString()+"][token][TIEMPO "+watch.time()+"]");
				JSONObject obj = new JSONObject(sb.toString());
				String accessToken = obj.getString("access_token");
				String tokenType = obj.getString("token_type");
				tokenGenerado = tokenType +" "+ accessToken;
				
				LOGGER.info("Token para apiCustomerInfo => "+ tokenGenerado);
				logPerformance.info(Utils.cabeceraLog()+"|[TOKEN "+tokenGenerado+"][token]|[TIEMPO "+watch.time()+"]");
			}
			else{
				LOGGER.info(Utils.cabeceraLog()+"No se ha procesado la informacion. Cod. Error : "+urlc.getResponseCode());
				logPerformance.error(Utils.cabeceraLog()+"|[token]|[ERROR AL OBTENER SERVICIO DE GENERAR TOKEN]|"
						+ "[TIEMPO "+watch.time()+"]");
			}
			urlc.disconnect();
			
		}catch(Exception e){
			LOGGER.error(Utils.cabeceraLog()+e);
			logPerformance.error(Utils.cabeceraLog()+"[token]|[ERROR AL OBTENER SERVICIO DE GENERAR TOKEN]|"
					+ "[DETALLE "+e.getMessage()+"]|[TIEMPO "+watch.time()+"]");
		}finally {
			watch.reset();
		}
		return tokenGenerado;
	}
	
	public static String generateConsumerId() {
		String consumerId = "";

		SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyyMMddHHmmss.SSS") {
			@Override
			public StringBuffer format(Date date, StringBuffer toAppendTo, java.text.FieldPosition pos) {
				StringBuffer toFix = super.format(date, toAppendTo, pos);
				return toFix.insert(toFix.length() - 2, ':');
			};
		};
		consumerId = "NTF"+dateFormat2.format(new Date()).toString();		
		return consumerId;
	}

	/*
	 * M�todo encargado de consultar la disponiblidad del C2C, en caso de est�r fuera de horario se retorna una excepcion para mostrar pantalla de error
	 * 
	 * 
	 */
	public Boolean getHorarioAtencion() throws ServiceException, ConsultarHorarioAtencionException, ParseException , AgendarLLamadaException{
		FUERA_DE_HORARIO=false;
		List<HorarioAtencionType> horarioAtencionList= new ArrayList<HorarioAtencionType>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaActual = new Date();

        String strfechaActual = sdf.format(fechaActual);

        SimpleDateFormat sdfh = new SimpleDateFormat("HHmm");
        int horaActual = Integer.parseInt(sdfh.format(fechaActual));


        LOGGER.info("Se ingresa al m�todo getHorarioAtencion que revisa horario de atencion del Click to Call [Fecha Actual]: "+ fechaActual);

        ConsultarHorarioAtencionReq request = new ConsultarHorarioAtencionReq();

        request.setFechaActual(strfechaActual);
        LOGGER.info("Se invoca al DAO del servicio de horario de atencion [getHorarioAtencion]");
        ConsultarHorarioAtencionRes  response = EntelServices.getHorarioAtencion(request);
        LOGGER.info("Servicio de ClickToCall.getHorarioAtencion responde codigo: " + response.getRespuesta().getCodigo());

        if(!Config.checkCodeService(Constantes.C2C_CODIGOS_SERVICIO_EXITOSO_CONSULTARHORARIOATENCION, response.getRespuesta().getCodigo())){
            LOGGER.info("Fecha no Existe. Codigo: " + response.getRespuesta().getCodigo() + " Mensaje: " + response.getRespuesta().getDescripcion());
            throw new ConsultarHorarioAtencionException(response.getRespuesta().getDescripcion());
        }
        FUERA_DE_HORARIO = false;
        if(horaActual>response.getFechasHorasAtencion().get(0).getRangoHorarios().get(0).getFin()|| horaActual<response.getFechasHorasAtencion().get(0).getRangoHorarios().get(0).getInicio()){
            LOGGER.info("Solicitud fuera de horario. Codigo: " + response.getRespuesta().getCodigo() + " Mensaje: " + response.getRespuesta().getDescripcion() + " Fecha actual: "+ fechaActual);
            FUERA_DE_HORARIO=true;
        }
        
        return FUERA_DE_HORARIO;
	}
	
	
	/**
	 * M�todo encargado de generar la solicitud de llamado al call para que el cliente pueda ser contactado
	 * @param clienteType Datos del cliente, se obtiene el telefono y datos adicionales para logear su solicitud
	 * @return responseC2C indica si la solicitud ha sido ingresada correctamente.
	 * @throws AgendarLLamadaException 
	 * @throws Exception 
	 * @throws ConsultarHorarioAtencionException 
	 * @throws ServiceException 
	 */
	public RespuestaC2CType solicitarLlamado(Cliente clienteType, String numContacto, Plan plan)throws  ServiceException, SolicitarLlamadaException, ConsultarHorarioAtencionException, AgendarLLamadaException, Exception{
		
        LOGGER.info("Se llama al service de crm_t_px_registraraccionsolicitudllamadaps.RegistrarAccionSolicitudLlamadaRequest, " +
                "[Numero Telefono|" + numContacto + "]" );
        LOGGER.info("Se ingresa al m�todo solicitarLlamado que solicita un llamado Click to Call [Movil| "+ numContacto+"][Rut|"+clienteType.getRut()+"]");

        SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdfh = new SimpleDateFormat("HH:mm");
        Date fechaActual = new Date();
        
        getHorarioAtencion();

        //Agendamiento momentaneo TODO
        if(FUERA_DE_HORARIO){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fechaActual);
            if(calendar.get(Calendar.HOUR_OF_DAY)>21){
                calendar.add(Calendar.DAY_OF_YEAR,1);
            }
            calendar.set(Calendar.HOUR_OF_DAY,10);
            calendar.set(Calendar.MINUTE,0);
            fechaActual=calendar.getTime();
        }

        String strfechaActual = sdff.format(fechaActual);
        String strhoraActual = sdfh.format(fechaActual);

        RespuestaC2CType objRespuesta = new RespuestaC2CType();

        RegistrarAccionSolicitudLlamadaReq request =new RegistrarAccionSolicitudLlamadaReq();
        
        //Campos Obligatorios : servicio,tiposolicitud, fecha, hora, fono
        //request.setSesionAccion("EnviarSolicitud");
        request.setServicio(Config.getCOServices(Constantes.C2C_PARAMETRO_SERVICIO_SOLICITARLLAMADO));

        request.setParamLinea1(clienteType.getFlujo());
        if(clienteType.getEsClienteEntel()){
        	request.setParamLinea1("Migracion");
        }
        request.setParamLinea2(plan.getNombrePlanCompleto());
        request.setParamLinea3(plan.getCargoPlan());
        
        String origenC2C = "Flujo Digital";
        if(null != plan.getOrigenOferta() && !plan.getOrigenOferta().trim().equals("")){
        	String palabras[]=plan.getOrigenOferta().toLowerCase().split("\\_");
        	
        	if(palabras[0].equals("app") || palabras[0].equals("atg") || palabras[0].equals("mientel")){
        		origenC2C = "app_movil";
            }
        }        
        request.setParamLinea4(origenC2C);

        request.setTipoSolicitud(Config.getCOServices(Constantes.C2C_PARAMETRO_TIPOSOLICITUD_SOLICITARLLAMADO));
        request.setFechaSolicitud(strfechaActual);
        request.setHoraSolicitud(strhoraActual);

        request.setNombre(clienteType.getNombre() != null && !clienteType.getNombre().equals(Constantes.VACIO) ? clienteType.getNombre() : Constantes.VACIO);
        request.setApellido(clienteType.getApellidos() != null && !clienteType.getApellidos().equals(Constantes.VACIO) ? clienteType.getApellidos() : Constantes.VACIO);
        request.setRut(clienteType.getRut());

        request.setNumeroFono(numContacto);


        LOGGER.info("Se invoca al DAO del servicio de registro de solicitud de llamada [registrarSolicitudLlamada]");

        RegistrarAccionSolicitudLlamadaRes response = EntelServices.registrarSolicitudLlamada(request);

         LOGGER.info("Servicio de ClickToCall.solicitarLlamado responde codigo: " + response.getCodigo());

        if (!Config.checkCodeService(Constantes.C2C_CODIGOS_SERVICIO_EXITOSO_SOLICITARLLAMADO, response.getCodigo())){
            throw new SolicitarLlamadaException(response.getDescripcion());
        }

        if(FUERA_DE_HORARIO){
            response.setCodigo("2");
            response.setDescripcion("Call fuera de horario, se agenda para el d�a siguiente.");
        }
        
        objRespuesta.setDescripcion(response.getDescripcion());
        objRespuesta.setCodigo(response.getCodigo());

        return objRespuesta;
	
	}
	
	/**
	 * M�todo encargado de validar el numero de serie para el rut ingresado
	 * @param clienteType Datos del cliente, se obtiene el telefono y datos adicionales para logear su solicitud
	 * @return responseC2C indica si la solicitud ha sido ingresada correctamente.
	 * @throws AgendarLLamadaException 
	 * @throws Exception 
	 * @throws ConsultarHorarioAtencionException 
	 * @throws ServiceException 
	 */
	@Override
	public RespuestaType validarSerie(Cliente clienteType)throws  ServiceException, Exception{
		
		RespuestaType resp = new RespuestaType();

        

        VerificaSerieCanalOnlineReq request = new VerificaSerieCanalOnlineReq();

        request.setRut(clienteType.getRut().substring(0, clienteType.getRut().length()-2));
        request.setSerie(clienteType.getnSerie().toUpperCase());

        VerificaSerieCanalOnlineRes response = EntelServices.verificarSerie(request);

        String codigoRespuesta = "1";

        if(Config.checkCodeService(SERVICIO_VALIDA_SERIE_RETORNO_EXITOSO, response.getCodRespuesta()+"")) {
            codigoRespuesta = "0";
        } else if (Config.checkCodeService(SERVICIO_VALIDA_SERIE_RETORNO_FALLIDO, response.getCodRespuesta()+"")){
            codigoRespuesta = "1";
        }  else if (Config.checkCodeService(SERVICIO_VALIDA_SERIE_RETORNO_BLOQUEADO, response.getCodRespuesta()+"")){
            codigoRespuesta = "2";
        }

        resp.setCodigo(codigoRespuesta);
        resp.setMensaje(response.getDescrRespuesta());

        return resp;
	
	}

	
	
}
