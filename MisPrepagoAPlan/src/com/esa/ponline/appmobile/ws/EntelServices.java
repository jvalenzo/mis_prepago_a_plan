package com.esa.ponline.appmobile.ws;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;
import org.tempuri.ObtenerListaCallesService;
import org.tempuri.ObtenerListaComunasService;
import org.tempuri.ObtenerListaDireccionesService;
import org.tempuri.ObtenerListaRegionesService;

import com.epcs.clientes.evaluacion.EvaluacionComercialNecService;
import com.epcs.integracionit.flujotrabajo.EnviarMailService;
import com.esa.billing.billingacco.t.consultardatosfacturacioncliente.ConsultarDatosFacturacionCliente;
import com.esa.cliente.contacto.sga.TicketSGAService;
import com.esa.crm.crmcustomerinterface.t.consultarhorarioatencion.ConsultarHorarioAtencion;
import com.esa.crm.crmcustomerinterface.t.consultarhorarioatencion.ConsultarHorarioAtencionPort;
import com.esa.crm.crmcustomerinterface.t.consultarhorarioatencion.request.ConsultarHorarioAtencionReq;
import com.esa.crm.crmcustomerinterface.t.consultarhorarioatencion.response.ConsultarHorarioAtencionRes;
import com.esa.crm.crmcustomerinterface.t.registraraccionsolicitudllamada.RegistrarAccionSolicitudLlamada;
import com.esa.crm.crmcustomerinterface.t.registraraccionsolicitudllamada.RegistrarAccionSolicitudLlamadaPort;
import com.esa.crm.crmcustomerinterface.t.registraraccionsolicitudllamada.request.RegistrarAccionSolicitudLlamadaReq;
import com.esa.crm.crmcustomerinterface.t.registraraccionsolicitudllamada.response.RegistrarAccionSolicitudLlamadaRes;
import com.esa.crm.crmloyalty.t.verificaseriecanalonline.CRMTVerificaSerieCanalOnline;
import com.esa.crm.crmloyalty.t.verificaseriecanalonline.CRMTVerificaSerieCanalOnlinePort;
import com.esa.crm.crmloyalty.t.verificaseriecanalonline.request.VerificaSerieCanalOnlineReq;
import com.esa.crm.crmloyalty.t.verificaseriecanalonline.response.VerificaSerieCanalOnlineRes;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.ValidaMsisdnService;
import com.esa.marketsales.salesmassive.n.enviarcapinternosuscripcion.EnviarCapInternoSuscripcion;
import com.esa.marketsales.salesmassive.n.validardatosclientesimaut.ValidarDatosClienteSIMAUT;
import com.esa.marketsales.salesmassive.n.validardatosportabilidadsimaut.ValidarDatosPortabilidadSIMAUT;
import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.imp.ServicesImplements;
import com.esa.ponline.appmobile.utils.TimeWatch;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.actions.Constantes;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.opensymphony.xwork2.ActionContext;

import cl.esa.ponline.appmobile.exception.ServiceException;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.SSGPConsultaFactPortacionSyncService;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.SSGPConsultaTitularDeudaSyncService;
import cl.iecisa.gateway.ws.integrator.services.ssgp_solicitacapasync.SSGPSolicitaCAPAsyncService;

public class EntelServices {
	private static final Logger LOGGER 	= Logger.getLogger(ServicesImplements.class);
	private static final Logger	LOGGER_PERFORMANCE	= Logger.getLogger("Performance");
	
	public static final boolean REUSE             = true;
	private static EvaluacionComercialNecService evaluacionComercialNecService         = null;
	private static ValidarDatosClienteSIMAUT validaDatosClienteSIMAUT                  = null;
	private static SSGPConsultaTitularDeudaSyncService consultaTitularDeudaSyncService = null;
	private static ValidaMsisdnService validaCompaniaService                           = null;
	private static SSGPSolicitaCAPAsyncService solicitaCap                             = null;
	private static EnviarCapInternoSuscripcion generaCapSuscripcion                    = null;
	private static ValidarDatosPortabilidadSIMAUT validarDatosPortabilidad             = null;
	private static SSGPConsultaFactPortacionSyncService consultaFactPortacion          = null;
	private static EnviarMailService respuestaEmailInstanceService  				   = null;
	private static TicketSGAService ticketSGAService                                   = null;
	private static ObtenerListaRegionesService obtenerListaRegionesService			   = null; 	
	private static ObtenerListaComunasService obtenerListaComunasService			   = null;
	private static ObtenerListaDireccionesService obtenerListaDireccionesService       = null;
	private static ObtenerListaCallesService obtenerListaCallesService                 = null;
	private static ConsultarDatosFacturacionCliente consultarDatosFacturacionCliente   = null;
	
    private static final String SERVICE_VALIDAR_SERIE_URL = "verificarserie.wsdl";
    private static final String SERVICE_VALIDAR_SERIE_USER = "verificarserie.user";
    private static final String SERVICE_VALIDAR_SERIE_PASS = "verificarserie.pass";
    private static final String SERVICE_VALIDAR_SERIE_ERROR = "error.ws.validarserie";
	
	//REST
	private static String customerInfo 												   = null;
	private static String token 													   = null;
	
	public static ValidarDatosPortabilidadSIMAUT validarDatosPortabilidad() throws Exception {
		if (validarDatosPortabilidad              != null && REUSE) {
			return validarDatosPortabilidad;
		}		
		try {
			String urlWS = Config.getCOServices("validarDatosPortabilidadEndPoint");
			LOGGER.info(Utils.cabeceraLog() + "urlWS: " + urlWS);
			validarDatosPortabilidad = new ValidarDatosPortabilidadSIMAUT(new URL(urlWS), new QName("http://www.esa.com/MarketSales/SalesMassive/N/validarDatosPortabilidadSIMAUT", "validarDatosPortabilidadSIMAUT"));
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio ValidarDatosPortabilidadSIMAUT: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + ValidarDatosPortabilidadSIMAUT.class, e);	
			throw new Exception(e.getMessage());
		}
		return validarDatosPortabilidad;	
	}
	public static EnviarCapInternoSuscripcion generaCapSuscripcion() {
		if (generaCapSuscripcion                     != null && REUSE) {
			return generaCapSuscripcion;
		}		
		try {
			String urlWS = Config.getCOServices("generaCapSuscripcionEndPoint");
			LOGGER.info(Utils.cabeceraLog() + "urlWS: " + urlWS);
			generaCapSuscripcion = new EnviarCapInternoSuscripcion(new URL(urlWS), new QName("http://www.esa.com/MarketSales/SalesMassive/N/enviarCapInternoSuscripcion", "enviarCapInternoSuscripcion"));
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio EnviarCapInternoSuscripcion: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + EnviarCapInternoSuscripcion.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				//LOGGER.error("No es posible crear la llamada al servicio EnviarCapInternoSuscripcion: " + e.getMessage());
				e1.printStackTrace();
			}
		}
		return generaCapSuscripcion;	
	}
	public static SSGPSolicitaCAPAsyncService generaCap() {
		if (solicitaCap                              != null && REUSE) {
			return solicitaCap;
		}
		
		try {
			String urlWS = Config.getCOServices("generaCapEndPoint");
			LOGGER.info(Utils.cabeceraLog() + "urlWS: " + urlWS);
			solicitaCap = new SSGPSolicitaCAPAsyncService(new URL(urlWS), new QName("http://SSGP_SolicitaCAPAsync.services.integrator.ws.gateway.iecisa.cl/", "SSGP_SolicitaCAPAsyncService"));
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio SSGPSolicitaCAPAsyncService: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + SSGPSolicitaCAPAsyncService.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				//LOGGER.error("No es posible crear la llamada al servicio SSGPSolicitaCAPAsyncService: " + e.getMessage());
				e1.printStackTrace();
			}
		}
		return solicitaCap;	
	}

	public static ValidaMsisdnService validaCompania() throws EntelServicesLocatorException {
		if (validaCompaniaService != null && REUSE) {
			return validaCompaniaService;
		}

		try {
			String urlWS = Config.getCOServices("obtenerCompaniaEndPoint");
			
			validaCompaniaService = new ValidaMsisdnService(
					new URL(urlWS),
					new QName("http://www.esa.com/CRM/Portabilidad/T/ObtenerCompaniaMovil", "validaMsisdnService"));
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio ValidaMsisdnService: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + ValidaMsisdnService.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio ValidaMsisdnService: " + e.getMessage());
				e1.printStackTrace();
			}
		}
		return validaCompaniaService;
	}
	
	public static SSGPConsultaTitularDeudaSyncService getConsultaTitularDeudaSyncService() {
		if (consultaTitularDeudaSyncService != null && REUSE) {
			return consultaTitularDeudaSyncService;
		}
		
		try {
			String urlWS = Config.getCOServices("consultaTitularDeudaSyncServiceEndPoint");
			LOGGER.info(Utils.cabeceraLog() + "urlWS: " + urlWS);
			consultaTitularDeudaSyncService = new SSGPConsultaTitularDeudaSyncService(
					new URL(urlWS), 
					new QName("http://SSGP_ConsultaTitularDeudaSync.services.integrator.ws.gateway.iecisa.cl/", 
					"SSGP_ConsultaTitularDeudaSyncService")
			);
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio SSGPConsultaTitularDeudaSyncService: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + SSGPConsultaTitularDeudaSyncService.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio SSGPConsultaTitularDeudaSyncService: " + e.getMessage());
				e1.printStackTrace();
			}
		}
		
		return consultaTitularDeudaSyncService;	
	}
	
	public static ValidarDatosClienteSIMAUT getValidaDatosClienteSIMAUT() {
		if (validaDatosClienteSIMAUT != null && REUSE) {
			return validaDatosClienteSIMAUT;
		}		
		try {
			String urlWS = Config.getCOServices("validaDatosClienteSIMAUTEndPoint");
			LOGGER.info(Utils.cabeceraLog() + "urlWS: " + urlWS);
			validaDatosClienteSIMAUT = new ValidarDatosClienteSIMAUT (new URL(urlWS), new QName("http://www.esa.com/MarketSales/SalesMassive/N/validarDatosClienteSIMAUT", "validarDatosClienteSIMAUT"));
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio ValidaDatosClienteSIMAUT: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + EnviarCapInternoSuscripcion.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio ValidaDatosClienteSIMAUT: " + e.getMessage());
				e1.printStackTrace();
			}
		}
		return validaDatosClienteSIMAUT;	
	}
	public static EvaluacionComercialNecService getEvaluacionComercial() throws EntelServicesLocatorException {
		if (evaluacionComercialNecService != null && REUSE) {
			return evaluacionComercialNecService;
		}
		
		try {
			String urlWS = Config.getCOServices("evaluacionComercialEndPoint");
			LOGGER.info(Utils.cabeceraLog() + "urlWS: " + urlWS);
			evaluacionComercialNecService = new EvaluacionComercialNecService(new URL(urlWS), new QName("http://www.epcs.com/clientes/evaluacion", "evaluacionComercialNecService"));
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio EvaluacionComercialNecService: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + EvaluacionComercialNecService.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio EvaluacionComercialNecService: " + e.getMessage());
				e1.printStackTrace();
			}
		}
		return evaluacionComercialNecService;		
	}
	public static SSGPConsultaFactPortacionSyncService getConsultaFactPortacion() throws Exception {
		if (consultaFactPortacion != null && REUSE) {
			return consultaFactPortacion;
		}		
		
		try {
			String urlWS = Config.getCOServices("consultaFactPortacionEndPoint");
			LOGGER.info(Utils.cabeceraLog() + "urlWS: " + urlWS);
			consultaFactPortacion = new SSGPConsultaFactPortacionSyncService (new URL(urlWS), new QName("http://SSGP_ConsultaFactPortacionSync.services.integrator.ws.gateway.iecisa.cl/", "SSGP_ConsultaFactPortacionSyncService"));
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio ConsultaFactPortacion: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + EnviarCapInternoSuscripcion.class, e);
			throw new Exception(e.getMessage());
		
		}
		
		return consultaFactPortacion;	
	}
	public static EnviarMailService getRespuestaEmailInstanceService() throws EntelServicesLocatorException {
		if (respuestaEmailInstanceService != null && REUSE) {
			return respuestaEmailInstanceService;
		}
		try {

			respuestaEmailInstanceService = new EnviarMailService(new URL(Config.getCOServices("EmailEndPoint")),
			//respuestaEmailInstanceService = new EnviarMailService(new URL("http://esb.entel.cl/IntegracionIT/enviomail/Proxies/enviamailPS?wsdl"),
			new QName("http://www.epcs.com/integracionit/flujotrabajo", "EnviarMailService"));

		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio EnviarMailService: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + EnviarMailService.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio EnviarMailService: " + e.getMessage());
				
			}
		}

		return respuestaEmailInstanceService;
	}
	public static TicketSGAService getTicketSGAService() throws Exception {
		
		if (ticketSGAService                                    != null && REUSE) {
			return ticketSGAService;
		}
		String urlWS = Config.getCOServices("generaTicketSGAEndPoint");
		try {
			ticketSGAService = new TicketSGAService(					
				new URL(urlWS),
				new QName("http://www.esa.com/cliente/contacto/sga/", "TicketSGAService")
			);
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + TicketSGAService.class, e);
			throw new Exception(e.getMessage());
		}

		return ticketSGAService;
	}
	
public static ObtenerListaRegionesService getObtenerListaRegionesService() throws EntelServicesLocatorException {
		
		if (obtenerListaRegionesService                                    != null && REUSE) {
			return obtenerListaRegionesService;
		}
		String urlWS = Config.getCOServices("obtenerlistaregionesservicepsEndPoint");
		LOGGER.info(Utils.cabeceraLog() + "URL SERVICIO!! >>>" + urlWS);
		try {
			obtenerListaRegionesService = new ObtenerListaRegionesService(					
				new URL(urlWS),
				new QName("http://tempuri.org/", "ObtenerListaRegionesService")
			);
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + ObtenerListaRegionesService.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio ObtenerListaRegionesService: " + e.getMessage());
				e1.printStackTrace();
			}
		}

		return obtenerListaRegionesService;
	}


	public static ObtenerListaComunasService getObtenerListaComunasService() throws EntelServicesLocatorException {
		
		if (obtenerListaComunasService                                    != null && REUSE) {
			return obtenerListaComunasService;
		}
		String urlWS = Config.getCOServices("obtenerlistacomunasservicepsEndPoint");
		try {
			obtenerListaComunasService = new ObtenerListaComunasService(					
				new URL(urlWS),
				new QName("http://tempuri.org/", "ObtenerListaComunasService")
			);
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + ObtenerListaComunasService.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio ObtenerListaComunasService: " + e.getMessage());
				e1.printStackTrace();
			}
		}
	
		return obtenerListaComunasService;
	}
	
	public static ObtenerListaCallesService getObtenerListaCallesService() throws EntelServicesLocatorException {
		
		if (obtenerListaCallesService                                    != null && REUSE) {
			return obtenerListaCallesService;
		}
		String urlWS = Config.getCOServices("obtenerlistacallesservicepsEndPint");
		try {
			obtenerListaCallesService = new ObtenerListaCallesService(					
				new URL(urlWS),
				new QName("http://tempuri.org/", "ObtenerListaCallesService")
			);
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + ObtenerListaCallesService.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio ObtenerListaCallesService: " + e.getMessage());
				e1.printStackTrace();
			}
		}

		return obtenerListaCallesService;
	}
	
	public static ObtenerListaDireccionesService getObtenerListaDireccionesService() throws EntelServicesLocatorException {
		
		if (obtenerListaDireccionesService                                    != null && REUSE) {
			return obtenerListaDireccionesService;
		}
		String urlWS = Config.getCOServices("obtenerlistadireccionesservicepsEndPoint");
		try {
			obtenerListaDireccionesService = new ObtenerListaDireccionesService(					
				new URL(urlWS),
				new QName("http://tempuri.org/", "ObtenerListaDireccionesService")
			);
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + ObtenerListaDireccionesService.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio ObtenerListaDireccionesService: " + e.getMessage());
				e1.printStackTrace();
			}
		}

		return obtenerListaDireccionesService;
	}
	

	public static ConsultarDatosFacturacionCliente consultarDatosFacturacionCliente() throws Exception {
		if (consultarDatosFacturacionCliente != null && REUSE) {
			return consultarDatosFacturacionCliente;
		}		
		try {
			String urlWS = Config.getCOServices("bil_t_px_consultardatosfacturacionclientepsEndPoint");
			LOGGER.info(Utils.cabeceraLog() + "urlWS: " + urlWS);
			consultarDatosFacturacionCliente = new ConsultarDatosFacturacionCliente(new URL(urlWS), new QName("http://www.esa.com/Billing/BillingAcco/T/consultarDatosFacturacionCliente", "ConsultarDatosFacturacionCliente"));
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio ValidarDatosPortabilidadSIMAUT: " + e.getMessage());
			LOGGER.error( e );
			throw new Exception(e.getMessage());
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + ValidarDatosPortabilidadSIMAUT.class, e);	
			throw new Exception(e.getMessage());
		}
		return consultarDatosFacturacionCliente;	
	}
	
	public static String customerInfo() throws Exception{
		if (customerInfo != null && REUSE) {
			return customerInfo;
		}		
		try {
			customerInfo = Config.getCOServices("customerInfoEndPoint");
			LOGGER.info(Utils.cabeceraLog() + "urlWS: " + customerInfo);
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio CustomerInfo" , e);	
			throw new Exception(e.getMessage());
		}
		return customerInfo;	
	}
	
	public static String token() throws Exception{
		if (token != null && REUSE) {
			return token;
		}		
		try {
			token = Config.getCOServices("tokenEndPoint");
			LOGGER.info(Utils.cabeceraLog() + "urlWS: " + token);
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio Token" , e);	
			throw new Exception(e.getMessage());
		}
		return token;	
	}

	public static ConsultarHorarioAtencionRes getHorarioAtencion (ConsultarHorarioAtencionReq request) throws ServiceException{
		
		TimeWatch watch = TimeWatch.start();
        ConsultarHorarioAtencionRes response;
        
        try {
            URL wsdlLocation = new URL(Config.getCOServices(Constantes.C2C_CONSULTARHORARIOATENCION_WSDL));

            ConsultarHorarioAtencion servicio= new ConsultarHorarioAtencion(wsdlLocation,new QName("http://www.esa.com/CRM/CRMCustomerInterface/T/ConsultarHorarioAtencion","ConsultarHorarioAtencion"));
            ConsultarHorarioAtencionPort port= servicio.getConsultarHorarioAtencionPort();

            LOGGER.info("Se invoca al servicio crm_t_px_consultarhorarioatencionps.ConsultarHorarioAtencionRequest con los siguientes parametros: Fecha-Actual: " + request.getFechaActual());
            response=port.consultarHorarioAtencion(request);
            LOGGER_PERFORMANCE.info("[crm_t_px_consultarhorarioatencionps.ConsultarHorarioAtencionRequest][Tiempo|"+watch.time()+ "][RESULTADO|"+ response.getRespuesta().getCodigo()+"]");
            LOGGER.info("Respuesta existosa servicio crm_t_px_consultarhorarioatencionps.ConsultarHorarioAtencionRequest con codigo: " + response.getRespuesta().getCodigo());
        }catch (Exception e){
            LOGGER.error("Error al llamar al servicio crm_t_px_consultarhorarioatencionps.ConsultarHorarioAtencionRequest, Mensaje: " + Utils.obtenerDetalleError(e));
            LOGGER_PERFORMANCE.error("[crm_t_px_consultarhorarioatencionps.ConsultarHorarioAtencionRequest]" +  "[Tiempo|"+watch.time() +"][|ERROR| " + Utils.obtenerDetalleError(e));
            throw new ServiceException(Utils.obtenerDetalleError(e));
        }finally {
            watch.reset();
        }
        
        return response;
	}
	
	public static RegistrarAccionSolicitudLlamadaRes registrarSolicitudLlamada(RegistrarAccionSolicitudLlamadaReq request) throws ServiceException {
        TimeWatch watch=TimeWatch.start();

        RegistrarAccionSolicitudLlamadaRes response;

        try{
            URL wsdlLocation = new URL(Config.getCOServices(Constantes.C2C_SOLICITARLLAMADO_WSDL));

            RegistrarAccionSolicitudLlamada servicio= new RegistrarAccionSolicitudLlamada(wsdlLocation,new QName("http://www.esa.com/CRM/CRMCustomerInterface/T/RegistrarAccionSolicitudLlamada","RegistrarAccionSolicitudLlamada"));
            RegistrarAccionSolicitudLlamadaPort port=servicio.getRegistrarAccionSolicitudLlamadaPort();

            LOGGER.info("Se invoca al servicio crm_t_px_registraraccionsolicitudllamadaps.RegistrarAccionSolicitudLlamadaRequest con los siguientes parametros: [accion|"+request.getAccion()+
                    "][movil|"+request.getMovil()+"][opcion|"+request.getOpcion()+"][sesionID|"+request.getSesionID()+"][sesionAccion|"+request.getSesionAccion()+"][servicio|"+request.getServicio()+"][paramLinea1|"+request.getParamLinea1()+
                    "][paramLinea2|"+request.getParamLinea2()+"][paramLinea3|"+request.getParamLinea3()+"][paramLinea4|"+request.getParamLinea4()+"][nombre|"+request.getNombre()+"][apellido|"+request.getApellido()+"][rut|"+request.getRut()+
                    "][email|"+request.getEmail()+"][tipoFono|"+request.getTipoFono()+"][numeroFono|"+request.getNumeroFono()+"][tipoSolicitud|"+request.getTipoSolicitud()+"][fechaSolicitud|"+request.getFechaSolicitud()+"][horaSolicitud|"+request.getHoraSolicitud()+"]");
            response=port.registrarAccionSolicitudLlamada(request);
            LOGGER_PERFORMANCE.info("crm_t_px_registraraccionsolicitudllamadaps.RegistrarAccionSolicitudLlamadaRequest[Tiempo|"+watch.time()+ "][RESULTADO|"+ response.getCodigo()+"]");
            LOGGER.info("Respuesta exitosa del servicio crm_t_px_registraraccionsolicitudllamadaps.RegistrarAccionSolicitudLlamadaRequest con codigo: " + response.getCodigo());
        }catch (Exception e){
            LOGGER.error("Error al llamar al servicio [crm_t_px_registraraccionsolicitudllamadaps.RegistrarAccionSolicitudLlamadaRequest]");
            LOGGER_PERFORMANCE.error("crm_t_px_registraraccionsolicitudllamadaps.RegistrarAccionSolicitudLlamadaRequest"+  "[Tiempo|"+watch.time() +"][|ERROR| " + Utils.obtenerDetalleError(e));
            throw new ServiceException(Utils.obtenerDetalleError(e));
        }finally {
            watch.reset();
        }

        return response;

    }
	
	public static VerificaSerieCanalOnlineRes verificarSerie(VerificaSerieCanalOnlineReq request) throws ServiceException{

        VerificaSerieCanalOnlineRes response;
        TimeWatch watch = TimeWatch.start();
        Cliente cliente = (Cliente) getSession().get("Cliente");
       
        try {

            /*
             *   se lee el endpoint desde el servicio y se crea una instancia de URL
             *   En este caso se descargo el wsdl y se coloc� dentro del jar en la carpeta /META-INF. Esto debido a que el wsdl en la web pide autenticacion
             *   Si se va a generar el cliente nuevamente se debe colocar el wsdl dento
             */

            URL wsdlLocation = EntelServices.class.getResource(Config.getCOServices(SERVICE_VALIDAR_SERIE_URL));

            CRMTVerificaSerieCanalOnline service = new CRMTVerificaSerieCanalOnline(wsdlLocation, new QName("http://www.esa.com/CRM/CRMLoyalty/T/VerificaSerieCanalOnline", "CRM_T_verificaSerieCanalOnline"));

            CRMTVerificaSerieCanalOnlinePort  verificaSerieCanalOnlinePort = service.getCRMTVerificaSerieCanalOnlinePort();

            Map<String, Object> requestContext = ((BindingProvider)verificaSerieCanalOnlinePort).getRequestContext();
            requestContext.put(BindingProvider.USERNAME_PROPERTY, Config.getCOServices(SERVICE_VALIDAR_SERIE_USER));
            requestContext.put(BindingProvider.PASSWORD_PROPERTY, Config.getCOServices(SERVICE_VALIDAR_SERIE_PASS));

            //Se instancia variable para medir el rendimiento del servicio "rut", "movil", "serie"
            LOGGER.debug(Utils.cabeceraLog() + "Se invoca al servicio [validarSerieCanalOnline] con request: [rut| " + request.getRut() + "][serie|" + request.getSerie()+"]");
            response = verificaSerieCanalOnlinePort.verificaSerieCanalOnline(request);
            LOGGER_PERFORMANCE.info("[validarSerieCanalOnline][Tiempo|"+watch.time()+ "][REQUEST|Rut:"+ request.getRut() +", NumSerie:"+ request.getSerie() +"][RESULTADO|"+ response.getCodRespuesta() +"]");
        } catch (Exception e) {
            LOGGER_PERFORMANCE.error("[validarSerieCanalOnline][Tiempo|"+watch.time()+ "][|ERROR| + " + Utils.obtenerDetalleError(e)+ "]");
            throw new ServiceException(Utils.obtenerDetalleError(e));
        }finally {
            watch.reset();
        }

        return response;
    }

	private static Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}
}
