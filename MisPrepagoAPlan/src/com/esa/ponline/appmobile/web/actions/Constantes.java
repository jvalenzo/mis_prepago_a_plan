package com.esa.ponline.appmobile.web.actions;

public final class Constantes {

	public static final int CODIGO_PLAN = 0;
	public static final int NOMBRE_PLAN = 1;
	public static final String MENSAJE_COBRO_ANTICIPADO = "mi.primer.multi.mensaje.cobro.anticipado";
	
	/*CONSTANTES PROPERTIES PARA OBTENER DETALLE PLAN*/
	public static final int DETALLE_PLAN_CODIGO = 0;
	public static final int DETALLE_PLAN_NOMBRE = 1;
	public static final int DETALLE_PLAN_DATOS = 2;
	public static final int DETALLE_PLAN_RRSS = 3;
	public static final int DETALLE_PLAN_MINUTOS = 4;
	public static final int DETALLE_PLAN_SMS = 5;
	public static final int DETALLE_PLAN_CARGO_FIJO = 6;
	public static final int DETALLE_PLAN_CARGO_FIJO_OFERTA = 7;
	public static final int DETALLE_PLAN_DESCUENTO_ESPECIFICO = 8;
	public static final int DETALLE_PLAN_PORCENTAJE_DESCUENTO = 9;
	public static final int DETALLE_PLAN_DESCUENTO_ONLINE = 10;
	public static final String DETALLE_PLAN = "Plan";
	
	
	public static final String TIENDA = "location.href='https://entel.cl/tiendas/'";
	public static final String PLANES = "location.href='https://www.entel.cl/planes/'";
	public static final String PAGAR = "location.href='https://www.entel.cl/pagosonline/'";
	
	public static final String OFERTA_ESPECIFICA_URL_CONTRATAR = "oferta.especifica.url.contratar";
	public static final String OFERTA_ESPECIFICA_MENSAJE_CONTRATAR = "oferta.especifica.mensaje.contratar";
	public static final String OFERTA_ESPECIFICA_URL_CAMBIAR = "oferta.especifica.url.cambiar";
	public static final String OFERTA_ESPECIFICA_MENSAJE_CAMBIAR = "oferta.especifica.mensaje.cambiar";
	
	public static final String NUMERO_INBOUND_DEFECTO="origen.numero.defecto";
	public static final String NUMERO_INBOUND_ORIGEN="origen.numero.";
	
	public static final String CLAVE_DESENCRIPTAR="clave.desencriptar";
	public static final String DURACION_TOKEN="duracion.token";
	
	public static final String MAX_DIAS_DEUDA="maximo.dias.deuda";
	public static final String MAX_ANIOS_CASTIGO="maximo.anios.castigo";

	public static final String C2C_CODIGOS_SERVICIO_EXITOSO_CONSULTARHORARIOATENCION = "C2C.codigos.servicio.exitoso.consultarHorarioAtencion";
	public static final String C2C_CONSULTARHORARIOATENCION_WSDL = "C2C.consultarHorarioAtencion.wsdl";
	public static final String C2C_SOLICITARLLAMADO_WSDL = "C2C.solicitarLlamado.wsdl";
	public static final String C2C_PARAMETRO_SERVICIO_SOLICITARLLAMADO = "C2C.parametro.servicio.solicitarLlamado";
	public static final String C2C_PARAMETRO_TIPOSOLICITUD_SOLICITARLLAMADO = "C2C.parametro.tipoSolicitud.solicitarLlamado";
	public static final String C2C_CODIGOS_SERVICIO_EXITOSO_SOLICITARLLAMADO = "C2C.codigos.servicio.exitoso.solicitarLlamado";
	public static final String VACIO = "";
	public static final String C2C_ACTIVO = "1";
	
	public static final String EMULAR_CAPTCHA = "captchaProduccion";
	public static final String IS_PRODUCCION = "isProduccion";
	
	public static final String CAPTCHA_SITE_PRO = "captcha.site.pro";
	public static final String CAPTCHA_SECRET_PRO = "captcha.secret.pro";
	public static final String CAPTCHA_SITE_LOCAL = "captcha.site.local";
	public static final String CAPTCHA_SECRET_LOCAL = "captcha.secret.local";
	
	public static final String SUBOPERACION_SGA_ECOMMERCE_DEFECTO = "suboperacion.sga.ecommerce.defecto";
	public static final String SUBOPERACION_SGA_ECOMMERCE_ORIGENES_PRIVADO = "suboperacion.sga.ecommerce.origen.privado";
	public static final String SUBOPERACION_SGA_ECOMMERCE_PRIVADO = "suboperacion.sga.ecommerce.privado";
	
}
