package com.esa.ponline.appmobile.web.actions;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.epcs.clientes.evaluacion.types.EvaluacionComercialNecResponseType;
import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.utils.FormatoMovilUtil;
import com.esa.ponline.appmobile.utils.PruebaUtil;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.ConsultaFactibilidadPortVO;
import com.esa.ponline.appmobile.web.bean.DatosComercialesVO;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.esa.ponline.appmobile.web.bean.ReqValidarDatosPortabilidadVO;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosPortabilidadVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cl.esa.ponline.appmobile.delegate.DelegateServices;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.RespuestaConsultaFactPortacionSync;

public class ContratacionPaso3Action extends ActionSupport {
	private static final long serialVersionUID = -2586889159761729857L;
	private static final Logger LOGGER = Logger.getLogger(ContratacionPaso3Action.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	private String paso3Page 			= "ContratacionPaso3";
	private String paso3ErrorPage 		= "ContratacionPaso3Error";
	
	
	private Plan planCO;
	private Cliente clienteCO;
	private List<String> comunas;
	private List<String> regiones;
	
	private DelegateServices delegateServices = new DelegateServices();
	
	
	@Override
	public String execute(){
		String response="";
		HttpServletRequest req = ServletActionContext.getRequest();
		clienteCO = (Cliente) getSession().get("Cliente");

		String codCAP			= Utils.reemplazarCaracteresExtranos(req.getParameter("codCap").toString());
		clienteCO.setCodigoCAP(codCAP);
		//String imei		= req.getParameter("imei").toString();
		//imei se omite su lectura desde la p�gina
		String imei = Config.getAppProperty("imei.por.defecto"); 
		response = contratacionPaso3(codCAP, imei, clienteCO);
		if(!response.equals(paso3ErrorPage)){
			logMetricas.info("PORTAL|ContratacionOnline|TIPO|INFO|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP());
		}
		getSession().put("Cliente", clienteCO);
		return response;
	}

	private String contratacionPaso3(String codCAP, String imei, Cliente clienteCO) {
		String response = paso3ErrorPage;
		setRegiones(regiones);
		
		if(Boolean.valueOf(Config.getAppProperty("switchPRueba"))){
			PruebaUtil.creaDatosPrueba();
			response = paso3Page;
		}else{
			//if(validaDatosPortabilidad(codCAP, imei, clienteCO)){ Se quita validacion de los datos de portabilidad por que la plataforma ONL ya no funciona
				if(factibilidadPortabilidad(codCAP, imei, clienteCO)){
					if(evaluacionComercial(clienteCO)){
						response = paso3Page;
					}
				}
			//}
		}
		return response;
	} 
	
	private boolean validaDatosPortabilidad(String codCAP, String imei, Cliente clienteCO){
		boolean resp = false;
		planCO = (Plan) getSession().get("Plan");
		ReqValidarDatosPortabilidadVO req = new ReqValidarDatosPortabilidadVO();
		ResValidarDatosPortabilidadVO res = new ResValidarDatosPortabilidadVO();
		
		LOGGER.info(Utils.cabeceraLog() + "-------------validarDatosPortabilidad---------------");
		LOGGER.info(Utils.cabeceraLog() + "codCAP: "+ codCAP);
		LOGGER.info(Utils.cabeceraLog() + "MercadoOrigen: "+ clienteCO.getMercadoOrigen());
		LOGGER.info(Utils.cabeceraLog() + "imei: "+ imei);
		LOGGER.info(Utils.cabeceraLog() + "NumeroPorta: "+FormatoMovilUtil.obtieneFormatoMovilPorLargo(clienteCO.getNumeroPorta()) );
		LOGGER.info(Utils.cabeceraLog() + "NumeroNegocio: "+clienteCO.getNumeroNegocio() );
		LOGGER.info(Utils.cabeceraLog() + "-------------validarDatosPortabilidad---------------");
		
		req.setCap(codCAP);
		req.setCodMercadoOrigen(String.valueOf(clienteCO.getMercadoOrigen()));
		req.setCodPlataforma("ONL");
		req.setImei(imei);
		req.setNumeroMovil(FormatoMovilUtil.obtieneFormatoMovilPorLargo(clienteCO.getNumeroPorta()));
		req.setNumeroNegocio("123456789");
		
		clienteCO.setImei(imei);
		try {
			res = delegateServices.validarDatosPortabilidad(req);
			//res.setIndicadorExito("000");
			
			LOGGER.info(Utils.cabeceraLog() + "RESULTADO DE LA CONSULTA DEL SERVICIO: "+res.getIndicadorExito()); 
			LOGGER.info(Utils.cabeceraLog() + "descrici�n: "+res.getDescripcion()); 
			if(res.getIndicadorExito().equals("000")){
				resp = true;
			
			}else if(res.getIndicadorExito().equals("N-10002")){
				//error en el c�digo cap ingresado
				planCO.setIconoErrorC2C("error_triste.svg");
				planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorCodigoCAP"));
				planCO.setSubMensajeErrorC2C("");
				planCO.setTipoError(Config.getAppProperty("url.tipo.error.errorCodigoCAP"));
				planCO.setBtnErrorC2C("Entendido");
				planCO.setActionErrorC2C(planCO.getUrlOrigen());
				
			}else if(res.getIndicadorExito().equals("N-30001")){
				//error en el IMEI ingresado
				planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.imeiIncorrecto"));
				planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.subMensaje2"));
				planCO.setTipoError(Config.getAppProperty("url.tipo.error.imeiIncorrecto"));
			}
			
			if(!resp){
				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR| " + planCO.getMensajeErrorC2C() +"|");
			}
			
		} catch (Exception e) {
			LOGGER.error("Error al llamar servicio Validar Datos Portabilidad");
			planCO.setIconoErrorC2C("error_triste.svg");
			planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
			planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
			planCO.setTipoError(Config.getAppProperty("url.tipo.error"));
			planCO.setBtnErrorC2C("Quiero que me llamen");
			planCO.setActionErrorC2C(Config.getAppProperty("cliick_"+planCO.getCargoPlan().replace(".", "")));
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR| " + Utils.obtenerDetalleError(e) +"|");
			return resp;
		}
		
		return resp;
	}
	
	private boolean factibilidadPortabilidad(String codCAP, String imei, Cliente clienteCO){
		boolean resp = false;
		planCO = (Plan) getSession().get("Plan");
		ConsultaFactibilidadPortVO req = new ConsultaFactibilidadPortVO();
		RespuestaConsultaFactPortacionSync res = new RespuestaConsultaFactPortacionSync();
		
		req.setAni(FormatoMovilUtil.obtieneFormatoMovilPorLargo(clienteCO.getNumeroPorta()));
		req.setIdd("220");
		req.setImei(imei);
		
		try {
			res = delegateServices.consultaFactibilidadPortabilidad(req);
		} catch (Exception e) {
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_SERVICIO_FACTIBILIDAD_PORTACION|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			return resp;
		}
		
		if(res.getErrorCode()==0){
			resp = true;
		}else{
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|FACTIBILIDAD_PORTACION_NEGATIVA|DETALLE|Cliente con Factibilidad de Portacion Negativa");
		}
		return resp;
	}
	
	/**
	 * @param clienteCO
	 * @return
	 */
	private boolean evaluacionComercial(Cliente clienteCO){
		planCO = (Plan) getSession().get("Plan");
		EvaluacionComercialNecResponseType res = new EvaluacionComercialNecResponseType();
		boolean resp = false;
		String[] rutSplit = clienteCO.getRut().split("-");
		String nRut=rutSplit[0];
		String dRut=rutSplit[1];
		DatosComercialesVO datosComercialCliente = null;
		
		try{
			datosComercialCliente = delegateServices.obtenerDatosCliente(clienteCO.getRut().replace("-", ""));
		}catch(Exception e){
			planCO.setIconoErrorC2C("error_triste.svg");
			planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
			planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
			planCO.setBtnErrorC2C("Quiero que me llamen");
			planCO.setActionErrorC2C(Config.getAppProperty("cliick_"+planCO.getCargoPlan().replace(".", "")));
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_SERVICIO_DATOS_FACTURACION|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			return resp;
		}
		
		try{
			res = delegateServices.obtenerEvaluacionComercial(nRut, dRut, clienteCO.getnSerie(), datosComercialCliente);
		}catch(Exception e){
			planCO.setIconoErrorC2C("error_triste.svg");
			planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
			planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
			planCO.setBtnErrorC2C("Quiero que me llamen");
			planCO.setActionErrorC2C(Config.getAppProperty("cliick_"+planCO.getCargoPlan().replace(".", "")));
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_SERVICIO_COMERCIAL|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			return resp;
		}
		
		try {
			
			if(res.getCodigoSalida().equals("0000")){
				if(null != res.getSalidaDicom()){
					int edad = Integer.parseInt(res.getSalidaDicom().getEdad());
					if(!res.getSalidaDicom().getMotivo().equals(Config.getAppProperty("validacion.comercial.serie.valida")) || edad < 18){
						//error al validar la serie
						planCO.setIconoErrorC2C("error_confuso.svg");
						planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.validacionSerie"));
						planCO.setSubMensajeErrorC2C("");
						planCO.setTipoError(Config.getAppProperty("url.tipo.error.validacionSerie"));
						planCO.setBtnErrorC2C("Entendido");
						planCO.setActionErrorC2C(planCO.getUrlOrigen());
						logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_VALIDAR_SERIE|DETALLE|error al validar numero de serie del cliente.|");
						return resp;
					}
				}
				
				//Si es cliente Churn
				if(null!=res.getClienteChurneado() && res.getClienteChurneado().size()!=0 || null!=res.getSalidaDeuda() && Utils.diferenciaAniosVSActual(res.getSalidaDeuda().getDetalleDeuda().get(0).getFechVencim())>=Integer.parseInt(Config.getAppProperty(Constantes.MAX_ANIOS_CASTIGO.toString()))){
					planCO.setIconoErrorC2C("error_fatal.svg");
					planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.comercial"));
					planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.subMensajeComercial"));
					planCO.setTipoError(Config.getAppProperty("url.tipo.error.comercial"));
					planCO.setBtnErrorC2C("Conocer la m&aacute;s cercana");
					planCO.setActionErrorC2C(Constantes.TIENDA.toString());
					logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_CLIENTE_CHURN|DETALLE|Cliente Churn|");
					return resp;
				}
				
				//Si tiene mas de una deuda o una deuda superior o igual a 10 dias
				if(null!=res.getSalidaDeuda() && (res.getSalidaDeuda().getDetalleDeuda().size()>1 || Utils.diferenciaDiasVSActual(res.getSalidaDeuda().getDetalleDeuda().get(0).getFechVencim())>=Integer.parseInt(Config.getAppProperty(Constantes.MAX_DIAS_DEUDA.toString())))){
					planCO.setIconoErrorC2C("error_fatal.svg");
					planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.deudaEntel"));
					planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.subMensajeDeudaEntel"));
					planCO.setTipoError(Config.getAppProperty("url.tipo.error.deudaEntel"));

					planCO.setBtnErrorC2C("Ir a pagar");
					planCO.setActionErrorC2C(Constantes.PAGAR.toString());
					logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_DEUDA_CLIENTE|DETALLE|Cliente con Deuda.|");
					return resp;
				}
				
				//Lineas disponibles > 0
				if(Integer.parseInt(res.getNmroLineasDisponibles())>0){
					//Si es grupo CLIENTE 23 y cargo PLAN
					LOGGER.info(Utils.cabeceraLog() + "El cliente es grupo: "+ res.getCodiGrupoCliente());
					if(!res.getCodiGrupoCliente().equals("23")){
						clienteCO.setGrupoCliente(res.getCodiGrupoCliente());
						resp = true;
					}else{
						
						resp = Utils.comprobarGrupoCliente23(clienteCO, planCO, res.getCodiGrupoCliente());
					                                                                              
						//condicion para a�adir mensaje de pago anticipado en los terminos y condiciones.
						if(planCO.getEsMiPrimerPlan()){
							List<String> terminosYCondiciones = (List<String>)getSession().get("terminosYCondiciones");
							
							String mensajeCobroAnticipado_TyC = Config.getConfigContratacionOnline_Terminos().get(Constantes.MENSAJE_COBRO_ANTICIPADO);
							terminosYCondiciones.add(mensajeCobroAnticipado_TyC);
							
							getSession().put("terminosYCondiciones", terminosYCondiciones);
						}
						
						if(!resp){
							logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_GRUPO_CLIENTE|DETALLE|Grupo Cliente Invalido|");
						}
					}
					
					if(null != res.getNombCliente()){
						LOGGER.info(Utils.cabeceraLog() + " Nombe Cliente: "+ res.getNombCliente());
						List nomreCliente;
						if(!"".equals(res.getNombCliente())){
							nomreCliente = Arrays.asList(res.getNombCliente().split(" "));
						}else{
							String nombreCompleto = clienteCO.getNombre() + " " + clienteCO.getApellidos();
							nomreCliente = Arrays.asList(nombreCompleto.split(" "));
						}
						
						clienteCO.setFirstName(Utils.primeraLetraMayus((String) nomreCliente.get(0)));

						//si el nombre trae mas de cuatro palabras se asume la tercera palabra como el primer apellido.
						if(nomreCliente.size() == 4){
							clienteCO.setLastName(Utils.primeraLetraMayus(nomreCliente.get(2).toString()));
							
							clienteCO.setNombre(Utils.primeraLetraMayus(nomreCliente.get(0).toString() + " " + nomreCliente.get(1).toString()));
							clienteCO.setApellidos(Utils.primeraLetraMayus(nomreCliente.get(2).toString() + " " + nomreCliente.get(3).toString()));
						}else if(nomreCliente.size() == 3){
							clienteCO.setNombre(Utils.primeraLetraMayus(nomreCliente.get(0).toString()));
							clienteCO.setApellidos(Utils.primeraLetraMayus(nomreCliente.get(1).toString() + " " + nomreCliente.get(2).toString()));
							clienteCO.setLastName(Utils.primeraLetraMayus(nomreCliente.get(1).toString()));
						}else if(nomreCliente.size() == 2){
							clienteCO.setLastName(Utils.primeraLetraMayus(nomreCliente.get(1).toString()));
						}else{
							clienteCO.setNombre(clienteCO.getFirstName());
							clienteCO.setApellidos("");
							clienteCO.setLastName("");
							
						}
						
					}
/*					if(res.getClasificacionDicom().equals("C") || res.getClasificacionDicom().equals("D") || !res.getSalidaDicom().getTotalDeuda().equals("0")){
						//se muesta mismo mensaje de sin lineas disponibles.
						planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.sinLineasDispopnibles"));
						planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
						logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|USUARIO|"+clienteCO.getTelefono()+"|RUT|"+  clienteCO.getRut() + "|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso3|NUMEROSERIE|"+clienteCO.getnSerie()+"|NUMERONEGOCIO|"+ clienteCO.getNumeroNegocio() +"|CAPTCHA|true|NUMEROAPORTAR|"+clienteCO.getNumeroPorta() +"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|RESPUESTAEMAIL||CODIGO PLAN|"+ planCO.getCodigoPlan()+"|CARGO FIJO|"+ planCO.getCargoPlan()+ "|GRUPO CLIENTE|" + res.getCodiGrupoCliente() + "|DESCRIPCION ERROR|Cliente con Dicom nivel:"+ res.getClasificacionDicom() +"|");
						return false;
					}*/
					
					
				}else{
					LOGGER.error("NO tiene lineas disponibles.");
					logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_SIN_LINEAS_DISPONIBLES|DETALLE|Cliente sin lineas disponibles.|");
					planCO.setIconoErrorC2C("error_fatal.svg");
					planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.sinLineasDispopnibles"));
					planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.subMensajeComercial"));
					planCO.setTipoError(Config.getAppProperty("url.tipo.error.sinLineasDispopnibles"));
					planCO.setBtnErrorC2C("Conocer la m&aacute;s cercana");
					planCO.setActionErrorC2C(Constantes.TIENDA.toString());
				}
			}
			
		} catch (Exception e) {
			LOGGER.error(Utils.cabeceraLog() + " Error al llamar al servicio de Evaluacion comercial: " + e.getMessage());
			planCO.setIconoErrorC2C("error_triste.svg");
			planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
			planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
			planCO.setTipoError(Config.getAppProperty("url.tipo.error"));
			planCO.setBtnErrorC2C("Quiero que me llamen");
			planCO.setActionErrorC2C(Config.getAppProperty("cliick_"+planCO.getCargoPlan().replace(".", "")));
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" +  Utils.obtenerDetalleError(e) +"|");
		}
		
		getSession().put("Plan", planCO);
		return resp;
	}
	
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}

	public List<String> getComunas() {
		return comunas;
	}

	public void setComunas(List<String> comunas) {
		this.comunas = comunas;
	}
	
	public List<String> getRegiones(){
		return regiones;
	}
	
	public void setRegiones(List<String> regiones){
		this.regiones = regiones;
	}
		
}
