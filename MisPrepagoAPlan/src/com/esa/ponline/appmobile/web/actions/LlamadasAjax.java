package com.esa.ponline.appmobile.web.actions;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.RespuestaType;
import com.google.gson.Gson;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cl.esa.ponline.appmobile.delegate.DelegateServices;

public class LlamadasAjax extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2645863743066475662L;
	private static final Logger LOGGER = Logger.getLogger(LlamadasAjax.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	
	private boolean numSerieValido = false;
	private String code = "0";
	
	public boolean isNumSerieValido() {
		return numSerieValido;
	}

	public void setNumSerieValido(boolean numSerieValido) {
		this.numSerieValido = numSerieValido;
	}
		
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String validarNumSerie(){
		RespuestaType respuestaType = null;
		Gson gson = new Gson();
		DelegateServices delegateServices = new DelegateServices();
		HttpServletRequest req = ServletActionContext.getRequest();
		
		LOGGER.info(Utils.cabeceraLog() + "Accediendo a verificar serie...");
		
		Cliente cliente = (Cliente) getSession().get("Cliente");

		String numSerie = req.getParameter("nSerie");
		String rut = req.getParameter("rut");
		
		if(null == rut){
			rut = cliente.getRut();
		}
		
		numSerie = numSerie != null ? Utils.reemplazarCaracteresExtranos(numSerie.toString()) : "";
		rut = rut != null ? Utils.reemplazarCaracteresExtranos(rut.toString()) : "";
		rut = rut.replace(".", "");
		
		cliente.setRut(rut);
		cliente.setnSerie(numSerie);
		
		try {
			respuestaType = delegateServices.validarNumeroSerie(cliente);
			LOGGER.info(Utils.cabeceraLog() + "Se ha realizado validacion: codigo:" + respuestaType.getCodigo() + " | mensaje:" + respuestaType.getMensaje());
			
			if(respuestaType.getCodigo().equals("0")){
				setNumSerieValido(true);
				cliente.setSerieValida(true);
			}else{
				setCode(respuestaType.getCodigo());
			}
		
		} catch (Exception e) {
			LOGGER.error(Utils.cabeceraLog() + "Error al validar serie: " + Utils.obtenerDetalleError(e));
		}
		getSession().put("Cliente", cliente);
		return Action.SUCCESS;
	}
	
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}

	
	
}
