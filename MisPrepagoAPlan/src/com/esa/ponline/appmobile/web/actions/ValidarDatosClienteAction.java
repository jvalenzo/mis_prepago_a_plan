package com.esa.ponline.appmobile.web.actions;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosClientesSIMMAUTVO;
import com.esa.ponline.appmobile.web.bean.ValidaDatosClientesVO;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;

import cl.esa.ponline.appmobile.delegate.DelegateServices;

public class ValidarDatosClienteAction {
	private static final Logger LOGGER = Logger.getLogger(ContratacionAction.class);
	private boolean numSerieValido = false;
		
	public boolean isNumSerieValido() {
		return numSerieValido;
	}

	public void setNumSerieValido(boolean numSerieValido) {
		this.numSerieValido = numSerieValido;
	}



	public String validarDatosCliente(){
		HttpServletRequest req = ServletActionContext.getRequest();
		DelegateServices delegateServices = new DelegateServices();
		ResValidarDatosClientesSIMMAUTVO validarDatosCliente = new ResValidarDatosClientesSIMMAUTVO();
        ValidaDatosClientesVO validaDatosClientes = new ValidaDatosClientesVO();
        
        String nSerie = req.getParameter("nSerie");
		String rut = req.getParameter("rut");
		
		nSerie = nSerie != null ? Utils.reemplazarCaracteresExtranos(nSerie.toString()) : "";
		rut = rut != null ? Utils.reemplazarCaracteresExtranos(rut.toString()) : "";
		
		String[] rutSplited = rut.replace(".", "").split("-");
		String nRut = "";
		String dRut = "";
        if(rutSplited.length > 1){
	        nRut			= rutSplited[0];
	        dRut         	= rutSplited[1];
	        
        }else{
        	LOGGER.info(Utils.cabeceraLog() + "Error al procesar rut: " + rut);
        }
        
        validaDatosClientes.setCodPlataforma("ONL");
		validaDatosClientes.setNumeroMovilVirtual("");
		validaDatosClientes.setNumeroSerie(nSerie);
		validaDatosClientes.setTipoNegocio(2);
		validaDatosClientes.setDigitoVerificador(dRut);
		validaDatosClientes.setRutClienteSinDV(Integer.valueOf(nRut));
		Cliente clienteCO = (Cliente)getSession().get("Cliente");
		
		try {
			validarDatosCliente = delegateServices.validarDatosClientesSimAut(validaDatosClientes);
			List codigosErrorNumSerie = Arrays.asList(Config.getAppProperty("codigos.error.validacion.serie").split(","));
			
			String indicador = validarDatosCliente.getRespuestaValidarCliente().getIndicadorExito();
			String descripcion = validarDatosCliente.getRespuestaValidarCliente().getDescription();
			
			LOGGER.info(Utils.cabeceraLog() + "*******RESPUESTA SERVICIO VALDIACION CLIENTE******");
			LOGGER.info(Utils.cabeceraLog() + "IndicadorExito: " + indicador);
			LOGGER.info(Utils.cabeceraLog() + "Descripcion: " + descripcion);
			
			if(null == clienteCO){
				clienteCO = new Cliente();
			}
			clienteCO.getValidacionCliente().setIndicadorExito(indicador);
			clienteCO.getValidacionCliente().setDescripcionValidacion(descripcion);
			if(null != indicador && !codigosErrorNumSerie.contains(indicador)){
	        	setNumSerieValido(true);
	        	clienteCO.getValidacionCliente().setNombresCliente(validarDatosCliente.getRespuestaValidarCliente().getNombresCliente());
				clienteCO.getValidacionCliente().setApellidos(validarDatosCliente.getRespuestaValidarCliente().getApellidoPaterno());
				clienteCO.getValidacionCliente().setNumeroNegocio(validarDatosCliente.getRespuestaValidarCliente().getNumeroNegocio());
	        }
			
		} catch (Exception e) {
			LOGGER.error(Utils.cabeceraLog() + "Ha ocurrido un error obteniendo información del Numero de serie del cliente");
			setNumSerieValido(false);
		}
		getSession().put("Cliente", clienteCO);
		return Action.SUCCESS;
	}
	
	
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}


}
