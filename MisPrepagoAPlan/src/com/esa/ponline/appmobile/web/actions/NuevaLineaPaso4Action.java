package com.esa.ponline.appmobile.web.actions;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.epcs.clientes.evaluacion.types.EvaluacionComercialNecResponseType;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.ServiceException;
import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.email.MailDatosHelper;
import com.esa.ponline.appmobile.email.MailType;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.DataEmailVO;
import com.esa.ponline.appmobile.web.bean.DatosComercialesVO;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.esa.ponline.appmobile.web.bean.ResTicketSGAVO;
import com.esa.ponline.appmobile.web.bean.TicketSGAVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cl.esa.ponline.appmobile.delegate.DelegateServices;

public class NuevaLineaPaso4Action extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = -269262279008923256L;
	private static final Logger LOGGER = Logger.getLogger(NuevaLineaPaso4Action.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	private String paso4Page 			= "NuevaLineaPaso4";
	private String paso4ErrorPage 		= "NuevaLineaPaso4Error";
	private String url = "https://www.entel.cl/planes/";
	private Plan planCO;
	private Cliente clienteCO;
	
	private DelegateServices delegateServices = new DelegateServices();
	@Override
	public String execute(){
		
		clienteCO = (Cliente) getSession().get("Cliente");
		if(clienteCO.getFinalizoFlujo()){
			return "redirect";
		}
		clienteCO.setFinalizoFlujo(true);
		
		String response=paso4ErrorPage;
		HttpServletRequest req = ServletActionContext.getRequest();
		String nSerie		= Utils.reemplazarCaracteresExtranos(req.getParameter("nSerie"));
		String nombre		= Utils.reemplazarCaracteresExtranos(req.getParameter("nombre"));
		String apellido		= Utils.reemplazarCaracteresExtranos(req.getParameter("apellido"));
		String email		= "";
		
		clienteCO = (Cliente) getSession().get("Cliente");
		planCO = (Plan) getSession().get("Plan");
		clienteCO.setNombre(nombre);
		clienteCO.setApellidos(apellido);
		
		 try {	
	        	if(clienteCO.getSerieValida() && clienteCO.getnSerie().equals(nSerie)){
	        		LOGGER.info(Utils.cabeceraLog() + "Serie coincide con la validada previamente");
	        	}else{
	        		LOGGER.info(Utils.cabeceraLog() + "Serie no corresponde a la validada: [SerieValida|" + clienteCO.getSerieValida() + "][rut|"+ clienteCO.getRut() + "][numeroSerie|" + clienteCO.getnSerie() + "|" + nSerie + "]");
	        		throw new Exception("Serie no corresponde a la validada: [SerieValida|" + clienteCO.getSerieValida() + "][rut|"+ clienteCO.getRut() + "][numeroSerie|" + clienteCO.getnSerie() + "|" + nSerie + "]");
	        	}
	        	
			} catch (Exception e) {
				//error de servicio
				planCO.setIconoErrorC2C("error_triste.svg");
	        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
				planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
				planCO.setTipoError(Config.getAppProperty("url.tipo.error"));
				planCO.setBtnErrorC2C("Quiero que me llamen");
				planCO.setActionErrorC2C("$('#modalSeleccionFlujo').modal('show');");
				LOGGER.error(Utils.cabeceraLog() + "Serie no corresponde a la validada: [SerieValida|" + clienteCO.getSerieValida() + "][rut|"+ clienteCO.getRut() + "][numeroSerie|" + clienteCO.getnSerie() + "|" + nSerie + "]");
				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+ clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+email+"|NUMEROAPORTAR||COMPANIA||MERCADO|"+ clienteCO.getMercadoOrigen() + "|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR| " + Utils.obtenerDetalleError(e) +"|");
				return paso4ErrorPage;
			}
		
		
		if(planCO.getTest().equals("B")){
			email		= Utils.reemplazarCaracteresExtranos(req.getParameter("email"));
			clienteCO.setEmail(email);
		}
		
		try{
			
			if(!evaluacionComercial(clienteCO, planCO)){
				return response;
			}
			
	        response = contratacionPaso5(clienteCO, planCO);
			
		}catch(Exception e){
			clienteCO.setFinalizoFlujo(false);
			LOGGER.error("Error al procesar paso final: "+ Utils.obtenerDetalleError(e));
			logMetricas.error("PORTAL|MisPrepagoAPlan|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
		}
				
		return response;
	}
	
	private String contratacionPaso5(Cliente clienteCO, Plan planCO) {
		String resp=paso4ErrorPage;
		String requerimiento="";
		
		//booleano para indicar si esta en producci�n, esto ayuda a que en fase de desarrollo podamos finalizar el flujo.
		Boolean esProduccion = Boolean.valueOf(Config.getAppProperty("captchaProduccion"));
		
		if(clienteCO.getSeguridad().checkSeguridad()){

			String subOperacion=Utils.getSubOperacion(planCO.getOrigenOferta());
			
			//GENERO TICKET
			if(!esProduccion){
				requerimiento=	 " Solicitud:NuevaLinea "+
						" - Hora/Fecha Solicitud: "+ Utils.getFechaHoraMinutoSistema()+
						" - Nombre: PRUEBA"+
					 	" - RUT: PRUEBA"+ 
					 	" - NumSerie: PRUEBA"+
					 	" - E-Mail: PRUEBA"+
					 	" - Telefono: PRUEBA"+
					 	" - NombrePlan: PRUEBA"+
					 	" - CodigoPlan: PRUEBA"+
					 	" - DatosPlan: PRUEBA"+
					 	" - MinutosPlan: PRUEBA"+
					 	" - CargoFijo: PRUEBA"+
					 	" - Flujo: NUEVA LINEA Contratacion Online Movil"+
					 	" - Motivo: Nuevas Lineas"+
					 	" - PortalOrigen: PRUEBA"+
					 	" - Numero Negocio: PRUEBA"+
					 	" - Grupo Cliente: PRUEBA"+
					 	" - DireccionDespacho: PRUEBA"+
					 	" - Origen Web: "+ planCO.getOrigenOferta()+
					 	" - SubOperacion: "+ subOperacion+
					 	" ";
						if(Double.valueOf(planCO.getCargoPlanOferta())>0){
						requerimiento+=" - Recuerda realizar el cambio de plan a linea actual al Plan juntos "+planCO.getDatosPlan()+" GB";
						}
						if(null!=getSession().get("OfertaEspecifica") && getSession().get("OfertaEspecifica").equals(Config.getAppProperty(Constantes.OFERTA_ESPECIFICA_MENSAJE_CAMBIAR))){
						requerimiento+=" - Recuerda cambiar el plan actual a $"+planCO.getCargoPlan()+" + Linea Adicional de "+planCO.getDatosPlan()+"GB Promocion "+planCO.getPorcentajeDsctoEspecifico()+"% en lineas adicionales";
						}
			}else{		
			requerimiento=	 " Solicitud:NuevaLinea "+
													" - Hora/Fecha Solicitud: "+ Utils.getFechaHoraMinutoSistema()+
													" - Nombre: "+ clienteCO.getNombre() + " " + clienteCO.getApellidos()+
												 	" - RUT: "+ clienteCO.getRut() +
												 	" - NumSerie: "+ clienteCO.getnSerie()+
												 	" - E-Mail: "+ clienteCO.getEmail()+
												 	" - Telefono: "+ clienteCO.getTelefono()+
												 	" - NombrePlan: "+ planCO.getNombrePlan2() +
												 	" - CodigoPlan: "+ planCO.getCodigoPlan() +
												 	" - DatosPlan: "+ planCO.getDatosPlan() +"GB"+
												 	" - MinutosPlan: "+ planCO.getMinutosPlan()+
												 	" - CargoFijo: "+ planCO.getCargoPlan()+
												 	" - Flujo: NUEVA LINEA Contratacion Online Movil"+
												 	" - Motivo: Nuevas Lineas"+
												 	" - PortalOrigen: P�blico Mobile"+
												 	" - Numero Negocio: "+ clienteCO.getNumeroNegocio()+
												 	" - Grupo Cliente: "+ clienteCO.getGrupoCliente()+
												 	" - DireccionDespacho:"+ clienteCO.getCalleDespacho() + " #"+ clienteCO.getNumeroDespacho() +" "+ clienteCO.getComunaDespacho() +", "+ clienteCO.getRegionDespacho()+
												 	" - Origen Web: "+ planCO.getOrigenOferta()+
												 	" - SubOperacion: "+ subOperacion+
												 	" ";
				if(Double.valueOf(planCO.getCargoPlanOferta())>0){
					requerimiento+=" - Recuerda realizar el cambio de plan a linea actual al Plan juntos "+planCO.getDatosPlan()+" GB";
				}
				if(null!=getSession().get("OfertaEspecifica") && getSession().get("OfertaEspecifica").equals(Config.getAppProperty(Constantes.OFERTA_ESPECIFICA_MENSAJE_CAMBIAR))){
					requerimiento+=" - Recuerda cambiar el plan actual a $"+planCO.getCargoPlan()+" + Linea Adicional de "+planCO.getDatosPlan()+"GB Promocion "+planCO.getPorcentajeDsctoEspecifico()+"% en lineas adicionales";
				}
			}
			
	//		LOGGER.info(Utils.cabeceraLog() + "REQUERIMIENTO:" +requerimiento);
			String respuestaEmail = "";	
			
			if(esProduccion){
				if(generarTicket(clienteCO,requerimiento,planCO.getOrigenOferta())){
					resp=paso4Page;
					//ENVIO EMAIL
								
					try {
						respuestaEmail = envioMail("no-responder@entel.cl", "Bienvenido a Entel", clienteCO.getEmail());			
					} catch (Exception e) {
						LOGGER.error("Error al enviar el Email, detalle error: "+ Utils.obtenerDetalleError(e));
						e.printStackTrace();
					}
					
					if(respuestaEmail.equals("NoOk")){
						LOGGER.error("Env�o de correo fallido, revisar servicio o plantilla de correo");
					}
				}
			}else{
				resp=paso4Page;
				//ENVIO EMAIL
							
				try {
					respuestaEmail = envioMail("no-responder@entel.cl", "Bienvenido a Entel", clienteCO.getEmail());			
				} catch (Exception e) {
					LOGGER.error("Error al enviar el Email");
					e.printStackTrace();
				}
				
				if(respuestaEmail.equals("NoOk")){
					LOGGER.error("Env�o de correo fallido, revisar servicio o plantilla de correo");
				}
			}
			
			//Logica del creacion de fichero y envio a FTP
			//Datos del FTP
			
			//Boolean que indica si esta en modo de prueba
			Boolean esModoPruebaFichero = Boolean.valueOf(Config.getAppProperty("modoPruebaFichero"));
			
			FTPClient client = new FTPClient();
	        String ftp = Config.getAppProperty("urlServidor"); 
	        String user = Config.getAppProperty("usuario");
	        String password = Config.getAppProperty("contrase�a");
			
	        String nombreFichero = new SimpleDateFormat("ddMMyyyyHHmm'.txt'").format(new Date());
	        
	        String ruta = Config.getAppProperty("rutaCreacionArchivo");
			String directorio = ruta + clienteCO.getTelefono()+"_"+nombreFichero;
			
			//Datos a llenar en el fichero
			String text = Utils.getFechaHoraMinutoSistema() + ","
					+ planCO.getOrigenOferta() + ","
					+ clienteCO.getNombre() + ","
					+ clienteCO.getApellidos()+ ","
					+ clienteCO.getRut()+ ","
					+ clienteCO.getnSerie()+ "," 
					+ "56"+clienteCO.getTelefono()+ ","
					+ clienteCO.getEmail()+ ","
					+ clienteCO.getRegionDespacho()+ ","
					+ clienteCO.getComunaDespacho()+ ","
					+ clienteCO.getCalleDespacho()+ ","
					+ clienteCO.getNumeroCalle()+ ","
					+ clienteCO.getDireccionAdicional() + ","
					+ planCO.getNombrePlanCompleto() + ","
					+ planCO.getCargoPlan();
			
			BufferedWriter bw = null;
			File fileTemp = null;
			File directionTemp = null;
			
			try {

				directionTemp = new File(ruta);
				// Make Folder
				if (!directionTemp.exists()) {
					directionTemp.mkdirs();
					LOGGER.info("[Paso 4: Carpeta creada]");
				}
				fileTemp = new File(directorio);

				if (!fileTemp.exists()) {
					fileTemp.createNewFile();
					LOGGER.info("[Paso 4: Archivo creado : "+fileTemp+"]");
				}
				bw = new BufferedWriter(new FileWriter(fileTemp));
				// Escritura del Archivo
				if (!IsNullOrEmpty(text)) {
					bw.write(text);
					LOGGER.info("[Paso 4: Archivo escrito]");
				}
				bw.close();
				LOGGER.info("[Paso 4: Archivo Finalizado]");
			} catch (Exception e) {
				LOGGER.error("[Paso 4: Error al generar archivo" + e.getMessage() + "]");
			} finally {
				try {
					if (null != bw) {
						bw.close();
					}
				} catch (Exception e2) {
					LOGGER.error("[Paso 4 Error: " + e2.getMessage() + "]");
				}
			}
			
			//Valida si esta en produccion para enviar al FTP
			if(!esModoPruebaFichero) {
				try {
					client.connect(ftp);
					if (client.login(user, password)) {
						client.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
						client.setFileTransferMode(FTP.BINARY_FILE_TYPE);
						client.enterLocalPassiveMode();

						String filename = clienteCO.getTelefono();

						InputStream fis = new BufferedInputStream(new FileInputStream(ruta + filename));

						if (client.storeFile(Config.getAppProperty("rutaDirectorioRpa") + filename, fis)) {
							LOGGER.info("[Paso 4: Se ha grabado el archivo de salida en FTP.]");
						} else {
							LOGGER.error("[Paso 4: No se ha podido guardar el archivo de salida enFTP.]");
						}

						fis.close();
						client.logout();
						client.disconnect();
					}

				} catch (IOException e) {
					LOGGER.error("[Paso 4: Error en creacion de archivo de salida y subida a ftp.]|" + e);
				}

				boolean estatus = fileTemp.delete();

				if (!estatus) {
					LOGGER.error("[Paso 4: Error no se ha podido eliminar el archivo de la ruta local.]");
				} else {
					LOGGER.info("[Paso 4: Se ha eliminado el archivo de la ruta local exitosamente.]");
				}
			}else {
				LOGGER.info("Modo de prueba activado. El fichero se creo localmente");
			}
				
			//Fin de logica de fichero y envio a FTP
		}
		
		if(!resp.equals(paso4ErrorPage)){
			logMetricas.info("PORTAL|MisPrepagoAPlan|TIPO|INFO|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|NUMEROAPORTAR||COMPANIA||CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|");
		}else{
			planCO.setIconoErrorC2C("error_triste.svg");
			planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
			planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
			planCO.setTipoError(Config.getAppProperty("url.tipo.error"));
			planCO.setBtnErrorC2C("Quiero que me llamen");
			planCO.setActionErrorC2C("$('#modalSeleccionFlujo').modal('show');");
			getSession().put("Plan", planCO);
			

		}
	
		return resp;
	}

	private boolean generarTicket(Cliente clienteCO, String requerimiento,String origen){
		boolean resp=false;
		TicketSGAVO ticket = new TicketSGAVO();
		ResTicketSGAVO resTicket = new ResTicketSGAVO();
		if(clienteCO.getNombre()!=null && clienteCO.getRut()!=null){
			ticket.setSuboperacion(Utils.getSubOperacion(origen));
			ticket.setRequerimientoCliente(requerimiento);
			//NuevaLinea
			ticket.setMotivo("Nuevas Lineas Flujo Digital");
		}
		try {
			if(null != ticket){
				resTicket = delegateServices.ticketSGA(ticket);
			}
		} catch (Exception e) {
			logMetricas.error("PORTAL|MisPrepagoAPlan|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_SERVICIO_TICKET_SGA|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			return resp;
			
		}
		if(resTicket.getCodigo().equals("0000")){
			resp = true;
		}else{
			LOGGER.error("TicketSGA cod. "+resTicket.getCodigo()+" Mensaje: "+ resTicket.getMensaje());
			logMetricas.error("PORTAL|MisPrepagoAPlan|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_TICKET_SGA|DETALLE|" + resTicket.getMensaje() +"|");
		}
		
		return resp;
		
	}
	
	private String envioMail(String remitente,String asunto, String to) throws Exception {
	 	String respuesta = "NoOk";
		LOGGER.info(Utils.cabeceraLog() + "Ingresa a cuerpoEmailConfirmacion para el envio de correo de: remitente" + remitente + ", asunto "+asunto+", to "+ to);

		MailType objMailType = MailDatosHelper.getDatosMail();
		String strCuerpoEMail = MailDatosHelper.cuerpoEmailConfirmacion(objMailType);
		List<DataEmailVO> adjuntos = new ArrayList<DataEmailVO>();
		respuesta = delegateServices.enviaEmail(to, remitente, asunto,strCuerpoEMail, adjuntos );
		
		return respuesta;
		
	}
	
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}
	
//	private boolean validarDatosCliente(){
//		boolean validaDatos = false;
//
//        try {
//        	        
//        	validaDatos = Utils.validarDatosCliente(clienteCO.getValidacionCliente().getIndicadorExito() , planCO);
//        	
//        	if(validaDatos){
//        		clienteCO.setNombre(Utils.primeraLetraMayus(clienteCO.getValidacionCliente().getNombresCliente()));
//    			clienteCO.setApellidos(Utils.primeraLetraMayus(clienteCO.getValidacionCliente().getApellidos()));
//    			clienteCO.setNumeroNegocio(clienteCO.getValidacionCliente().getNumeroNegocio());
//        	}
//        	
//		} catch (Exception e) {
//			//error de servicio
//			planCO.setIconoErrorC2C("error_triste.svg");
//        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
//			planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
//			planCO.setBtnErrorC2C("Quiero que me llamen");
//			planCO.setActionErrorC2C(Config.getAppProperty("cliick_"+planCO.getCargoPlan().replace(".", "")));
//			LOGGER.error("Error Servicio Evaluacion Comercial | Rut : " + clienteCO.getRut() + "|"+Utils.obtenerDetalleError(e));
//			logMetricas.error("PORTAL|MisPrepagoAPlan|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1B|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR| " + Utils.obtenerDetalleError(e) +"|");
//		}
//        return validaDatos;
//	}
	
	private boolean evaluacionComercial(Cliente clienteCO, Plan plan){
		EvaluacionComercialNecResponseType res = new EvaluacionComercialNecResponseType();
		boolean resp = false;
		clienteCO.getSeguridad().setComercial(false);
		clienteCO.getSeguridad().setSerie(false);
		String[] rutSplit = clienteCO.getRut().split("-");
		String nRut=rutSplit[0];
		String dRut=rutSplit[1];
		DatosComercialesVO datosComercialCliente;
		
		try{
			datosComercialCliente = delegateServices.obtenerDatosCliente(clienteCO.getRut().replace("-", ""));
		}catch(Exception e){
			LOGGER.error("Error al llamar al servicio de Evaluacion comercial");
			plan.setIconoErrorC2C("error_triste.svg");
			plan.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
			plan.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
			plan.setBtnErrorC2C("Quiero que me llamen");
			plan.setActionErrorC2C("$('#modalSeleccionFlujo').modal('show');");
			logMetricas.error("PORTAL|MisPrepagoAPlan|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+plan.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_SERVICIO_DATOS_FACTURACION|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			return resp;
		}
		
		try{
			res = delegateServices.obtenerEvaluacionComercial(nRut, dRut, clienteCO.getnSerie() , datosComercialCliente);
		}catch(Exception e){
			LOGGER.error("Error al llamar al servicio de Evaluacion comercial");
			plan.setIconoErrorC2C("error_triste.svg");
			plan.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
			plan.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
			plan.setBtnErrorC2C("Quiero que me llamen");
			plan.setActionErrorC2C("$('#modalSeleccionFlujo').modal('show');");
			logMetricas.error("PORTAL|MisPrepagoAPlan|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+plan.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_SERVICIO_COMERCIAL|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			return resp;
		}
		
		try {
			if(res.getCodigoSalida().equals("0000")){
				if(null != res.getSalidaDicom()){
					int edad = Integer.parseInt(res.getSalidaDicom().getEdad());
					if(!res.getSalidaDicom().getMotivo().equals(Config.getAppProperty("validacion.comercial.serie.valida")) || edad < 18){
						//error al validar la serie
						plan.setIconoErrorC2C("error_confuso.svg");
						plan.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.validacionSerie"));
						plan.setTipoError(Config.getAppProperty("url.tipo.error.validacionSerie"));
						plan.setSubMensajeErrorC2C("");
						plan.setBtnErrorC2C("Entendido");
						plan.setActionErrorC2C("location.href='" + plan.getUrlOrigen() + "'");
						logMetricas.error("PORTAL|MisPrepagoAPlan|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+plan.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_VALIDAR_SERIE|DETALLE|error al validar numero de serie del cliente.|");
						return resp;
					}
				}
				
				//Si es cliente Churn
				if(null!=res.getClienteChurneado() && res.getClienteChurneado().size()!=0 || null!=res.getSalidaDeuda() && Utils.diferenciaAniosVSActual(res.getSalidaDeuda().getDetalleDeuda().get(0).getFechVencim())>=Integer.parseInt(Config.getAppProperty(Constantes.MAX_ANIOS_CASTIGO.toString()))){
					plan.setIconoErrorC2C("error_fatal.svg");
					plan.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.comercial"));
					plan.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.subMensajeComercial"));
					plan.setTipoError(Config.getAppProperty("url.tipo.error.comercial"));
					plan.setBtnErrorC2C("Conocer la m&aacute;s cercana");
					plan.setActionErrorC2C(Constantes.TIENDA.toString());
					logMetricas.error("PORTAL|MisPrepagoAPlan|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+plan.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_CLIENTE_CHURN|DETALLE|Cliente Churn|");
					return resp;
				}
				
				//Si tiene mas de una deuda o una deuda superior o igual a 10 dias
				if(null!=res.getSalidaDeuda() && (res.getSalidaDeuda().getDetalleDeuda().size()>1 || Utils.diferenciaDiasVSActual(res.getSalidaDeuda().getDetalleDeuda().get(0).getFechVencim())>=Integer.parseInt(Config.getAppProperty(Constantes.MAX_DIAS_DEUDA.toString())))){
					plan.setIconoErrorC2C("error_fatal.svg");
					plan.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.deudaEntel"));
					plan.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.subMensajeDeudaEntel"));
					plan.setTipoError(Config.getAppProperty("url.tipo.error.deudaEntel"));

					plan.setBtnErrorC2C("Ir a pagar");
					plan.setActionErrorC2C(Constantes.PAGAR.toString());
					logMetricas.error("PORTAL|MisPrepagoAPlan|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+plan.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_DEUDA_CLIENTE|DETALLE|Cliente con Deuda.|");
					return resp;
				}
				
				clienteCO.setLineasDisponibles(res.getNmroLineasDisponibles());
				//Lineas disponibles > 0
				if(Integer.parseInt(res.getNmroLineasDisponibles())>0){
					if(res.getCodiGrupoCliente().equals("23")){
							resp = Utils.comprobarGrupoCliente23(clienteCO, plan, res.getCodiGrupoCliente());
							if(!resp){
								logMetricas.error("PORTAL|MisPrepagoAPlan|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso2|TEST|"+plan.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_GRUPO_CLIENTE|DETALLE|Grupo Cliente Invalido|");
							}
					}else{
						clienteCO.setGrupoCliente(res.getCodiGrupoCliente());
						clienteCO.getSeguridad().setComercial(true);
						clienteCO.getSeguridad().setSerie(true);
						resp = true;
					}
					
					if(null != res.getNombCliente()){
						
						List nomreCliente;
						if(!"".equals(res.getNombCliente())){
							nomreCliente = Arrays.asList(res.getNombCliente().split(" "));
						}else{
							String nombreCompleto = clienteCO.getNombre() + " " + clienteCO.getApellidos();
							nomreCliente = Arrays.asList(nombreCompleto.split(" "));
						}
						
						clienteCO.setFirstName(Utils.primeraLetraMayus((String) nomreCliente.get(0)));

						//si el nombre trae mas de cuatro palabras se asume la tercera palabra como el primer apellido.
						if(nomreCliente.size() == 4){
							clienteCO.setLastName(Utils.primeraLetraMayus(nomreCliente.get(2).toString()));
							
							clienteCO.setNombre(Utils.primeraLetraMayus(nomreCliente.get(0).toString() + " " + nomreCliente.get(1).toString()));
							clienteCO.setApellidos(Utils.primeraLetraMayus(nomreCliente.get(2).toString() + " " + nomreCliente.get(3).toString()));
						}else if(nomreCliente.size() == 3){
							clienteCO.setNombre(Utils.primeraLetraMayus(nomreCliente.get(0).toString()));
							clienteCO.setApellidos(Utils.primeraLetraMayus(nomreCliente.get(1).toString() + " " + nomreCliente.get(2).toString()));
							clienteCO.setLastName(Utils.primeraLetraMayus(nomreCliente.get(1).toString()));
						}else if(nomreCliente.size() == 2){
							clienteCO.setLastName(Utils.primeraLetraMayus(nomreCliente.get(1).toString()));
						}else{
							clienteCO.setNombre(clienteCO.getFirstName());
							clienteCO.setApellidos("");
							clienteCO.setLastName("");
							
						}
						
					}
					
					
					/*if(res.getClasificacionDicom().equals("C") || res.getClasificacionDicom().equals("D") || !res.getSalidaDicom().getTotalDeuda().equals("0")){
						//se muesta mismo mensaje de sin lineas disponibles.
						plan.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.sinLineasDispopnibles"));
						plan.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
						logMetricas.error("PORTAL|MisPrepagoAPlan|TIPO|ERROR|USUARIO|"+clienteCO.getTelefono()+"|RUT|"+  clienteCO.getRut() + "|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso2|NUMEROSERIE|"+clienteCO.getnSerie()+"|NUMERONEGOCIO|"+ clienteCO.getNumeroNegocio() +"|CAPTCHA|true|NUMEROAPORTAR|"+clienteCO.getNumeroPorta() +"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|RESPUESTAEMAIL||CODIGO PLAN|"+ plan.getCodigoPlan()+"|CARGO FIJO|"+ plan.getCargoPlan()+ "|GRUPO CLIENTE|" + res.getCodiGrupoCliente() + "|DESCRIPCION ERROR|Cliente con Dicom nivel:"+ res.getClasificacionDicom() +"|");
						return false;
					}*/
				}else{
					LOGGER.error("NO tiene lineas disponibles.");
					plan.setIconoErrorC2C("error_fatal.svg");
					plan.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.sinLineasDispopnibles"));
					plan.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.subMensajeComercial"));
					plan.setTipoError(Config.getAppProperty("url.tipo.error.sinLineasDispopnibles"));
					plan.setBtnErrorC2C("Conocer la m&aacute;s cercana");
					plan.setActionErrorC2C(Constantes.TIENDA.toString());
					logMetricas.error("PORTAL|MisPrepagoAPlan|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+plan.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_SIN_LINEAS_DISPONIBLES|DETALLE|Cliente sin lineas disponibles.|");
				}
			}
			
		} catch (Exception e) {
			clienteCO.setFinalizoFlujo(false);
			LOGGER.error("Error al llamar al servicio de Evaluacion comercial");
			plan.setIconoErrorC2C("error_triste.svg");
			plan.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
			plan.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
			plan.setTipoError(Config.getAppProperty("url.tipo.error"));
			plan.setBtnErrorC2C("Quiero que me llamen");
			plan.setActionErrorC2C("$('#modalSeleccionFlujo').modal('show');");
			logMetricas.error("PORTAL|MisPrepagoAPlan|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+plan.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
		}
		
		return resp;
	}
	
	private boolean IsNullOrEmpty(String text) {
		return !(text != null && !text.isEmpty() && !text.equalsIgnoreCase(""));
	}
	
	public String getUrl() {
		return url;
	}
}
