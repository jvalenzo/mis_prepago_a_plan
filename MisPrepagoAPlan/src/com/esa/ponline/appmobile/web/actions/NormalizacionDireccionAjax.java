package com.esa.ponline.appmobile.web.actions;



import java.util.Collections;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.tempuri.ArrayOfCalle;
import org.tempuri.ArrayOfComuna;
import org.tempuri.ArrayOfDireccion;
import org.tempuri.ArrayOfRegion;

import com.esa.ponline.appmobile.utils.ComunaComparator;
import com.esa.ponline.appmobile.utils.RegionComparator;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.google.gson.Gson;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

import cl.esa.ponline.appmobile.delegate.DelegateServices;

public class NormalizacionDireccionAjax extends ActionSupport {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8997894870928780266L;
	private DelegateServices delegateServices = new DelegateServices();
	private static final Logger LOGGER = Logger.getLogger(NormalizacionDireccionAjax.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	private String jsonRegiones;
	private String jsonComunas;
	private String jsonCalles;
	private String jsonNumeros;
	
	private String idRegion;
	private String idComuna;
	private String idCalle;
	
	public String obtenerRegiones(){
		HttpServletRequest req = ServletActionContext.getRequest();
		HttpSession session=req.getSession();
		Cliente cliente=(Cliente)session.getAttribute("Cliente");
		Plan plan=(Plan)session.getAttribute("Plan");
	//Obtener Regiones
		Gson gson = new Gson();
		LOGGER.info(Utils.cabeceraLog() + "LLamando al servicio de REGIONES XYGO");
		ArrayOfRegion regiones = null;
		try {
			regiones = delegateServices.obtenerListaRegiones();
		} catch (Exception e) {
			if(cliente.getFlujo().equals("NuevaLinea")){
				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ cliente.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+plan.getTest()+"|USUARIO|"+cliente.getTelefono()+"|GRUPOCLIENTE|"+cliente.getGrupoCliente()+"|RUT|"+cliente.getRut()+"|NUMEROSERIE|"+cliente.getnSerie()+"|EMAIL|"+cliente.getEmail()+ "|NUMEROAPORTAR||COMPANIA||MERCADO||CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ cliente.getRegionDespacho()+"|COMUNA|"+ cliente.getComunaDespacho()+"|CALLE|"+ cliente.getCalleDespacho()+"|NUMERO|"+ cliente.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_SERVICIO_REGION|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			}else{
				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ cliente.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+plan.getTest()+"|USUARIO|"+cliente.getTelefono()+"|GRUPOCLIENTE|"+cliente.getGrupoCliente()+"|RUT|"+cliente.getRut()+"|NUMEROSERIE|"+cliente.getnSerie()+"|EMAIL|"+cliente.getEmail()+"|NUMEROAPORTAR|"+cliente.getNumeroPorta()+"|COMPANIA|"+cliente.getCompaniaOrigen()+ "|MERCADO|"+ cliente.getMercadoOrigen() + "|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ cliente.getRegionDespacho()+"|COMUNA|"+ cliente.getComunaDespacho()+"|CALLE|"+ cliente.getCalleDespacho()+"|NUMERO|"+ cliente.getNumeroDespacho()+"|CAP-SEGURIDAD|"+cliente.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_SERVICIO_REGION|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			}
			LOGGER.error("Error al llamar al servicio de REGIONES XYGO");
			e.printStackTrace();
			return Action.ERROR;
		}
		Collections.sort(regiones.getRegion(), new RegionComparator());
		String jsonString = gson.toJson(regiones.getRegion());

		setJsonRegiones(jsonString);
		
		return Action.SUCCESS;
	}
	
	public String obtenerComunas(){
		HttpServletRequest req = ServletActionContext.getRequest();
		HttpSession session=req.getSession();
		Cliente cliente=(Cliente)session.getAttribute("Cliente");
		Plan plan=(Plan)session.getAttribute("Plan");
		//Obtener Comunas
		Gson gson = new Gson();
		LOGGER.info(Utils.cabeceraLog() + "LLamando al servicio de COMUNAS XYGO");
		LOGGER.info(Utils.cabeceraLog() + "Id de Region >>> "+ getIdRegion());
		ArrayOfComuna comunas = null;
		try {
			comunas = delegateServices.obtenerListaComunas(getIdRegion());
		} catch (Exception e) {
			if(cliente.getFlujo().equals("NuevaLinea")){
				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ cliente.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+plan.getTest()+"|USUARIO|"+cliente.getTelefono()+"|GRUPOCLIENTE|"+cliente.getGrupoCliente()+"|RUT|"+cliente.getRut()+"|NUMEROSERIE|"+cliente.getnSerie()+"|EMAIL|"+cliente.getEmail()+ "|NUMEROAPORTAR||COMPANIA||MERCADO||CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ cliente.getRegionDespacho()+"|COMUNA|"+ cliente.getComunaDespacho()+"|CALLE|"+ cliente.getCalleDespacho()+"|NUMERO|"+ cliente.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_SERVICIO_COMUNA|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			}else{
				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ cliente.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+plan.getTest()+"|USUARIO|"+cliente.getTelefono()+"|GRUPOCLIENTE|"+cliente.getGrupoCliente()+"|RUT|"+cliente.getRut()+"|NUMEROSERIE|"+cliente.getnSerie()+"|EMAIL|"+cliente.getEmail()+"|NUMEROAPORTAR|"+cliente.getNumeroPorta()+"|COMPANIA|"+cliente.getCompaniaOrigen()+ "|MERCADO|"+ cliente.getMercadoOrigen() + "|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ cliente.getRegionDespacho()+"|COMUNA|"+ cliente.getComunaDespacho()+"|CALLE|"+ cliente.getCalleDespacho()+"|NUMERO|"+ cliente.getNumeroDespacho()+"|CAP-SEGURIDAD|"+cliente.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_SERVICIO_COMUNA|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			}
			LOGGER.error("Error al llamar al servicio de COMUNAS XYGO");
			e.printStackTrace();
			return Action.ERROR;
		}
		
		Collections.sort(comunas.getComuna(), new ComunaComparator());
		
		String jsonString = gson.toJson(comunas.getComuna());

		setJsonComunas(jsonString);
		return Action.SUCCESS;
	}
	
	public String obtenerCalles(){
		HttpServletRequest req = ServletActionContext.getRequest();
		HttpSession session=req.getSession();
		Cliente cliente=(Cliente)session.getAttribute("Cliente");
		Plan plan=(Plan)session.getAttribute("Plan");
		//Obtener Calles
		Gson gson = new Gson();
		LOGGER.info(Utils.cabeceraLog() + "LLamando al servicio de CALLES XYGO");
		LOGGER.info(Utils.cabeceraLog() + "Id de Region >>> "+ getIdRegion());
		LOGGER.info(Utils.cabeceraLog() + "Id de Comuna >>> "+ getIdComuna());
		ArrayOfCalle calles = null;
		try {
			calles = delegateServices.obtenerListaCalles(getIdRegion(), getIdComuna(), "");
		} catch (Exception e) {
			if(cliente.getFlujo().equals("NuevaLinea")){
				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ cliente.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+plan.getTest()+"|USUARIO|"+cliente.getTelefono()+"|GRUPOCLIENTE|"+cliente.getGrupoCliente()+"|RUT|"+cliente.getRut()+"|NUMEROSERIE|"+cliente.getnSerie()+"|EMAIL|"+cliente.getEmail()+ "|NUMEROAPORTAR||COMPANIA||MERCADO||CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ cliente.getRegionDespacho()+"|COMUNA|"+ cliente.getComunaDespacho()+"|CALLE|"+ cliente.getCalleDespacho()+"|NUMERO|"+ cliente.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_SERVICIO_CALLES|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			}else{
				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ cliente.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+plan.getTest()+"|USUARIO|"+cliente.getTelefono()+"|GRUPOCLIENTE|"+cliente.getGrupoCliente()+"|RUT|"+cliente.getRut()+"|NUMEROSERIE|"+cliente.getnSerie()+"|EMAIL|"+cliente.getEmail()+"|NUMEROAPORTAR|"+cliente.getNumeroPorta()+"|COMPANIA|"+cliente.getCompaniaOrigen()+ "|MERCADO|"+ cliente.getMercadoOrigen() + "|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ cliente.getRegionDespacho()+"|COMUNA|"+ cliente.getComunaDespacho()+"|CALLE|"+ cliente.getCalleDespacho()+"|NUMERO|"+ cliente.getNumeroDespacho()+"|CAP-SEGURIDAD|"+cliente.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_SERVICIO_CALLES|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			}
			LOGGER.error("Error al llamar al servicio de COMUNAS XYGO");
			e.printStackTrace();
			return Action.ERROR;
		}
		
		String jsonString = gson.toJson(calles.getCalle());

		setJsonCalles(jsonString);
		return Action.SUCCESS;
	}
	
	public String obtenerNumeros(){
		HttpServletRequest req = ServletActionContext.getRequest();
		HttpSession session=req.getSession();
		Cliente cliente=(Cliente)session.getAttribute("Cliente");
		Plan plan=(Plan)session.getAttribute("Plan");
		//Obtener Numeros Direccion
		Gson gson = new Gson();
		LOGGER.info(Utils.cabeceraLog() + "LLamando al servicio de NUMEROS MUNICIPALIDADES XYGO");
		LOGGER.info(Utils.cabeceraLog() + "Id de Calle >>> "+ getIdCalle());
		ArrayOfDireccion direccion = null;
		try {
			direccion = delegateServices.obtenerListaDirecciones(getIdCalle());
		} catch (Exception e) {
			if(cliente.getFlujo().equals("NuevaLinea")){
				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ cliente.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+plan.getTest()+"|USUARIO|"+cliente.getTelefono()+"|GRUPOCLIENTE|"+cliente.getGrupoCliente()+"|RUT|"+cliente.getRut()+"|NUMEROSERIE|"+cliente.getnSerie()+"|EMAIL|"+cliente.getEmail()+ "|NUMEROAPORTAR||COMPANIA||MERCADO||CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ cliente.getRegionDespacho()+"|COMUNA|"+ cliente.getComunaDespacho()+"|CALLE|"+ cliente.getCalleDespacho()+"|NUMERO|"+ cliente.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_SERVICIO_NUMEROS_CALLES|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			}else{
				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ cliente.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+plan.getTest()+"|USUARIO|"+cliente.getTelefono()+"|GRUPOCLIENTE|"+cliente.getGrupoCliente()+"|RUT|"+cliente.getRut()+"|NUMEROSERIE|"+cliente.getnSerie()+"|EMAIL|"+cliente.getEmail()+"|NUMEROAPORTAR|"+cliente.getNumeroPorta()+"|COMPANIA|"+cliente.getCompaniaOrigen()+ "|MERCADO|"+ cliente.getMercadoOrigen() + "|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ cliente.getRegionDespacho()+"|COMUNA|"+ cliente.getComunaDespacho()+"|CALLE|"+ cliente.getCalleDespacho()+"|NUMERO|"+ cliente.getNumeroDespacho()+"|CAP-SEGURIDAD|"+cliente.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_SERVICIO_NUMEROS_CALLES|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			}
			LOGGER.error("Error al llamar al servicio de NUMEROS MUNICIPALIDADES XYGO");
			e.printStackTrace();
			return Action.ERROR;
		}
		
		String jsonString = gson.toJson(direccion.getDireccion());

		setJsonNumeros(jsonString);
		return Action.SUCCESS;
	}


	public String getJsonRegiones() {
		return jsonRegiones;
	}

	public void setJsonRegiones(String jsonRegiones) {
		this.jsonRegiones = jsonRegiones;
	}

	public String getJsonComunas() {
		return jsonComunas;
	}

	public void setJsonComunas(String jsonComunas) {
		this.jsonComunas = jsonComunas;
	}

	public String getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(String idRegion) {
		this.idRegion = idRegion;
	}

	public String getJsonCalles() {
		return jsonCalles;
	}

	public void setJsonCalles(String jsonCalles) {
		this.jsonCalles = jsonCalles;
	}

	public String getIdComuna() {
		return idComuna;
	}

	public void setIdComuna(String idComuna) {
		this.idComuna = idComuna;
	}

	public String getIdCalle() {
		return idCalle;
	}

	public void setIdCalle(String idCalle) {
		this.idCalle = idCalle;
	}

	public String getJsonNumeros() {
		return jsonNumeros;
	}

	public void setJsonNumeros(String jsonNumeros) {
		this.jsonNumeros = jsonNumeros;
	}

	
}
