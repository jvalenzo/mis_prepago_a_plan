package com.esa.ponline.appmobile.web.actions;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.esa.ponline.appmobile.web.bean.RespuestaC2CType;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

import cl.esa.ponline.appmobile.delegate.DelegateServices;
import cl.esa.ponline.appmobile.exception.SolicitarLlamadaException;

public class AgendarC2CMovilAction extends ActionSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9195925802012269623L;
	private static final Logger LOGGER = Logger.getLogger(ContratacionAction.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	private String code;
	
	private String numContact;
	private String captcha;
	
	
	public String solicitarLlamadaC2C() throws Exception{
		
		HttpServletRequest req = ServletActionContext.getRequest();
		HttpSession session=req.getSession();
		DelegateServices delegateServices = new DelegateServices();
		Cliente cliente = (Cliente)session.getAttribute("Cliente");
		Plan plan = (Plan)session.getAttribute("Plan");
		
        LOGGER.info(Utils.cabeceraLog() + "Se ingresa al Action AgendarC2CMovilAction.solicitarLlamadaC2C");
        
        //Obtenemos el numero de telefono
        String numContact = getNumContact();
        String captcha = getCaptcha();
        
        numContact = numContact != null ? Utils.reemplazarCaracteresExtranos(numContact.toString()) : "";
    	if(!(null != Pattern.compile("[0-9]+").matcher(numContact) && numContact.length() == 9)){
    		//n�mero de telefono no cumple con formato
    		setCode("-1");
            return Action.SUCCESS;
    	}         	
        
    	try{
    		
    		if(!Boolean.valueOf(Config.getAppProperty(Constantes.EMULAR_CAPTCHA)) || Utils.recapcha(captcha, Utils.secretKeyCaptcha())){
    			//se consulta la disponibilidad de horario del C2C, en caso de estar fuera de horario se agenda para el d�a siguiente.
            	delegateServices.consultarHorarioC2C();
                
            	//Se agenda llamada
            	RespuestaC2CType response = delegateServices.SolicitarLlamadaC2C(cliente, numContact, plan);
            	        	
            	setCode(response.getCodigo());            
                logMetricas.info("PORTAL|ContratacionOnline|TIPO|INFO|FLUJO|"+ cliente.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|AgendoC2C|TEST|"+plan.getTest()+"|USUARIO|"+cliente.getTelefono()+"|GRUPOCLIENTE|"+cliente.getGrupoCliente()+"|RUT|"+cliente.getRut()+"|NUMEROSERIE|"+cliente.getnSerie()+"|EMAIL|"+cliente.getEmail()+"|NUMEROAPORTAR|"+cliente.getNumeroPorta()+"|COMPANIA|"+cliente.getCompaniaOrigen()+"|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ cliente.getRegionDespacho()+"|COMUNA|"+ cliente.getComunaDespacho()+"|CALLE|"+ cliente.getCalleDespacho()+"|NUMERO|"+ cliente.getNumeroDespacho()+"|CAP-SEGURIDAD|"+cliente.getCodigoCAP());
    		}else{
    			LOGGER.error(Utils.cabeceraLog() + "Error al validar Recaptcha");
    			setCode("-2");
    		}
    		
        	

        } catch (SolicitarLlamadaException se){

            LOGGER.error(Utils.cabeceraLog() + "Servicio retorno codigo de error inesperado. Mensaje: " + Utils.obtenerDetalleError(se));
            logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ cliente.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|AgendoC2C_ERROR|TEST|"+plan.getTest()+"|USUARIO|"+cliente.getTelefono()+"|GRUPOCLIENTE|"+cliente.getGrupoCliente()+"|RUT|"+cliente.getRut()+"|NUMEROSERIE|"+cliente.getnSerie()+"|EMAIL|"+cliente.getEmail()+"|NUMEROAPORTAR|"+cliente.getNumeroPorta()+"|COMPANIA|"+cliente.getCompaniaOrigen()+"|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ cliente.getRegionDespacho()+"|COMUNA|"+ cliente.getComunaDespacho()+"|CALLE|"+ cliente.getCalleDespacho()+"|NUMERO|"+ cliente.getNumeroDespacho()+"|CAP-SEGURIDAD|"+cliente.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(se) +"|");
        	
            setCode("-1");
        } catch (ServiceException e){

            LOGGER.error(Utils.cabeceraLog() + "Error al llamar al servicio crm_t_px_registraraccionsolicitudllamadaps.RegistrarAccionSolicitudLlamadaRequest " +
                    "[Mensaje:"+Utils.obtenerDetalleError(e)+"]");

            logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ cliente.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|AgendoC2C_ERROR|TEST|"+plan.getTest()+"|USUARIO|"+cliente.getTelefono()+"|GRUPOCLIENTE|"+cliente.getGrupoCliente()+"|RUT|"+cliente.getRut()+"|NUMEROSERIE|"+cliente.getnSerie()+"|EMAIL|"+cliente.getEmail()+"|NUMEROAPORTAR|"+cliente.getNumeroPorta()+"|COMPANIA|"+cliente.getCompaniaOrigen()+"|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ cliente.getRegionDespacho()+"|COMUNA|"+ cliente.getComunaDespacho()+"|CALLE|"+ cliente.getCalleDespacho()+"|NUMERO|"+ cliente.getNumeroDespacho()+"|CAP-SEGURIDAD|"+cliente.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
            setCode("-1");
        }
		return Action.SUCCESS;
	}
	
	
/*	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}*/
	
	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public String getNumContact() {
		return numContact;
	}

	public void setNumContact(String numContact) {
		this.numContact = numContact;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}



}
