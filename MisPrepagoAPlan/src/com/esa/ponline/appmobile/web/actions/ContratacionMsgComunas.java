package com.esa.ponline.appmobile.web.actions;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.google.gson.Gson;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ContratacionMsgComunas extends ActionSupport {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1406108767659335750L;
	private static final Logger LOGGER = Logger.getLogger(ContratacionAction.class);
	private String jsonDeliveryComunas;
	private String mensaje="";
	private Map<String,String> mapOp1;
	private Map<String,String> mapOp2;
	private Map<String,String> mapOp3;
	private Map<String,String> mapOp4;
	private Map<String,String> mapOp5;
	private Map<String,String> mapOp6;
	private Map<String,String> mapOp7;
	private Map<String,String> mapOp8;
	private Map<String,String> mapOp9;
	private Map<String,String> mapOp10;
	private Map<String,String> mapOp11;
	private static final int PREPAGO = 0;
	
	@Override
	public String execute(){	
		Gson gson = new Gson();
		String tieneDelivery = "true";
		HashMap<String, String> resp = new HashMap<String, String>();
		Cliente clienteCO = (Cliente) getSession().get("Cliente");
		//String response="ContratacionMsgAjax";
		
		HttpServletRequest req = ServletActionContext.getRequest();
		String comuna = req.getParameter("comuna") != null ? Utils.reemplazarCaracteresExtranos(req.getParameter("comuna").toString()) : "";
		String region = req.getParameter("region") != null ? Utils.reemplazarCaracteresExtranos(req.getParameter("region").toString()) : "";
		
		//Key properties para comunas sin coberturas del proveedor Brightstart o VVTT
		String keyComunasSinCobertura = "brightstar";
		if(null != clienteCO.getNumeroPorta() && clienteCO.getMercadoOrigen() == PREPAGO){
			keyComunasSinCobertura = "vvtt";
		}		
		keyComunasSinCobertura += "." + region.replaceAll(" ", "").toLowerCase();
		
		String comunasSinCobertura = Config.getAppProperty(keyComunasSinCobertura);
		
		if(null != comunasSinCobertura){
			List listComunasSinCobertura = Arrays.asList(Config.getAppProperty(keyComunasSinCobertura).split(","));
			
			//Se evalua si la comuna tiene delivery
			if(listComunasSinCobertura.contains(comuna)){
				tieneDelivery = "false";
			}
		}
				
		
		//System.out.println("se recibe: "+comuna);
		if(!comuna.equals("") && tieneDelivery.equals("true")){
			if(msgOp1(comuna)){
				setMensaje("El Chip ser&aacute; despachado en un plazo m&aacute;ximo de 48 horas h&aacute;biles entre las 09:00 y 18:00 Hrs.");
			}else if(msgOp2(comuna)){
				setMensaje("El Chip ser&aacute; despachado en un plazo de 48 horas h&aacute;biles entre las 09:00 y 18:00 Hrs. Te llegar&aacute; un SMS indicando la fecha de despacho.");
			}else if(msgOp3(comuna)){
				setMensaje("El Chip ser&aacute; despachado el pr&oacute;ximo Jueves h&aacute;bil entre las 09:00 y 18:00 Hrs. Te llegar&aacute; un SMS indicando la fecha de despacho.");
			}else if(msgOp4(comuna)){
				setMensaje("El Chip ser&aacute; despachado el pr&oacute;ximo Lunes h&aacute;bil entre las 09:00 y 18:00 Hrs. Te llegar&aacute; un SMS indicando la fecha de despacho.");
			}else if(msgOp5(comuna)){
				setMensaje("Para esta comuna, los despachos son los d&iacute;as Lunesa, Mi&eacute;rcoles o Viernes h&aacute;biles entre las 09:00 y 18:00 Hrs. Te llegar&aacute; un SMS indicando la fecha de despacho.");
			}else if(msgOp6(comuna)){
				setMensaje("El Chip ser&aacute; despachado el pr&oacute;ximo Martes h&aacute;bil entre las 09:00 y 18:00 Hrs. Te llegar&aacute; un SMS indicando la fecha de despacho.");
			}else if(msgOp7(comuna)){
				setMensaje("Para esta comuna, los despachos son los Martes o Jueves h&aacute;biles entre las 09:00 y 18:00 Hrs. Te llegar&aacute; un SMS indicando la fecha de despacho.");
			}else if(msgOp8(comuna)){
				setMensaje("El Chip ser&aacute; despachado el pr&oacute;ximo Mi&eacute;rcoles h&aacute;bil entre las 09:00 y 18:00 Hrs. Te llegar&aacute; un SMS indicando la fecha de despacho.");
			}else if(msgOp9(comuna)){
				setMensaje("En esta comuna los despachos son los d&iacute;as Mi&eacute;rcoles h&aacute;biles, de la segunda o tercera semana del mes entre las 09:00 y 18:00 Hrs. Te llegar&aacute; un SMS indicando la fecha de despacho.");
			}else if(msgOp10(comuna)){
				setMensaje("El Chip ser&aacute; despachado el pr&oacute;ximo Viernes h&aacute;bil entre las 09:00 y 18:00 Hrs. Te llegar&aacute; un SMS indicando la fecha de despacho.");
			}else if(msgOp11(comuna)){
				setMensaje("Para esta comuna, los despacho son los d&iacute;as Lunes o Mi&eacute;rcoles h&aacute;biles entre las 9 y 18 hrs. Te llegar&aacute; un mail o SMS indicando la fecha de despacho.");
			}
		}
		resp.put("tieneDelivery", tieneDelivery);
		resp.put("mensaje", mensaje);
		Plan planCO = (Plan)getSession().get("Plan");
		planCO.setMensajeDespacho(mensaje);
		getSession().put("Plan", planCO);
		
		setJsonDeliveryComunas(gson.toJson(resp));
		return Action.SUCCESS;
	}
	
	public boolean msgOp1(String comuna){
		boolean respuesta=false;
		try{
			if(getMapOp1()!=null){
				respuesta = getMapOp1().containsKey(comuna);
			}else{
				String comOp1 = Config.getAppProperty("comOp1");
				List<String> opciones = Arrays.asList(comOp1.split(","));
				Map<String,String> mapa = new HashMap<String, String>();
				for(String opcion : opciones){
					mapa.put(opcion, opcion);
				}
				respuesta = mapa.containsKey(comuna);
				setMapOp1(mapa);
			}
		}catch(Exception e){
			LOGGER.error("error buscando en propertie de comunas msgOp1: "+e);
		}
		return respuesta;
	}
	
	public boolean msgOp2(String comuna){
		boolean respuesta=false;
		try{
			if(getMapOp2()!=null){
				respuesta = getMapOp2().containsKey(comuna);
			}else{
				String comOp2 = Config.getAppProperty("comOp2");
				List<String> opciones = Arrays.asList(comOp2.split(","));
				Map<String,String> mapa = new HashMap<String, String>();
				for(String opcion : opciones){
					mapa.put(opcion, opcion);
				}
				respuesta = mapa.containsKey(comuna);
				setMapOp2(mapa);
			}
		}catch(Exception e){
			LOGGER.error("error buscando en propertie de comunas msgOp2: "+e);
		}
		return respuesta;
	}
	
	public boolean msgOp3(String comuna){
		boolean respuesta=false;
		try{
			if(getMapOp3()!=null){
				respuesta = getMapOp3().containsKey(comuna);
			}else{
				String comOp3 = Config.getAppProperty("comOp3");
				List<String> opciones = Arrays.asList(comOp3.split(","));
				Map<String,String> mapa = new HashMap<String, String>();
				for(String opcion : opciones){
					mapa.put(opcion, opcion);
				}
				respuesta = mapa.containsKey(comuna);
				setMapOp3(mapa);
			}
		}catch(Exception e){
			LOGGER.error("error buscando en propertie de comunas msgOp3: "+e);
		}
		return respuesta;
	}
	
	public boolean msgOp4(String comuna){
		boolean respuesta=false;
		try{
			if(getMapOp4()!=null){
				respuesta = getMapOp4().containsKey(comuna);
			}else{
				String comOp4 = Config.getAppProperty("comOp4");
				List<String> opciones = Arrays.asList(comOp4.split(","));
				Map<String,String> mapa = new HashMap<String, String>();
				for(String opcion : opciones){
					mapa.put(opcion, opcion);
				}
				respuesta = mapa.containsKey(comuna);
				setMapOp4(mapa);
			}
		}catch(Exception e){
			LOGGER.error("error buscando en propertie de comunas msgOp4: "+e);
		}
		return respuesta;
	}
	
	public boolean msgOp5(String comuna){
		boolean respuesta=false;
		try{
			if(getMapOp5()!=null){
				respuesta = getMapOp5().containsKey(comuna);
			}else{
				String comOp5 = Config.getAppProperty("comOp5");
				List<String> opciones = Arrays.asList(comOp5.split(","));
				Map<String,String> mapa = new HashMap<String, String>();
				for(String opcion : opciones){
					mapa.put(opcion, opcion);
				}
				respuesta = mapa.containsKey(comuna);
				setMapOp5(mapa);
			}
		}catch(Exception e){
			LOGGER.error("error buscando en propertie de comunas msgOp5: "+e);
		}
		return respuesta;
	}
	
	public boolean msgOp6(String comuna){
		boolean respuesta=false;
		try{
			if(getMapOp6()!=null){
				respuesta = getMapOp6().containsKey(comuna);
			}else{
				String comOp6 = Config.getAppProperty("comOp6");
				List<String> opciones = Arrays.asList(comOp6.split(","));
				Map<String,String> mapa = new HashMap<String, String>();
				for(String opcion : opciones){
					mapa.put(opcion, opcion);
				}
				respuesta = mapa.containsKey(comuna);
				setMapOp6(mapa);
			}
		}catch(Exception e){
			LOGGER.error("error buscando en propertie de comunas msgOp6: "+e);
		}
		return respuesta;
	}
	
	public boolean msgOp7(String comuna){
		boolean respuesta=false;
		try{
			if(getMapOp7()!=null){
				respuesta = getMapOp7().containsKey(comuna);
			}else{
				String comOp7 = Config.getAppProperty("comOp7");
				List<String> opciones = Arrays.asList(comOp7.split(","));
				Map<String,String> mapa = new HashMap<String, String>();
				for(String opcion : opciones){
					mapa.put(opcion, opcion);
				}
				respuesta = mapa.containsKey(comuna);
				setMapOp7(mapa);
			}
		}catch(Exception e){
			LOGGER.error("error buscando en propertie de comunas msgOp7: "+e);
		}
		return respuesta;
	}
	
	public boolean msgOp8(String comuna){
		boolean respuesta=false;
		try{
			if(getMapOp8()!=null){
				respuesta = getMapOp8().containsKey(comuna);
			}else{
				String comOp8 = Config.getAppProperty("comOp8");
				List<String> opciones = Arrays.asList(comOp8.split(","));
				Map<String,String> mapa = new HashMap<String, String>();
				for(String opcion : opciones){
					mapa.put(opcion, opcion);
				}
				respuesta = mapa.containsKey(comuna);
				setMapOp8(mapa);
			}
		}catch(Exception e){
			LOGGER.error("error buscando en propertie de comunas msgOp8: "+e);
		}
		return respuesta;
	}
	
	public boolean msgOp9(String comuna){
		boolean respuesta=false;
		try{
			if(getMapOp9()!=null){
				respuesta = getMapOp9().containsKey(comuna);
			}else{
				String comOp9 = Config.getAppProperty("comOp9");
				List<String> opciones = Arrays.asList(comOp9.split(","));
				Map<String,String> mapa = new HashMap<String, String>();
				for(String opcion : opciones){
					mapa.put(opcion, opcion);
				}
				respuesta = mapa.containsKey(comuna);
				setMapOp9(mapa);
			}
		}catch(Exception e){
			LOGGER.error("error buscando en propertie de comunas msgOp9: "+e);
		}
		return respuesta;
	}
	
	public boolean msgOp10(String comuna){
		boolean respuesta=false;
		try{
			if(getMapOp10()!=null){
				respuesta = getMapOp10().containsKey(comuna);
			}else{
				String comOp10 = Config.getAppProperty("comOp10");
				List<String> opciones = Arrays.asList(comOp10.split(","));
				Map<String,String> mapa = new HashMap<String, String>();
				for(String opcion : opciones){
					mapa.put(opcion, opcion);
				}
				respuesta = mapa.containsKey(comuna);
				setMapOp10(mapa);
			}
		}catch(Exception e){
			LOGGER.error("error buscando en propertie de comunas msgOp10: "+e);
		}
		return respuesta;
	}
	
	public boolean msgOp11(String comuna){
		boolean respuesta=false;
		try{
			if(getMapOp11()!=null){
				respuesta = getMapOp11().containsKey(comuna);
			}else{
				String comOp11 = Config.getAppProperty("comOp11");
				List<String> opciones = Arrays.asList(comOp11.split(","));
				Map<String,String> mapa = new HashMap<String, String>();
				for(String opcion : opciones){
					mapa.put(opcion, opcion);
				}
				respuesta = mapa.containsKey(comuna);
				setMapOp11(mapa);
			}
		}catch(Exception e){
			LOGGER.error("error buscando en propertie de comunas msgOp11: "+e);
		}
		return respuesta;
	}
	
	
	public String getMensaje(){
		return this.mensaje;
	}
	
	public void setMensaje(String mensaje){
		this.mensaje = mensaje;
	}
	
	public Map<String,String> getMapOp1(){
		return mapOp1;
	}
	
	public void setMapOp1(Map<String,String> mapOp1){
		this.mapOp1 = mapOp1;
	}
	
	public Map<String,String> getMapOp2(){
		return mapOp2;
	}
	
	public void setMapOp2(Map<String,String> mapOp2){
		this.mapOp2 = mapOp2;
	}
	
	public Map<String,String> getMapOp3(){
		return mapOp3;
	}
	
	public void setMapOp3(Map<String,String> mapOp3){
		this.mapOp3 = mapOp3;
	}
	
	public Map<String,String> getMapOp4(){
		return mapOp4;
	}
	
	public void setMapOp4(Map<String,String> mapOp4){
		this.mapOp4 = mapOp4;
	}
	
	public Map<String,String> getMapOp5(){
		return mapOp5;
	}
	
	public void setMapOp5(Map<String,String> mapOp5){
		this.mapOp5 = mapOp5;
	}
	
	public Map<String,String> getMapOp6(){
		return mapOp6;
	}
	
	public void setMapOp6(Map<String,String> mapOp6){
		this.mapOp6 = mapOp6;
	}
	
	public Map<String,String> getMapOp7(){
		return mapOp7;
	}
	
	public void setMapOp7(Map<String,String> mapOp7){
		this.mapOp7 = mapOp7;
	}
	
	public Map<String,String> getMapOp8(){
		return mapOp8;
	}
	
	public void setMapOp8(Map<String,String> mapOp8){
		this.mapOp8 = mapOp8;
	}
	
	public Map<String,String> getMapOp9(){
		return mapOp9;
	}
	
	public void setMapOp9(Map<String,String> mapOp9){
		this.mapOp9 = mapOp9;
	}
	
	public Map<String,String> getMapOp10(){
		return mapOp10;
	}
	
	public void setMapOp10(Map<String,String> mapOp10){
		this.mapOp10 = mapOp10;
	}
	
	public Map<String,String> getMapOp11(){
		return mapOp11;
	}
	
	public void setMapOp11(Map<String,String> mapOp11){
		this.mapOp11 = mapOp11;
	}
	
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}

	public String getJsonDeliveryComunas() {
		return jsonDeliveryComunas;
	}

	public void setJsonDeliveryComunas(String jsonDeliveryComunas) {
		this.jsonDeliveryComunas = jsonDeliveryComunas;
	}
	
	
	
	
}
