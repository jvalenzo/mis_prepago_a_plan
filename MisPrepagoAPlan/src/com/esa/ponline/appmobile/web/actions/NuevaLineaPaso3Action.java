package com.esa.ponline.appmobile.web.actions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.helper.OfertaContratacionOnlineHelper;
import com.esa.ponline.appmobile.utils.PruebaUtil;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cl.esa.ponline.appmobile.delegate.DelegateServices;

import com.esa.ponline.appmobile.web.actions.Constantes;

public class NuevaLineaPaso3Action  extends ActionSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5354672520090783565L;
	private static final Logger LOGGER = Logger.getLogger(NuevaLineaPaso3Action.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	private String paso3Page 			= "NuevaLineaPaso3";
	private String paso3ErrorPage 		= "NuevaLineaPaso3Error";
	
	private Cliente clienteCO;
	private Plan plan;
	
	private List<String> terminosYCondiciones;
	private DelegateServices delegateServices = new DelegateServices();
	
	@Override
	public String execute(){
		if(Boolean.valueOf(Config.getAppProperty("switchPRueba"))){
			PruebaUtil.creaDatosPrueba();
		}		
		String response = paso3ErrorPage;
		HttpServletRequest req = ServletActionContext.getRequest();
		clienteCO = (Cliente) getSession().get("Cliente");
		plan = (Plan)getSession().get("Plan");
		
		try {
			String errorDireccion = req.getParameter("direccion");
			if(null != errorDireccion && errorDireccion.toString().equals("error")){
				plan.setIconoErrorC2C("error_triste.svg");
				plan.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
				plan.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
				plan.setTipoError(Config.getAppProperty("url.tipo.error"));
				plan.setBtnErrorC2C("Quiero que me llamen");
				plan.setActionErrorC2C("$('#modalSeleccionFlujo').modal('show');");
				getSession().put("Plan", plan);
				return response;
			}	
			
			String comunaDespacho			= Utils.reemplazarCaracteresExtranos(req.getParameter("selComunaH").toString());
			String regionDespacho			= Utils.reemplazarCaracteresExtranos(req.getParameter("selRegionH").toString());
			String calleDespacho			= Utils.reemplazarCaracteresExtranos(req.getParameter("calleH").toString());
			String numeroDespacho			= Utils.reemplazarCaracteresExtranos(req.getParameter("numCalleH").toString());
			String direccionAdicional = Utils.reemplazarCaracteresExtranos(req.getParameter("deptoH").toString());
			
			clienteCO.setNumeroCalle(numeroDespacho);
			if(null != direccionAdicional){
				if(!"".equals(direccionAdicional.trim())){
					clienteCO.setDireccionAdicional(direccionAdicional);
					numeroDespacho += ", Depto/Casa: "+ direccionAdicional;
				}
			}
			clienteCO.setComunaDespacho(Utils.capitalizaString(comunaDespacho));
			clienteCO.setRegionDespacho(Utils.capitalizaString(regionDespacho));
			clienteCO.setCalleDespacho(Utils.capitalizaString(calleDespacho));
			clienteCO.setNumeroDespacho(Utils.capitalizaString(numeroDespacho));
			//Se sube a sesion
			getSession().put("Cliente", clienteCO);
			getSession().put("plan", plan);
			response = paso3Page;
			
			this.loadConditionsAndTerms(plan);
			logMetricas.info("PORTAL|ContratacionOnline|TIPO|INFO|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+plan.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|NUMEROAPORTAR||COMPANIA||MERCADO||CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|");
		} catch (Exception e) {
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso3|TEST|"+plan.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|NUMEROAPORTAR||COMPANIA||MERCADO||CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
		}
		
		
		return response;
	}
	
	private void loadConditionsAndTerms(Plan plan){
		//Obtenemos terminos y condiciones
		HashMap<String, String> hashMapTerminos = new HashMap<String, String>(Config.getConfigContratacionOnline_Terminos());
		if(null!=hashMapTerminos){
			if(Double.valueOf(plan.getCargoPlanOferta())>0){
				setTerminosYCondiciones(
						OfertaContratacionOnlineHelper.cargarTerminosyDebesSaber("NLD_"+plan.getCargoPlan().replace(".", ""),hashMapTerminos,"_COM_"));
			}else{
				setTerminosYCondiciones(
						OfertaContratacionOnlineHelper.cargarTerminosyDebesSaber("NL_"+plan.getCargoPlan().replace(".", ""),hashMapTerminos,"_COM_"));
			}
		}else{
			LOGGER.info(Utils.cabeceraLog() + "Error al cargar terminos hashMapDebesLA>>> "+ hashMapTerminos);
		}
		
		if(plan.getEsMiPrimerPlan()){
			String mensajeCobroAnticipado_TyC = Config.getConfigContratacionOnline_Terminos().get(Constantes.MENSAJE_COBRO_ANTICIPADO);
			terminosYCondiciones.add(mensajeCobroAnticipado_TyC);
		}
		
		getSession().put("terminosYCondiciones", terminosYCondiciones);
	}
	
	public void setTerminosYCondiciones(List<String> terminosYCondiciones) {
		this.terminosYCondiciones = terminosYCondiciones;	
	}
	
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}
	
}
