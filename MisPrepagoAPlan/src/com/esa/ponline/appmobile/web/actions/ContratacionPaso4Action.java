package com.esa.ponline.appmobile.web.actions;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.utils.PruebaUtil;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ContratacionPaso4Action extends ActionSupport  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7264720493176902590L;
	private static final Logger LOGGER = Logger.getLogger(ContratacionPaso4Action.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	private String paso4Page 			= "ContratacionPaso4";
	private String paso4ErrorPage 		= "ContratacionPaso4Error";
	
	private Cliente clienteCO;
	private Plan planCO;
	
	
	@Override
	public String execute(){
		if(Boolean.valueOf(Config.getAppProperty("switchPRueba"))){
			PruebaUtil.creaDatosPrueba();
		}
		String response= paso4ErrorPage;
		HttpServletRequest req = ServletActionContext.getRequest();
		
		try {
			String errorDireccion = req.getParameter("direccion");
			clienteCO = (Cliente) getSession().get("Cliente");
			planCO = (Plan)getSession().get("Plan");
			
			if(null != errorDireccion && errorDireccion.toString().equals("error")){
				planCO.setIconoErrorC2C("error_triste.svg");
				planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
				planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
				planCO.setTipoError(Config.getAppProperty("url.tipo.error"));
				planCO.setBtnErrorC2C("Quiero que me llamen");
				planCO.setActionErrorC2C(Config.getAppProperty("cliick_"+planCO.getCargoPlan().replace(".", "")));
				getSession().put("Plan", planCO);
				return response;
			}
			
			String comunaDespacho			= Utils.reemplazarCaracteresExtranos(req.getParameter("selComunaH").toString());
			String regionDespacho			= Utils.reemplazarCaracteresExtranos(req.getParameter("selRegionH").toString());
			String calleDespacho			= Utils.reemplazarCaracteresExtranos(req.getParameter("calleH").toString());
			String numeroDespacho			= Utils.reemplazarCaracteresExtranos(req.getParameter("numCalleH").toString());
			String direccionAdicional = Utils.reemplazarCaracteresExtranos(req.getParameter("deptoH").toString());
			
			clienteCO.setNumeroCalle(numeroDespacho);
			if(null != direccionAdicional){
				if(!"".equals(direccionAdicional.trim())){
					clienteCO.setDireccionAdicional(direccionAdicional);
					numeroDespacho += ", Depto/Casa: "+ direccionAdicional;
				}
			}
			
			clienteCO.setComunaDespacho(Utils.capitalizaString(comunaDespacho));
			clienteCO.setRegionDespacho(Utils.capitalizaString(regionDespacho));
			clienteCO.setCalleDespacho(Utils.capitalizaString(calleDespacho));
			clienteCO.setNumeroDespacho(Utils.capitalizaString(numeroDespacho));
			
			//Se sube a sesion
			getSession().put("Cliente", clienteCO);
			response = paso4Page;
			logMetricas.info("PORTAL|ContratacionOnline|TIPO|INFO|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP());
		} catch (Exception e) {
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso4|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
		}
		

		return response;
	}
	
	
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}
	
}
