package com.esa.ponline.appmobile.web.actions;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.StrutsStatics;

import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.helper.OfertaContratacionOnlineHelper;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;


import cl.esa.ponline.appmobile.delegate.DelegateServices;


public class ContratacionAction extends ActionSupport {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 4716733355940579957L;
	private static final Logger LOGGER      = Logger.getLogger(ContratacionAction.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	private String paso1APage 			= "ContratacionPaso1A";
	private String paso1BPage 			= "ContratacionPaso1B";
	private String paso1ErrorPage 		= "ContratacionPaso1Error";
	private DelegateServices delegateServices = new DelegateServices();
	public static String secret = "";
	public static Boolean esProduccion = true;
	private String url = "https://www.entel.cl/planes/";
	private String urlHttps = "";
	
	
	private Plan planCO;
	private Cliente clienteCO;
	
	private List<String> terminosYCondiciones;
	@Override
	public String execute(){		
		
		planCO = new Plan();
		clienteCO=new Cliente();
		clienteCO.setFlujo("Portabilidad");
		String response=paso1ErrorPage;
		//Se obtienen los parametros que se entregaran en la URL desde el portal publico
		HttpServletRequest req = ServletActionContext.getRequest();
		String paso	= "1A";
		String idPlan	= "";
		String origen = "";
				
		try{
			paso = req.getParameter("paso").toString();
			if(paso.equals("1A")){
				idPlan = req.getParameter("idPlan").toString();
			}
		}catch(Exception e){
			HttpSession session=req.getSession();
        	session.invalidate();
			return "redirect";
		}
		
		try{
			//Se define si el paso a entregar sera A o B.
			if(paso.equals("1A")){
				HttpSession session=req.getSession();
				//logica para determinar si el C2C se debe activar o no
				Utils.activacionC2C(session, req.getParameter("c2"));
				
				//atributo para identificar si se debe mostrar el btn de captcha para no repetir en caso de paso 1 de porta.
				getSession().put("mostrarBtnCaptcha", false);
				getSession().put("siteKeyCaptcha", Utils.siteKeyCaptcha());
				getSession().put("activarChat", Boolean.valueOf(Config.getAppProperty("mostrar.chat.teayudo")));
				
				//logica para determinar si el C2C se debe activar o no
				Utils.activacionC2C(session, req.getParameter("c2"));
				
				//se rescata el origen de la oferta
				if(req.getParameter("origen") != null){
					planCO.setOrigenOferta(req.getParameter("origen"));
					if(req.getParameter("k")!=null){
						planCO.setBuscarDescuentoEspecifico(Utils.ajustesDesuentosEspecificos(req.getParameter("k")));
					}else{
						getSession().put("OfertaEspecifica", null);
					}
				}
				getSession().put("Inbound", Utils.numeroInbound(planCO.getOrigenOferta()));
				response = contratacionPaso1A();
			}else if(paso.equals("1B")){
				//atributo para identificar si se debe mostrar el btn de captcha para no repetir en caso de paso 1 de porta.
				
				if((Boolean)getSession().get("activarChat")){
					getSession().put("mostrarBtnCaptcha", true);
				}
				
				response = contratacionPaso1B();
			}else{
				HttpSession session=req.getSession();
            	session.invalidate();
				return "redirect";
			}
		}catch(Exception e){
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso"+ paso + "|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
		}
		
		return response;
	}
	

	public String contratacionPaso1A() {
		LOGGER.info(Utils.cabeceraLog() + "Paso 1 A - Contratacion");
		esProduccion = Boolean.valueOf(Config.getAppProperty("captchaProduccion"));
		boolean redireccionHTTPS = Boolean.valueOf(Config.getAppProperty("redireccionHTTPS"));
		secret = Utils.obtenerKeyCaptcha(esProduccion);
		
		HttpServletRequest req = ServletActionContext.getRequest();
		String idPlan = Utils.reemplazarCaracteresExtranos(req.getParameter("idPlan"));
		
		if(redireccionHTTPS && !urlSegura(idPlan,planCO.getOrigenOferta())){
			return "redirectHttps";
		}
		
		planCO.setUrlOrigen("https://appswls.entel.cl/ContratacionOnLineMovil/paso1?origen="+planCO.getOrigenOferta()+"&idPlan="+idPlan+"&paso=1A");
		
		Boolean clickToCallFueraHorario = false;
		try {
			//se consulta la disponibilidad de horario del C2C, en caso de estar fuera de horario se agenda para el d�a siguiente.
			clickToCallFueraHorario = delegateServices.consultarHorarioC2C();
		} catch (Exception e) {
			LOGGER.error("Error al consultar horario de C2C");
		}
		clienteCO.setMostrarOpcionChat(false);
		clienteCO.setC2cFueraHorario(clickToCallFueraHorario);
		
		try{
			
			try{				
				if(req.getParameter("token") != null){	
					String token=req.getParameter("token");
					String[] datos=Utils.parseToken(token);
					clienteCO.setRut(datos[1]);
				}
				
				OfertaContratacionOnlineHelper.obtenerDetallePlan(planCO, idPlan);
				
				Utils.iniciarContadorPlanes();		
				if(null==getSession().get("Test")){
					String testAB=Utils.seleccionarTest(Utils.incrementarContadorPortabilidad(idPlan));
					getSession().put("Test", testAB);
					planCO.setTest(testAB);
				}else{
					planCO.setTest(getSession().get("Test").toString());
				}
			}catch(Exception e){
								
				LOGGER.error(Utils.cabeceraLog() + "Error al ingresar los parametros de la URL paso1-NL:");
				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_URL|DETALLE|Url no es correcta|");
				HttpSession session=req.getSession();
            	session.invalidate();
				return "redirect";
			}
			
			//Obtenemos terminos y condiciones
			HashMap<String, String> hashMapTerminos = new HashMap<String, String>(Config.getConfigContratacionOnline_Terminos());
			if(null!=hashMapTerminos){
				if(Double.valueOf(planCO.getCargoPlanOferta())>0){
					setTerminosYCondiciones(
							OfertaContratacionOnlineHelper.cargarTerminosyDebesSaber(planCO.getCargoPlan().replace(".", ""), 
									hashMapTerminos,"_COMD_"));
				}else{
					setTerminosYCondiciones(
							OfertaContratacionOnlineHelper.cargarTerminosyDebesSaber(planCO.getCargoPlan().replace(".", ""), 
									hashMapTerminos,"_COM_"));
				}
			}else{
				LOGGER.info(Utils.cabeceraLog() + "Error al cargar terminos hashMapDebesLA>>> "+ hashMapTerminos);
			}
			getSession().put("terminosYCondiciones", terminosYCondiciones);
			
			logMetricas.info("PORTAL|ContratacionOnline|TIPO|INFO|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP());
			
		}catch (Exception e){
			//error de servicio
			planCO.setIconoErrorC2C("error_triste.svg");
        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
			planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
			planCO.setTipoError(Config.getAppProperty("url.tipo.error"));
			planCO.setActionErrorC2C(Config.getAppProperty("cliick_"+planCO.getCargoPlan().replace(".", "")));
			planCO.setBtnErrorC2C("Quiero que me llamen");
			LOGGER.error("Error al procesar controlador del paso1:" + Utils.obtenerDetalleError(e));
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			//Se deja en sesion el objeto PLAN
			getSession().put("Plan", planCO);
			
			return paso1ErrorPage;
		}	
		//Se deja en sesion el objeto PLAN
		getSession().put("Plan", planCO);
		getSession().put("Cliente", clienteCO);
		
		return paso1APage ;
	}
	
	public String contratacionPaso1B() {
		LOGGER.info(Utils.cabeceraLog() + "Paso 1 B - Contratacion");	
		
		planCO = (Plan)getSession().get("Plan");
		clienteCO = (Cliente)getSession().get("Cliente");
		clienteCO.setMostrarOpcionChat(true);
		
		if(null == clienteCO){
			clienteCO = new Cliente();
		}
		
		boolean validaDatos = false;
        String nRut;
        String dRut;
        
		//Trae datos
		HttpServletRequest req = ServletActionContext.getRequest();
		String rut			= Utils.reemplazarCaracteresExtranos(req.getParameter("rut"));
		String nSerie		= Utils.reemplazarCaracteresExtranos(req.getParameter("nSerie"));
		String email		= Utils.reemplazarCaracteresExtranosEmail(req.getParameter("email"));
		
		boolean validaRecapcha = false;
		try {
			validaRecapcha = Utils.validarRecaptcha(esProduccion, req.getParameter("g-recaptcha-response"), Utils.secretKeyCaptcha());
		} catch (Exception e1) {
			LOGGER.info(Utils.cabeceraLog() + "error al validar captcha: " + e1.getMessage());
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1B|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_CAPTCHA|DETALLE|" + Utils.obtenerDetalleError(e1) +"|");
			return paso1ErrorPage;
		}
		
		try {
			if(validaRecapcha){
				
				nSerie = nSerie.replaceAll("\\.", "");
				rut = rut.replace(".", "");
				
				if(Boolean.parseBoolean(Config.getAppProperty("restringirClientesTDE"))){
					if(delegateServices.isClienteTDE(rut)){
						clienteCO=new Cliente();
						logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1B|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_CLIENTE_TDE|DETALLE|Cliente TDE - Migrado|");
						planCO.setIconoErrorC2C("error_tienda.svg");
						planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.tde"));
						planCO.setSubMensajeErrorC2C("");
						planCO.setTipoError(Config.getAppProperty("url.tipo.error.tde"));
						planCO.setBtnErrorC2C("Conocer la m&aacute;s cercana");
						planCO.setActionErrorC2C(Constantes.TIENDA.toString());
						return paso1ErrorPage;
					}      
				}
				 
		        
		        try {
		        	
		        	//validaDatos = Utils.validarDatosCliente(clienteCO.getValidacionCliente().getIndicadorExito(), planCO);
		        	
				} catch (Exception e) {
					//error de servicio
					planCO.setIconoErrorC2C("error_triste.svg");
		        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
					planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
					planCO.setTipoError(Config.getAppProperty("url.tipo.error"));
					planCO.setBtnErrorC2C("Quiero que me llamen");
					planCO.setActionErrorC2C(Config.getAppProperty("cliick_"+planCO.getCargoPlan().replace(".", "")));
					LOGGER.error("Error Servicio Evaluacion Comercial | Rut : " + rut + "|"+Utils.obtenerDetalleError(e));
					logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1B|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+rut+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+email+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR| " + Utils.obtenerDetalleError(e) +"|");
				}
			       /* if(clienteCO.getValidacionCliente().getIndicadorExito().equals("O-000")){
			        	validaDatos = true;
			       
			        
					if(!validaDatos){
						logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1B|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+rut+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+email+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR| " + clienteCO.getValidacionCliente().getDescripcionValidacion() +"|");
						return paso1ErrorPage;
					}else{
					 }*/
						//Se setean los parametros enviados del formulario 1A, paramostrar en 1B
						clienteCO.setRut(rut);
						clienteCO.setRutFormateado(req.getParameter("rut"));
						clienteCO.setnSerie(nSerie);
						clienteCO.setEmail(email);
						
						/*
						clienteCO.setNombre(Utils.primeraLetraMayus(clienteCO.getValidacionCliente().getNombresCliente()));
						clienteCO.setApellidos(Utils.primeraLetraMayus(clienteCO.getValidacionCliente().getApellidos()));
						clienteCO.setNumeroNegocio(clienteCO.getValidacionCliente().getNumeroNegocio());
						
						clienteCO.setFirstName(Utils.primeraLetraMayus(Arrays.asList(clienteCO.getNombre().split(" ")).get(0)));
						clienteCO.setLastName(Utils.primeraLetraMayus(Arrays.asList(clienteCO.getApellidos().split(" ")).get(0)));
						*/
						//Parametros en sesion con nombre Cliente
						getSession().put("Cliente", clienteCO);
					//}
				}else{
					LOGGER.error(Utils.cabeceraLog() + "Error al validar captcha");
					logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1B|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_RECAPTCHA_INVALIDO|DETALLE|recaptcha invalido|");
					return paso1ErrorPage;
				}
			logMetricas.info("PORTAL|ContratacionOnline|TIPO|INFO|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1B|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP());
		
		} catch (Exception e) {
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1B|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			return paso1ErrorPage;
		}
	
		return paso1BPage ;
	}
	
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}


	public List<String> getTerminosYCondiciones() {
		return terminosYCondiciones;
	}


	public void setTerminosYCondiciones(List<String> terminosYCondiciones) {
		this.terminosYCondiciones = terminosYCondiciones;
	}  
	
	public String calculaDescuento(String cargoFijo, String dcto){
		Double cargoDcto = null;
		//Se consulta descuento para planes $9990, $19990, $21990 y $25990
		if(Arrays.asList(Config.getAppProperty("Planes.SinDescuento").split(",")).contains(cargoFijo)){
		    cargoDcto = Double.valueOf(cargoFijo);
		} else {
			cargoDcto = Double.valueOf(cargoFijo) - (Double.valueOf(cargoFijo) * Integer.parseInt(dcto)/100);
		}
		int cargo = cargoDcto.intValue();
		return String.valueOf(cargo);
	}
	
	
	public String errorAction(){
		
			HttpServletResponse response =(HttpServletResponse) ActionContext.getContext().get(StrutsStatics.HTTP_RESPONSE);	
			String url="https://www.entel.cl/planes/";
		    try {
				response.sendRedirect(url);		
				return super.execute();
			} catch (Exception e) {
				return "";
			}
	}
	
	public String getUrl() {
		return url;
	}
	
	public String getUrlHttps() {
		return urlHttps;
	}
	
	public void setUrlHttps(String urlHttps) {
		this.urlHttps = urlHttps;
	}
	
	/**
	 * Metodo para comprobar si es una peticion segura, si no lo es lo es redirigido a la misma url segura
	 * @param idPlan
	 * @return
	 */
	public Boolean urlSegura(String idPlan,String origen){
		try{
			HttpServletRequest req = ServletActionContext.getRequest();			
            if(!req.isSecure()){
            	HttpSession session=req.getSession();
            	session.invalidate();
                setUrlHttps("https://appswls.entel.cl/ContratacionOnLineMovil/paso1?origen="+origen+"&idPlan="+idPlan+"&paso=1A");
                return false;
            }
        }catch (Exception e){
        	logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
        	return false;
        }
		return true;
	}
}
