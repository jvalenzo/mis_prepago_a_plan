package com.esa.ponline.appmobile.web.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.email.MailDatosHelper;
import com.esa.ponline.appmobile.email.MailType;
import com.esa.ponline.appmobile.utils.FormatoMovilUtil;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.DataEmailVO;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.esa.ponline.appmobile.web.bean.ResTicketSGAVO;
import com.esa.ponline.appmobile.web.bean.TicketSGAVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cl.esa.ponline.appmobile.delegate.DelegateServices;

public class ContratacionPaso5Action extends ActionSupport  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7264720493176902590L;
	private static final Logger LOGGER = Logger.getLogger(ContratacionAction.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	private String paso5Page 			= "ContratacionPaso5";
	private String paso5ErrorPage 		= "ContratacionPaso5Error";
	private String url = "https://www.entel.cl/planes/";
	private Plan planCO;
	private Cliente clienteCO;
	
	private DelegateServices delegateServices = new DelegateServices();
	@Override
	public String execute(){
		clienteCO = (Cliente) getSession().get("Cliente");
		if(clienteCO.getFinalizoFlujo()){
			return "redirect";
		}
		clienteCO.setFinalizoFlujo(true);
		String response=paso5ErrorPage;
		HttpServletRequest req = ServletActionContext.getRequest();
		
		planCO = (Plan) getSession().get("Plan");
		
		try {
			response = contratacionPaso5(clienteCO, planCO);
		} catch (Exception e) {
			LOGGER.error("Error al procesar paso final: "+ Utils.obtenerDetalleError(e));
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso5|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
		}
		
		return response;
	}
	
	private String contratacionPaso5(Cliente clienteCO, Plan planCO) {
		String resp=paso5ErrorPage;
		String requerimiento="";
		
		if(clienteCO.getSeguridad().checkSeguridad()){

			String subOperacion=Utils.getSubOperacion(planCO.getOrigenOferta());
					
			//GENERO TICKET
			if(Boolean.valueOf(Config.getAppProperty("switchPRueba"))){
				requerimiento=	 " Solicitud:Portabilidad "+
						" - Hora/Fecha Solicitud: "+ Utils.getFechaHoraMinutoSistema()+
						" - Nombre: PRUEBA"+ 
					 	" - RUT: PRUEBA"+
					 	" - NumSerie: PRUEBA"+
					 	" - E-Mail: PRUEBA"+
					 	" - Telefono: PRUEBA"+
					 	" - NombrePlan: PRUEBA"+
					 	" - CodigoPlan: PRUEBA"+
					 	" - CompaniaOrigen: PRUEBA"+
					 	" - Mercado Origen: PRUEBA"+
					 	" - codigo cap: PRUEBA"+
					 	" - DatosPlan: PRUEBA"+
					 	" - MinutosPlan: PRUEBA"+
					 	" - CargoFijo: PRUEBA"+
					 	" - Numero a portar: PRUEBA"+
					 	" - Flujo: Contratacion Online Movil"+
					 	" - Motivo: Nuevas Lineas Portabilidad"+
					 	" - PortalOrigen: P�blico Mobile"+
					 	" - Numero Negocio: PRUEBA"+
					 	" - IMEI: 357598090494354"+
					 	" - Grupo Cliente: PRUEBA"+
					 	" - DireccionDespacho: PRUEBA"+
					 	" - Origen Web: "+ planCO.getOrigenOferta()+
					 	" - SubOperacion: "+ subOperacion+
					 	" ";
						if(Double.valueOf(planCO.getCargoPlanOferta())>0){
						requerimiento+=" - Recuerda realizar el cambio de plan a linea actual al Plan juntos "+planCO.getDatosPlan()+" GB";
						}
						if(null!=getSession().get("OfertaEspecifica") && getSession().get("OfertaEspecifica").equals(Config.getAppProperty(Constantes.OFERTA_ESPECIFICA_MENSAJE_CAMBIAR))){
						requerimiento+=" - Recuerda cambiar el plan actual a $"+planCO.getCargoPlan()+" + Linea Adicional de "+planCO.getDatosPlan()+"GB Promocion "+planCO.getPorcentajeDsctoEspecifico()+"% en lineas adicionales";
						}
			}else{
			requerimiento=	 " Solicitud:Portabilidad "+
													" - Hora/Fecha Solicitud: "+ Utils.getFechaHoraMinutoSistema()+
													" - Nombre: "+ clienteCO.getNombre() + " " + clienteCO.getApellidos()+
												 	" - RUT: "+ clienteCO.getRut() +
												 	" - NumSerie: "+ clienteCO.getnSerie()+
												 	" - E-Mail: "+ clienteCO.getEmail()+
												 	" - Telefono: "+ clienteCO.getTelefono()+
												 	" - NombrePlan: "+ planCO.getNombrePlan2() +
												 	" - CodigoPlan: "+ planCO.getCodigoPlan() +
												 	" - CompaniaOrigen: "+ clienteCO.getCompaniaOrigen()+
												 	" - Mercado Origen: " + (clienteCO.getMercadoOrigen() == 0 ? "PREPAGO" : "PLAN")+
												 	" - codigo cap: "+ clienteCO.getCodigoCAP()+
												 	" - DatosPlan: "+ planCO.getDatosPlan() +"GB"+
												 	" - MinutosPlan: "+ planCO.getMinutosPlan()+
												 	" - CargoFijo: "+ planCO.getCargoPlan()+
												 	" - Numero a portar: "+ clienteCO.getNumeroPorta()+
												 	" - Flujo: Contratacion Online Movil"+
												 	" - Motivo: Nuevas Lineas Portabilidad"+
												 	" - PortalOrigen: P�blico Mobile"+
												 	" - Numero Negocio: "+ clienteCO.getNumeroNegocio()+
												 	" - IMEI: 357598090494354"+
												 	" - Grupo Cliente: "+ clienteCO.getGrupoCliente()+
												 	" - DireccionDespacho:"+ clienteCO.getCalleDespacho() + " #"+ clienteCO.getNumeroDespacho() +" "+ clienteCO.getComunaDespacho() +", "+ clienteCO.getRegionDespacho()+
												 	" - Origen Web: "+ planCO.getOrigenOferta()+
												 	" - SubOperacion: "+ subOperacion+
												 	" ";
				if(Double.valueOf(planCO.getCargoPlanOferta())>0){
					requerimiento+=" - Recuerda realizar el cambio de plan a linea actual al Plan juntos "+planCO.getDatosPlan()+" GB";
				}
				if(null!=getSession().get("OfertaEspecifica") && getSession().get("OfertaEspecifica").equals(Config.getAppProperty(Constantes.OFERTA_ESPECIFICA_MENSAJE_CAMBIAR))){
					requerimiento+=" - Recuerda cambiar el plan actual a $"+planCO.getCargoPlan()+" + Linea Adicional de "+planCO.getDatosPlan()+"GB Promocion "+planCO.getPorcentajeDsctoEspecifico()+"% en lineas adicionales";
				}
			}
			
			LOGGER.info(Utils.cabeceraLog() + "REQUERIMIENTO:" +requerimiento);
			//booleano para indicar si esta en producci�n, esto ayuda a que en fase de desarrollo podamos finalizar el flujo.
			Boolean esProduccion = Boolean.valueOf(Config.getAppProperty("captchaProduccion"));
			if(esProduccion){
				if(generarTicket(clienteCO,requerimiento,planCO.getOrigenOferta())){
					resp=paso5Page;
				}
			}else{
				resp=paso5Page;
			}
			
			//ENVIO EMAIL
			String respuestaEmail = "";				
			try {
				respuestaEmail = envioMail("no-responder@entel.cl", "Bienvenido a Entel", clienteCO.getEmail());			
			} catch (Exception e) {
				LOGGER.error("Error al enviar el Email");
				e.printStackTrace();
			}
			
			if(respuestaEmail.equals("NoOk")){
				LOGGER.error("Env�o de correo fallido, revisar servicio o plantilla de correo");
			}
		
		}
		
		if(!resp.equals(paso5ErrorPage)){
			logMetricas.info("PORTAL|ContratacionOnline|TIPO|INFO|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso5|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP());
		}else{
			planCO.setIconoErrorC2C("error_triste.svg");
			planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
			planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
			planCO.setTipoError(Config.getAppProperty("url.tipo.error"));
			planCO.setBtnErrorC2C("Quiero que me llamen");
			planCO.setActionErrorC2C(Config.getAppProperty("cliick_"+planCO.getCargoPlan().replace(".", "")));
			getSession().put("Plan", planCO);
		}
		return resp;
	}
	
	private boolean generarTicket(Cliente clienteCO, String requerimiento,String origen){
		boolean resp=false;
		TicketSGAVO ticket = new TicketSGAVO();
		ResTicketSGAVO resTicket = new ResTicketSGAVO();
		if(clienteCO.getNombre()!=null && clienteCO.getRut()!=null && clienteCO.getNumeroPorta() != null ){
			ticket.setSuboperacion(Utils.getSubOperacion(origen));
			ticket.setRequerimientoCliente(requerimiento);
			ticket.setMotivo("Nuevas Lineas Portabilidad");
			
		}
		try {
			if(null != ticket){
				resTicket = delegateServices.ticketSGA(ticket);
			}
		} catch (Exception e) {
			LOGGER.error("Error al llamar al servicio que genera el ticket, detalle error: "+Utils.obtenerDetalleError(e) );
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso5|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_SERVICIO_TICKET_SGA|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			return resp;
		}
		if(resTicket.getCodigo().equals("0000")){
			resp = true;
		}else{
			LOGGER.error("TicketSGA cod. "+resTicket.getCodigo()+" Mensaje: "+ resTicket.getMensaje());
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso5|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_TICKET_SGA|" + resTicket.getMensaje()+"|");
		}
		
		return resp;
		
	}
	
	private String envioMail(String remitente,String asunto, String to) throws Exception {
	 	String respuesta = "NoOk";
		LOGGER.info(Utils.cabeceraLog() + "Ingresa a cuerpoEmailConfirmacion para el envio de correo de: remitente" + remitente + ", asunto "+asunto+", to "+ to);

		MailType objMailType = MailDatosHelper.getDatosMail();
		String strCuerpoEMail = MailDatosHelper.cuerpoEmailConfirmacion(objMailType);
		List<DataEmailVO> adjuntos = new ArrayList<DataEmailVO>();
		respuesta = delegateServices.enviaEmail(to, remitente, asunto,strCuerpoEMail, adjuntos );
		
		return respuesta;
		
	}
	
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}
	
	public String getUrl() {
		return url;
	}
}
