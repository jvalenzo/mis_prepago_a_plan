package com.esa.ponline.appmobile.web.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.esa.crm.portabilidad.t.obtenercompaniamovil.Operator;
import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.utils.FormatoMovilUtil;
import com.esa.ponline.appmobile.utils.PruebaUtil;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.DataEmailVO;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.esa.ponline.appmobile.web.bean.ReqConsultaTitularDeudaSyncVO;
import com.esa.ponline.appmobile.web.bean.ReqGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.ResGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.RespuestaGenerarCapVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cl.esa.ponline.appmobile.delegate.DelegateServices;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.RespuestaConsultaTitularDeudaSync;

public class ContratacionPaso2Action extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4420653111614019364L;
	private static final Logger LOGGER = Logger.getLogger(ContratacionPaso2Action.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	private String paso2APage 			= "ContratacionPaso2A";
	private String paso2BPage 			= "ContratacionPaso2B";
	private String paso2ErrorPage 		= "ContratacionPaso2Error";
	private File[] cedulaFront;
	private Cliente clienteCO;
	private Plan planCO;
	
	private DelegateServices delegateServices = new DelegateServices();
	
	ReqConsultaTitularDeudaSyncVO titularidadDeudaVO;
	RespuestaConsultaTitularDeudaSync responseTitularidad;
	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Override
	public String execute(){
		String response="";
		String numeroPorta = "";
		String compania = "";
		clienteCO = (Cliente) getSession().get("Cliente");
		//Se obtienen parametro de pasos con el fin de definir si va a pantalla A o B
		HttpServletRequest req = ServletActionContext.getRequest();
		String paso			= req.getParameter("paso").toString();	
		planCO = (Plan)getSession().get("Plan");
		if(paso.equals("2A")){
			String nombres			= Utils.reemplazarCaracteresExtranos(req.getParameter("nombreCliente").toString());
			String apellidos		= Utils.reemplazarCaracteresExtranos(req.getParameter("apellidoCliente").toString());
			numeroPorta		        = Utils.reemplazarCaracteresExtranos(req.getParameter("numeroPortar").toString());
			compania			    = "";
			String radio			= Utils.reemplazarCaracteresExtranos(req.getParameter("radio-group"));
			
			
			response = contratacionPaso2A(numeroPorta, compania, radio, nombres, apellidos);
			
		}else if(paso.equals("2B")){
			response = contratacionPaso2B();
		}		
		return response;
	}
	
	public String contratacionPaso2A( String numeroPorta, String compania, String radio, String nombre, String apellido) {
		int modalidad = 0;
		String response = paso2ErrorPage;
		clienteCO = (Cliente) getSession().get("Cliente");
			
		clienteCO.setNumeroPorta(numeroPorta);
		clienteCO.setTelefono(numeroPorta);
		clienteCO.setApellidos(apellido);
		clienteCO.setNombre(nombre);
		clienteCO.setMostrarOpcionChat(false);
		
		//Titularidad y deuda
		titularidadDeudaVO = new ReqConsultaTitularDeudaSyncVO();
		if(radio.toUpperCase().equals("PREPAGO")){
			modalidad = 0;
			titularidadDeudaVO.setRutTitular("");
			
		}else if(radio.toUpperCase().equals("PLAN")){
			modalidad = 1;
			titularidadDeudaVO.setRutTitular(clienteCO.getRut());
		}
		
		
		titularidadDeudaVO.setIdd("220");
		titularidadDeudaVO.setModalidad(modalidad);
		titularidadDeudaVO.setMsisdn(FormatoMovilUtil.obtieneFormatoMovilPorLargo(numeroPorta));
		titularidadDeudaVO.setRutSolicitante(clienteCO.getRut());
		
		titularidadDeudaVO.setTipoServicio(0);
		titularidadDeudaVO.setTipoServicioTo(0);
		
		responseTitularidad = new RespuestaConsultaTitularDeudaSync();
		
		try{
			responseTitularidad = delegateServices.consultaTitularDeudaSyncService(titularidadDeudaVO);
		}catch(Exception e){
			//error de servicio
			planCO.setIconoErrorC2C("error_triste.svg");
        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
			planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
			planCO.setBtnErrorC2C("Quiero que me llamen");
			planCO.setActionErrorC2C(Config.getAppProperty("cliick_"+planCO.getCargoPlan().replace(".", "")));
			getSession().put("Plan", planCO);
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso2A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_SERVICIO_TITULAR_DEUDA|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			return paso2ErrorPage;
		}
		
		try {
			
			//if para comprobar que el mercado seleccionado por el cliente PREPAGO o PLAN sea correcto
			if(responseTitularidad.getRespuestasANI().get(0).getModalidad() == modalidad){
				//if para validar el estado del numero de telefono
				if(responseTitularidad.getRespuestasANI().get(0).getEstado() == 0){
					//if para comprobar si el rut es el titular de la cuenta.
					if(responseTitularidad.getRespuestasANI().get(0).getTitularidad() == 0){
						if(responseTitularidad.getRespuestasANI().get(0).getSaldoPendienteANI()<=0){
							
							try{
								//Obtener Compania movil
								compania = ObtenerCompaniaMovil(numeroPorta);
							}catch(Exception e){
								planCO.setIconoErrorC2C("error_triste.svg");
					        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
								planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
								planCO.setBtnErrorC2C("Quiero que me llamen");
								planCO.setActionErrorC2C(Config.getAppProperty("cliick_"+planCO.getCargoPlan().replace(".", "")));
								getSession().put("Plan", planCO);
								logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso2A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_SERVICIO_OBTENER_COMPANIA|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
								return paso2ErrorPage;
							}				
							
							if(null != compania  && !compania.contains("ENTEL")){
								clienteCO.setCompaniaOrigen(compania);
								//getSession().put("Plan", planCO);
								if(radio.toUpperCase().equals("PREPAGO")){
									
									try{
										if(clienteCO.getMercadoOrigen()!=0){
											if(generaCAPPrepago(numeroPorta))
												clienteCO.setMercadoOrigen(0);
										}
										
									}catch(Exception e){
										//error de servicio
										planCO.setIconoErrorC2C("error_triste.svg");
							        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
										planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
										planCO.setBtnErrorC2C("Quiero que me llamen");
										planCO.setActionErrorC2C(Config.getAppProperty("cliick_"+planCO.getCargoPlan().replace(".", "")));
										getSession().put("Plan", planCO);
										logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso2A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_SERVICIO_CAP_PREPAGO|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
										return paso2ErrorPage;
									}
								}else if(radio.toUpperCase().equals("PLAN")){
									
									try{
										if(generaCAPPlan(numeroPorta))
											clienteCO.setMercadoOrigen(1);
									}catch(Exception e){
										//error de servicio
										planCO.setIconoErrorC2C("error_triste.svg");
							        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
										planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
										planCO.setBtnErrorC2C("Quiero que me llamen");
										planCO.setActionErrorC2C(Config.getAppProperty("cliick_"+planCO.getCargoPlan().replace(".", "")));
										getSession().put("Plan", planCO);
										logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso2A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_SERVICIO_CAP_SUSCRIPCION|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
										return paso2ErrorPage;
									}
								}
								getSession().put("Cliente", clienteCO);
								response = paso2BPage;
							}else{
								LOGGER.error("Numero no existente o no tiene compania");
								//error en la compa�ia ingresada
					        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.ErrorCompaniaIngresada"));
								planCO.setSubMensajeErrorC2C("");
								logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso2A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_MOVIL_INEXISTENTE|DETALLE|Numero no existe|");
								planCO.setTipoError(Config.getAppProperty("url.tipo.error.ErrorCompaniaIngresada"));
							}
						}else{
							LOGGER.error("El saldo pendiente no debe ser mayor a cero");
							//error tiene deuda en la otra compania
							planCO.setIconoErrorC2C("error_fatal.svg");
				        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.deudaCompaniaActual"));
							planCO.setSubMensajeErrorC2C("");
							planCO.setTipoError(Config.getAppProperty("url.tipo.error.deudaCompaniaActual"));
							planCO.setBtnErrorC2C("Entendido");
							planCO.setActionErrorC2C(Constantes.PLANES.toString());
							logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso2A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_DEUDA_COMPANIA_ORIGEN|DETALLE|Tiene deuda en la otra compania|");
						}
					}else{
						LOGGER.error("El rut solicitante no es TITULAR");
						//error No es el titular del numero a portar
						planCO.setIconoErrorC2C("error_triste.svg");
			        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.movilNoTitular"));
						planCO.setSubMensajeErrorC2C("");
						planCO.setTipoError(Config.getAppProperty("url.tipo.error.movilNoTitular"));
						planCO.setBtnErrorC2C("Entendido");
						planCO.setActionErrorC2C(planCO.getUrlOrigen());
						logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso2A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_TITULAR_MOVIL|DETALLE|El rut solicitante no es titular|");
					}
				}else{
					LOGGER.error("El estado es INACTIVO o no cumple con la permanencia minima");
					//error numero esta inactivo o no cumple con permanecnia minima.
					planCO.setIconoErrorC2C("error_triste.svg");
		        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.noCumpleAntiguedadOInactivo"));
					planCO.setSubMensajeErrorC2C("");
					planCO.setTipoError(Config.getAppProperty("url.tipo.error.noCumpleAntiguedadOInactivo"));
					planCO.setBtnErrorC2C("Entendido");
					planCO.setActionErrorC2C(Constantes.PLANES.toString());
					logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso2A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_ESTADO_MOVIL|DETALLE|Numero esta inactivo o no cumple con permanecnia minima.|");
				}
			}else{
				//error El mercado seleccionado no corresponde al mercado del movil
				planCO.setIconoErrorC2C("error_confuso.svg");
	        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorMercadoSeleccionado"));
				planCO.setSubMensajeErrorC2C("");
				planCO.setTipoError(Config.getAppProperty("url.tipo.error.errorMercadoSeleccionado"));
				planCO.setBtnErrorC2C("Entendido");
				planCO.setActionErrorC2C(planCO.getUrlOrigen());
				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso2A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_MERCADO_MOVIL|DETALLE|El mercado seleccionado no corresponde al mercado del movil|");
			}
			
		} catch (Exception e) {
			//error de servicio
			planCO.setIconoErrorC2C("error_triste.svg");
        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
			planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
			planCO.setTipoError(Config.getAppProperty("url.tipo.error"));
			planCO.setBtnErrorC2C("Quiero que me llamen");
			planCO.setActionErrorC2C(Config.getAppProperty("cliick_"+planCO.getCargoPlan().replace(".", "")));
			getSession().put("Plan", planCO);
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso2A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			return paso2ErrorPage;
		}
		getSession().put("Plan", planCO);
		if(!response.equals(paso2ErrorPage)){
			logMetricas.info("PORTAL|ContratacionOnline|TIPO|INFO|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso2A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP());
		}
	return response;
	}
	
	public String contratacionPaso2B() {			
		File cedulaDelantera = null;
		File cedulaTrasera = null;
		int cont = 1;
		String respuesta = "";
		LOGGER.info(Utils.cabeceraLog() + "\n\n Imagenes Cedula de Identidad");
		LOGGER.info(Utils.cabeceraLog() + "Archivos:");
		for (File u : cedulaFront) {
			LOGGER.info(Utils.cabeceraLog() + "Archivo : " + u.getName() + "\t" +"Tama�o archivo :"+ u.length());					
			if(cont == 1 ){cedulaDelantera = u;}
			if(cont == 2 ){cedulaTrasera = u;}
			cont++;
		}
		try{
			respuesta = guardarImagenesEmail(cedulaDelantera,cedulaTrasera);
		}catch (Exception e) {
			LOGGER.error("Al guardar las imagenes via correo electronico");
		}
		if(Boolean.valueOf(Config.getAppProperty("switchPRueba"))){
			PruebaUtil.creaDatosPrueba();
		}
		logMetricas.info("PORTAL|ContratacionOnline|TIPO|INFO|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso2B|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR|"+clienteCO.getNumeroPorta()+"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|CAP-SEGURIDAD|"+clienteCO.getCodigoCAP());
		return paso2BPage;
	}
	
	private byte[] cargaArchivo(File archivo) throws IOException {
		InputStream is = new FileInputStream(archivo);

	    long length = archivo.length();
	    if (length > Integer.MAX_VALUE) {
	        // File is too large
	    }
	    byte[] bytes = new byte[(int)length];

	    int offset = 0;
	    int numRead = 0;
	    while (offset < bytes.length
	           && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
	        offset += numRead;
	    }

	    if (offset < bytes.length) {
	        throw new IOException("No se puede leer el archivo :  "+archivo.getName());
	    }

	    is.close();
	    return bytes;
	}
	private String convertirImagenAString(File imagen) throws IOException {
		String imagenTexto = "";
		byte[] bytes = null;
		bytes = cargaArchivo(imagen);		
		byte[] encoded = Base64.encodeBase64(bytes);
		String encodedString = new String(encoded);
		imagenTexto = encodedString;
		return imagenTexto;
	}
	private String guardarImagenesEmail(File cedulaDelantera, File cedulaTrasera) {
		String to = "SER_flujomovil1@entel.cl";
		String from= "cedulascontratacion@entel.cl";
		String subject= "Cedulas contratacion";
		String body= "Estimado, recuerde cambiar la extension del archivo de .bin a .jpg para poder visualizar las imagenes. Saludos";
		String tImagen = "IMAGE";
		List<DataEmailVO> adjunt = new ArrayList<DataEmailVO>();
		String respuestaEmail = "";
		String cedulaDelanteraS = "";
		String cedulaTraseraS = "";
		try {
			cedulaDelanteraS = convertirImagenAString(cedulaDelantera);
			cedulaTraseraS   = convertirImagenAString(cedulaTrasera);
		} catch (IOException e1) {
			LOGGER.error("Error al convertir la imagen a texto");
			e1.printStackTrace();
		}
		
		DataEmailVO cedulaD = new DataEmailVO();
		cedulaD.setData(cedulaDelanteraS);
		cedulaD.setNombre("CedulaDelantera");
		cedulaD.setTipo(tImagen);
		adjunt.add(cedulaD);
		DataEmailVO cedulaT = new DataEmailVO();		
		cedulaT.setData(cedulaTraseraS);
		cedulaT.setNombre("CedulaTrasera");
		cedulaT.setTipo(tImagen);
		adjunt.add(cedulaT);
		//Envio Email con adjuntos
		try {
			respuestaEmail = delegateServices.enviaEmail(to, from, subject, body, adjunt);
		} catch (Exception e) {
			LOGGER.error("Error al enviar las imagenes de la cedula por email");
			e.printStackTrace();
		}
		return respuestaEmail;
	}
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}
	
	private String ObtenerCompaniaMovil(String movilCliente) throws Exception{
		String compania = null;
		movilCliente = FormatoMovilUtil.obtieneFormatoMovilPorLargo(movilCliente);
		try {
			Operator op = new Operator();
			op = delegateServices.obtieneCompania(movilCliente);
			if(!op.getOperatorDsc().equals("UNK")){
				compania = op.getOperatorDsc();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return compania;
	}
	
	private Boolean generaCAPPrepago(String movilCliente) throws Exception{
		RespuestaGenerarCapVO respGeneraCAP = new RespuestaGenerarCapVO();
		movilCliente = FormatoMovilUtil.obtieneFormatoMovilPorLargo(movilCliente);
		Boolean resp = false;
		try {
			respGeneraCAP = delegateServices.generarCap(movilCliente, "220");
			if(respGeneraCAP.getErrorCode().equals("0")){
				resp=true;
			}
		} catch (Exception e) {
			resp = false;
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return resp;
	}
	
	private Boolean generaCAPPlan(String movilCliente) throws Exception{
		ReqGeneraCapSuscripcionVO req = new ReqGeneraCapSuscripcionVO();
		ResGeneraCapSuscripcionVO respGeneraCAP = new ResGeneraCapSuscripcionVO();
		movilCliente = FormatoMovilUtil.obtieneFormatoMovilPorLargo(movilCliente);
		//Setea datos del request
		req.setNumeroMovil(movilCliente);
		req.setCodPlataforma("ONL");
		Boolean resp = false;
		try {
			respGeneraCAP = delegateServices.generarCapSuscripcion(req);
			if(respGeneraCAP.getIndicadorExito().equals("O-000")){
				resp = true;
			}else if(respGeneraCAP.getIndicadorExito().equals("N-10003")){
				//Intentos excedidos.
				throw new Exception();
			}
		} catch (Exception e) {
			resp = false;
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return resp;
	}

	
	
	public File[] getCedulaFront() {
		return cedulaFront;
	}

	public void setCedulaFront(File[] cedulaFront) {
		this.cedulaFront = cedulaFront;
	}

}
