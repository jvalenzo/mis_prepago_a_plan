package com.esa.ponline.appmobile.web.actions;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.tempuri.Region;

import com.epcs.clientes.evaluacion.types.EvaluacionComercialNecResponseType;
import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.utils.PruebaUtil;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.DatosComercialesVO;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cl.esa.ponline.appmobile.delegate.DelegateServices;

public class NuevaLineaPaso2Action  extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2310439782487462414L;
	private static final Logger LOGGER = Logger.getLogger(NuevaLineaPaso2Action.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	private String paso2Page 			= "NuevaLineaPaso2";
	private String paso2ErrorPage 		= "NuevaLineaPaso2Error";
	
	//private Plan plan;
	private Cliente clienteCO;
	private Plan plan;
	private List<String> comunas;
	private List<Region> regiones;
	
	private DelegateServices delegateServices = new DelegateServices();
	
	
	@Override
	public String execute(){
		String response=paso2ErrorPage;
		HttpServletRequest req = ServletActionContext.getRequest();
		
		try {
			clienteCO = (Cliente) getSession().get("Cliente");
			plan = (Plan)getSession().get("Plan");
			
			String nombres			= Utils.reemplazarCaracteresExtranos(req.getParameter("nombreCliente").toString());
			String apellidos		= Utils.reemplazarCaracteresExtranos(req.getParameter("apellidosCliente").toString());
			clienteCO.setNombre(nombres);
			clienteCO.setApellidos(apellidos);
			getSession().put("Cliente", clienteCO);
			
			response = contratacionPaso2(clienteCO, plan);
			if(!response.equals(paso2ErrorPage)){
				logMetricas.info("PORTAL|ContratacionOnline|TIPO|INFO|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso2|TEST|"+plan.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|NUMEROAPORTAR||COMPANIA||MERCADO||CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|");
			}
			getSession().put("Cliente", clienteCO);
		} catch (Exception e) {
			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso2|TEST|"+plan.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|NUMEROAPORTAR||COMPANIA||MERCADO|"+ clienteCO.getMercadoOrigen() + "|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
		}
		
		return response;
	}

	private String contratacionPaso2(Cliente clienteCO, Plan plan) {
		String response = paso2ErrorPage;
		
		if(Boolean.valueOf(Config.getAppProperty("switchPRueba"))){
			PruebaUtil.creaDatosPrueba();
			response = paso2Page;
		}else{
//			if(evaluacionComercial(clienteCO, plan)){
//					response = paso2Page;
//			}
		}
		return response;
	} 
	
//	private boolean evaluacionComercial(Cliente clienteCO, Plan plan){
//		EvaluacionComercialNecResponseType res = new EvaluacionComercialNecResponseType();
//		boolean resp = false;
//		String[] rutSplit = clienteCO.getRut().split("-");
//		String nRut=rutSplit[0];
//		String dRut=rutSplit[1];
//		DatosComercialesVO datosComercialCliente;
//		
//		try {
//			datosComercialCliente = delegateServices.obtenerDatosCliente(clienteCO.getRut().replace("-", ""));
//			
//			res = delegateServices.obtenerEvaluacionComercial(nRut, dRut, clienteCO.getnSerie(), datosComercialCliente);
//			
//			if(res.getCodigoSalida().equals("0000")){
//				clienteCO.setLineasDisponibles(res.getNmroLineasDisponibles());
//				//Lineas disponibles > 0
//				if(Integer.parseInt(res.getNmroLineasDisponibles())>0){
//					if(res.getCodiGrupoCliente().equals("23")){
//							resp = Utils.comprobarGrupoCliente23(clienteCO, plan, res.getCodiGrupoCliente());
//							if(!resp){
//								logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso2|TEST|"+plan.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|MERCADO|"+ clienteCO.getMercadoOrigen() + "|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|Grupo cliente invalido|");
//							}
//					}else{
//						clienteCO.setGrupoCliente(res.getCodiGrupoCliente());
//						resp = true;
//					}
//					/*if(res.getClasificacionDicom().equals("C") || res.getClasificacionDicom().equals("D") || !res.getSalidaDicom().getTotalDeuda().equals("0")){
//						//se muesta mismo mensaje de sin lineas disponibles.
//						plan.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.sinLineasDispopnibles"));
//						plan.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
//						logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|USUARIO|"+clienteCO.getTelefono()+"|RUT|"+  clienteCO.getRut() + "|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso2|NUMEROSERIE|"+clienteCO.getnSerie()+"|NUMERONEGOCIO|"+ clienteCO.getNumeroNegocio() +"|CAPTCHA|true|NUMEROAPORTAR|"+clienteCO.getNumeroPorta() +"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|RESPUESTAEMAIL||CODIGO PLAN|"+ plan.getCodigoPlan()+"|CARGO FIJO|"+ plan.getCargoPlan()+ "|GRUPO CLIENTE|" + res.getCodiGrupoCliente() + "|DESCRIPCION ERROR|Cliente con Dicom nivel:"+ res.getClasificacionDicom() +"|");
//						return false;
//					}*/
//				}else{
//					LOGGER.error("NO tiene lineas disponibles.");
//					plan.setIconoErrorC2C("error_fatal.svg");
//					plan.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.sinLineasDispopnibles"));
//					plan.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.subMensajeComercial"));
//					plan.setBtnErrorC2C("Conocer la m&aacute;s cercana");
//					plan.setActionErrorC2C(Constantes.TIENDA.toString());
//					logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso2|TEST|"+plan.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|MERCADO|"+ clienteCO.getMercadoOrigen() + "|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|SIN LINEAS DISPONIBLES|");
//				}
//			}
//			
//		} catch (Exception e) {
//			LOGGER.error("Error al llamar al servicio de Evaluacion comercial");
//			plan.setIconoErrorC2C("error_triste.svg");
//			plan.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
//			plan.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
//			plan.setBtnErrorC2C("Quiero que me llamen");
//			plan.setActionErrorC2C("$('#modalSeleccionFlujo').modal('show');");
//			logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+plan.getOrigenOferta()+"|ETAPA|Paso2|TEST|"+plan.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|MERCADO|"+ clienteCO.getMercadoOrigen() + "|CODIGOPLAN|"+plan.getCodigoPlan()+"|NOMBREPLAN|"+plan.getNombrePlanCompleto()+"|CARGO FIJO|"+ plan.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR| " + Utils.obtenerDetalleError(e) +"|");
//		}
//		
//		return resp;
//	}

	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}

	public List<String> getComunas() {
		return comunas;
	}

	public void setComunas(List<String> comunas) {
		this.comunas = comunas;
	}
	
	public List<Region> getRegiones(){
		return regiones;
	}
	
	public void setRegiones(List<Region> regiones){
		this.regiones = regiones;
	}
		
}
