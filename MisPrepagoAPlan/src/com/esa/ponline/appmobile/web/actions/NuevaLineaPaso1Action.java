package com.esa.ponline.appmobile.web.actions;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.helper.OfertaContratacionOnlineHelper;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cl.esa.ponline.appmobile.delegate.DelegateServices; 

public class NuevaLineaPaso1Action extends ActionSupport {
	
		private static final long serialVersionUID = -3537986217489964114L;
		private static final Logger LOGGER      = Logger.getLogger(NuevaLineaPaso1Action.class);
		private static final Logger	logMetricas	= Logger.getLogger("Metricas");
		private String paso1APage 			= "NuevaLineaPaso1A";
		private String paso1BPage 			= "NuevaLineaPaso1B";
		private String paso1ErrorPage 		= "NuevaLineaPAso1Error";
		private DelegateServices delegateServices = new DelegateServices();
		public static String secret = "";
		public static Boolean esProduccion = true;
		private String url = "https://www.entel.cl/planes/";
		private String urlHttps = "";
		private String classOferta = "";

				
		private Plan planCO;
		private Cliente clienteCO;
	
		@Override
		public String execute(){		
			String response=paso1ErrorPage;
			planCO = new Plan();
			clienteCO=new Cliente();
			//Se obtienen los parametros que se entregaran en la URL desde el portal publico
			HttpServletRequest req = ServletActionContext.getRequest();		
			HttpSession session=req.getSession();
			getSession().put("invocarWSC2CEntel", Boolean.valueOf(Config.getAppProperty("solicitud.c2c.invocar.a.ws.entel")));
			
			try {
				
				//Se define si el paso a entregar sera A o B.
				if(req.getParameter("paso").toString().equals("1A")){
					planCO.setIdPlan(Utils.reemplazarCaracteresExtranos(req.getParameter("idPlan")));
					
					//se rescata el origen de la oferta
					if(null != req.getParameter("origen")){
						planCO.setOrigenOferta(Utils.reemplazarCaracteresExtranos(req.getParameter("origen")));
					}
					getSession().put("Inbound", Utils.numeroInbound(planCO.getOrigenOferta()));
					
					//Descuento Especifico
					if(null != req.getParameter("k")){
						planCO.setBuscarDescuentoEspecifico(Utils.ajustesDesuentosEspecificos(req.getParameter("k")));
					}else{
						getSession().put("OfertaEspecifica", null);
					}
					
					//logica para determinar si el C2C se debe activar o no
					Utils.activacionC2C(session, req.getParameter("c2"));
					
					//atributo para identificar si se debe mostrar el btn de captcha para no repetir en caso de paso 1 de porta.
					getSession().put("mostrarBtnCaptcha", true);
					getSession().put("siteKeyCaptcha", Utils.siteKeyCaptcha());
					getSession().put("activarChat", Boolean.valueOf(Config.getAppProperty("mostrar.chat.teayudo")));
					
					
					response = contratacionPaso1A();
				}else if(req.getParameter("paso").toString().equals("1B")){
					response = contratacionPaso1B();
				}else{
					session=req.getSession();
	            	session.invalidate();
					return "redirect";
				}
			} catch (Exception e) {
				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso"+ clienteCO.getPaso() + "|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|MERCADO|"+ clienteCO.getMercadoOrigen() + "|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
			}
			
			return response;
		}
		

		public String contratacionPaso1A() {
			LOGGER.info(Utils.cabeceraLog() + "Paso 1 A - NuevaLinea");
			esProduccion = Boolean.valueOf(Config.getAppProperty("captchaProduccion"));
			boolean redireccionHTTPS = Boolean.valueOf(Config.getAppProperty("redireccionHTTPS"));
			secret = Utils.obtenerKeyCaptcha(esProduccion);
			HttpServletRequest req = ServletActionContext.getRequest();
			
			
			try {
				
				if(redireccionHTTPS && !urlSegura(planCO.getIdPlan(),planCO.getOrigenOferta())){
					return "redirectHttps";
				}
				
				planCO.setUrlOrigen(Utils.urlOriginal(planCO));
				
				clienteCO.setFlujo("NuevaLinea");
				clienteCO.setPaso(req.getParameter("paso").toString());
				
				try {
					//se consulta la disponibilidad de horario del C2C, en caso de estar fuera de horario se agenda para el d�a siguiente.
					Boolean clickToCallFueraHorario = delegateServices.consultarHorarioC2C();
					clienteCO.setC2cFueraHorario(clickToCallFueraHorario);
				} catch (Exception e) {
					LOGGER.error("Error al consultar horario de C2C");
				}
				
				
				try{				
					if(req.getParameter("token") != null){	
						String token=req.getParameter("token");
						String[] datos=Utils.parseToken(token);
						clienteCO.setRut(datos[1]);
						clienteCO.setTelefono(datos[0]);
					}
					
					OfertaContratacionOnlineHelper.obtenerDetallePlan(planCO, planCO.getIdPlan());
					
					Utils.iniciarContadorPlanes();		
					if(null==getSession().get("Test")){
						String testAB=Utils.seleccionarTest(Utils.incrementarContador(planCO.getIdPlan()));
						getSession().put("Test", testAB);
						planCO.setTest(testAB);
					}else{
						planCO.setTest(getSession().get("Test").toString());
					}
				}catch(Exception e){
										
					LOGGER.error("Error al ingresar los parametros de la URL paso1-NL:");
					logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|MERCADO|"+ clienteCO.getMercadoOrigen() + "|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_URL|DETALLE|Url no es correcta|");
					HttpSession session=req.getSession();
                	session.invalidate();
					return "redirect";
				}
				
				if(planCO.isMostrarOferta() && planCO.getTieneDescuento()){
					classOferta = "ocultar";
					getSession().put("ClassOferta", classOferta);
				}else{
					getSession().put("ClassOferta", "");
				}
				
				//Se deja en sesion el objeto PLAN
				getSession().put("Plan", planCO);
				getSession().put("Cliente", clienteCO);

				//Log Paso OK	
				logMetricas.info("PORTAL|ContratacionOnline|TIPO|INFO|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+"|NUMEROAPORTAR||COMPANIA||CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|");
			} catch (Exception e) {
				//error de servicio
				planCO.setIconoErrorC2C("error_triste.svg");
	        	planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicio"));
				planCO.setSubMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje"));
				planCO.setTipoError(Config.getAppProperty("url.tipo.error"));
				planCO.setBtnErrorC2C("Quiero que me llamen");
				planCO.setActionErrorC2C("$('#modalSeleccionFlujo').modal('show');");
				
				LOGGER.error("Error al procesar controlador del paso1-NL:" + Utils.obtenerDetalleError(e));
				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|MERCADO|"+ clienteCO.getMercadoOrigen() + "|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
				//Se deja en sesion el objeto PLAN
				getSession().put("Plan", planCO);
				
				return paso1ErrorPage;
			}
						
			return paso1APage;
		}
		
		public String contratacionPaso1B() {
			LOGGER.info(Utils.cabeceraLog() + "Paso 1 B - NuevaLinea");	
			
			clienteCO = (Cliente)getSession().get("Cliente");
			if(clienteCO != null && clienteCO.getVolver().isVolvioPaso1()){
				clienteCO.getVolver().setVolvioPaso1(false);
				return paso1BPage;
			}
			//Flag para guardar estado del c2c
			Boolean clickToCallFueraHorario = clienteCO.getC2cFueraHorario();
			
			clienteCO = new Cliente();
			clienteCO.setFlujo("NuevaLinea");
			/*si es que es que paso valdiaci�n anterior significa que no viene desde un volver, 
			y en ese caso se cambia el flag a true para el caso en que se decida volver nuevamente */
			clienteCO.getVolver().setVolvioPaso1(true);
			
			//se setea atributo del c2c fuera de horario para no perder el valor al inciializar el cliente neuvamente.
			clienteCO.setC2cFueraHorario(clickToCallFueraHorario);
			
			planCO = (Plan)getSession().get("Plan");
			boolean validaDatos = false;
	        String nRut;
	        String dRut;
	        
			//Trae datos
			HttpServletRequest req = ServletActionContext.getRequest();
			String rut			= Utils.reemplazarCaracteresExtranos(req.getParameter("rut"));
			String telefono		= Utils.reemplazarCaracteresExtranos(req.getParameter("telefono"));
			String email="";
			if(planCO.getTest().equals("A")){
				email = Utils.reemplazarCaracteresExtranosEmail(req.getParameter("email"));
			}
			
			
//			boolean validaRecapcha = false;
//			try {
//				validaRecapcha = Utils.validarRecaptcha(esProduccion, req.getParameter("g-recaptcha-response"), secret);
//			} catch (Exception e1) {
//				LOGGER.info(Utils.cabeceraLog() + "error al validar captcha: " + Utils.obtenerDetalleError(e1));
//				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|USUARIO|"+telefono+"|RUT|"+  rut + "|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1B|NUMERONEGOCIO||CAPTCHA|"+validaRecapcha+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|DESCRIPCION ERROR|"+ Utils.obtenerDetalleError(e1) +"|");
//				return paso1ErrorPage;
//			}		
			
			try {
//				if(validaRecapcha){
					rut = rut.replace(".", "");
					clienteCO.setRut(rut);
					//Se setean los parametros enviados del formulario 1A, paramostrar en 1B
					clienteCO.setRutFormateado(Utils.reemplazarCaracteresExtranos(req.getParameter("rut")));
					clienteCO.setTelefono(telefono);
					if(planCO.getTest().equals("A")){
						clienteCO.setEmail(email);
					}				
					
					//Se guarda altiro en session para logs.
					getSession().put("Cliente", clienteCO);
					
					if(Boolean.parseBoolean(Config.getAppProperty("restringirClientesTDE"))){
						if(delegateServices.isClienteTDE(rut)){
							logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1B|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|MERCADO|"+ clienteCO.getMercadoOrigen() + "|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_CLIENTE_TDE|DETALLE|Cliente TDE - Migrado|");
							planCO.setIconoErrorC2C("error_tienda.svg");
							planCO.setMensajeErrorC2C(Config.getAppProperty("mensaje.error.click.to.call.tde"));
							planCO.setSubMensajeErrorC2C("");
							planCO.setTipoError(Config.getAppProperty("url.tipo.error.tde"));
							planCO.setBtnErrorC2C("Conocer la m&aacute;s cercana");
							planCO.setActionErrorC2C(Constantes.TIENDA.toString());
							return paso1ErrorPage;
						}
					}
		
				        String[] rutSplited = rut.replace(".", "").split("-");
				        if(rutSplited.length>1){
					        nRut			= rutSplited[0];
					        dRut         	= rutSplited[1];
					        
				        }else{
				        	logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1B|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|MERCADO|"+ clienteCO.getMercadoOrigen() + "|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|Error al informar rut para validar cliente|");
				        	return paso1ErrorPage;
				        }		       
				        
						//Parametros en sesion con nombre Cliente
						getSession().put("Cliente", clienteCO);
						
//				}else{
//					logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|USUARIO|"+telefono+"|RUT|"+  rut + "|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1B|NUMERONEGOCIO||CAPTCHA|"+validaRecapcha+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|DESCRIPCION ERROR|recaptcha invalido|");
//					return paso1ErrorPage;
//				}
				logMetricas.info("PORTAL|ContratacionOnline|TIPO|INFO|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1B|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|NUMEROAPORTAR||COMPANIA||CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|");
			} catch (Exception e) {
				logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1B|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|MERCADO|"+ clienteCO.getMercadoOrigen() + "|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
				return paso1ErrorPage;
			}
	
			return paso1BPage ;
		}
		
		
		private Map<String, Object> getSession() {
			Map<String, Object> attibutes = ActionContext.getContext().getSession();
			return attibutes;
		}


		public String getUrl() {
			return url;
		}
		
		public String getUrlHttps() {
			return urlHttps;
		}
		
		public void setUrlHttps(String urlHttps) {
			this.urlHttps = urlHttps;
		}
		
		/**
		 * Metodo para comprobar si es una peticion segura, si no lo es lo es redirigido a la misma url segura
		 * @param idPlan
		 * @return
		 */
		public Boolean urlSegura(String idPlan,String origen){
			try{
				HttpServletRequest req = ServletActionContext.getRequest();			
                if(!req.isSecure()){
                	HttpSession session=req.getSession();
                	session.invalidate();
                    setUrlHttps("https://appswls.entel.cl/ContratacionOnLineMovil/nl_paso1?origen="+origen+"&idPlan="+idPlan+"&paso=1A");
                    return false;
                }
            }catch (Exception e){
            	logMetricas.error("PORTAL|ContratacionOnline|TIPO|ERROR|FLUJO|"+ clienteCO.getFlujo()+"|ORIGEN|"+planCO.getOrigenOferta()+"|ETAPA|Paso1A|TEST|"+planCO.getTest()+"|USUARIO|"+clienteCO.getTelefono()+"|GRUPOCLIENTE|"+clienteCO.getGrupoCliente()+"|RUT|"+clienteCO.getRut()+"|NUMEROSERIE|"+clienteCO.getnSerie()+"|EMAIL|"+clienteCO.getEmail()+ "|MERCADO|"+ clienteCO.getMercadoOrigen() + "|CODIGOPLAN|"+planCO.getCodigoPlan()+"|NOMBREPLAN|"+planCO.getNombrePlanCompleto()+"|CARGO FIJO|"+ planCO.getCargoPlan()+"|REGION|"+ clienteCO.getRegionDespacho()+"|COMUNA|"+ clienteCO.getComunaDespacho()+"|CALLE|"+ clienteCO.getCalleDespacho()+"|NUMERO|"+ clienteCO.getNumeroDespacho()+"|DESCRIPCION ERROR|ERROR_PROCESO_DATOS|DETALLE|" + Utils.obtenerDetalleError(e) +"|");
            	return false;
            }
			return true;
		}


		public String getClassOferta() {
			return classOferta;
		}
}