package com.esa.ponline.appmobile.web.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.epcs.clientes.evaluacion.types.EvaluacionComercialNecResponseType;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.ServiceException;
import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.email.MailDatosHelper;
import com.esa.ponline.appmobile.email.MailType;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.DataEmailVO;
import com.esa.ponline.appmobile.web.bean.DatosComercialesVO;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.esa.ponline.appmobile.web.bean.ResTicketSGAVO;
import com.esa.ponline.appmobile.web.bean.TicketSGAVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cl.esa.ponline.appmobile.delegate.DelegateServices;

public class VolverAction extends ActionSupport {

	private static final long serialVersionUID = -269262279008923256L;
	private static final Logger LOGGER = Logger.getLogger(VolverAction.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	
	private String paso1PageNL 			= "NuevaLineaPaso1";
	private String paso2PageNL 			= "NuevaLineaPaso2";
	
	private String paso1APage 			= "ContratacionPaso1A";
	private String paso1BPage 			= "ContratacionPaso1B";
	private String paso2BPage 			= "ContratacionPaso2B";
	private String paso3Page 			= "ContratacionPaso3";
	
	private Cliente clienteCO;

	public String volverPaso1NL(){
		return paso1PageNL;
	}
	
	public String volverPaso2NL(){
		return paso2PageNL;
	}
	
	public String volverPaso1(){
		clienteCO = (Cliente) getSession().get("Cliente");
		clienteCO.setMostrarOpcionChat(false);
		getSession().put("Cliente", clienteCO);
		return paso1APage;
	}
	
	public String volverPaso2(){
		clienteCO = (Cliente) getSession().get("Cliente");
		clienteCO.setMostrarOpcionChat(true);
		getSession().put("Cliente", clienteCO);
		return paso1BPage;
	}
	public String volverPaso3(){
		return paso2BPage;
	}
	public String volverPaso4(){
		return paso3Page;
	}

	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}
}
