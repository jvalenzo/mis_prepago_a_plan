package com.esa.ponline.appmobile.web.bean;

import com.esa.ponline.appmobile.config.Config;

public class Plan {
	private String urlOrigen;
	private String idPlan;
	private String nombrePlanCompleto;
	private String nombrePlan2;
	private String datosPlan; 
	private String rrssPlan;
	private String smsPlan;
	private String minutosPlan;
	private String cargoPlan;
	private String cargoPlanOferta;
	private String descuentoPlanOferta;
	private String cargoPlanMeses;
	private String cargoDcto;
	private String actionErrorC2C;
	private String codigoPlan;
	private String mensajeErrorC2C;
	private String subMensajeErrorC2C;
	private String iconoErrorC2C;
	private String btnErrorC2C;
	private Boolean tieneDescuento;
	private String valorDelDescuento;
	private String mensajeDespacho;
	private Boolean esMiPrimerPlan;
	private String origenOferta;
	private Boolean mostrarBtnClickToCall;
	private ParametrosURL paramURL;
	private String test;
	private String duracionOferta;
	private int porcentajeDsctoOferta;
	private String vigenciaOferta;
	private boolean mostrarOferta;
	private boolean buscarDescuentoEspecifico;
	private boolean tieneDescuentoEspecifico;
	private int porcentajeDsctoEspecifico;
	private String valorDctoEspecifico;
	private String tipoError;
	
	public Plan(){
		this.iconoErrorC2C="error_triste.svg";
		this.mensajeErrorC2C = Config.getAppProperty("mensaje.error.click.to.call.errorServicio");
		this.subMensajeErrorC2C = Config.getAppProperty("mensaje.error.click.to.call.errorServicioSubMensaje");
		this.btnErrorC2C = "Quiero que me llamen";
		this.esMiPrimerPlan = false;
		this.origenOferta = "";
		this.mostrarBtnClickToCall = true;
		this.paramURL = new ParametrosURL();
		this.test="";
		this.duracionOferta = "12";
		this.porcentajeDsctoOferta = 10;
		this.vigenciaOferta = "";
		this.mostrarOferta = true;
		this.buscarDescuentoEspecifico=false;
		this.tieneDescuentoEspecifico=false;
		this.porcentajeDsctoEspecifico=0;
		this.tieneDescuento=false;
		this.tipoError= Config.getAppProperty("url.tipo.error");
		this.nombrePlanCompleto = "";
	}
	
	
	
	public String getUrlOrigen() {
		return urlOrigen;
	}



	public void setUrlOrigen(String urlOrigen) {
		this.urlOrigen = urlOrigen;
	}



	public String getIdPlan() {
		return idPlan;
	}
	public void setIdPlan(String idPlan) {
		this.idPlan = idPlan;
	}
	public String getDatosPlan() {
		return datosPlan;
	}
	public void setDatosPlan(String datosPlan) {
		this.datosPlan = datosPlan;
	}
	public String getRrssPlan() {
		return rrssPlan;
	}
	public void setRrssPlan(String rrssPlan) {
		this.rrssPlan = rrssPlan;
	}
	public String getMinutosPlan() {
		return minutosPlan;
	}
	public void setMinutosPlan(String minutosPlan) {
		this.minutosPlan = minutosPlan;
	}
	public String getCargoPlan() {
		return cargoPlan;
	}
	public void setCargoPlan(String cargoPlan) {
		this.cargoPlan = cargoPlan;
	}
	public String getActionErrorC2C() {
		return actionErrorC2C;
	}
	public void setActionErrorC2C(String actionErrorC2C) {
		this.actionErrorC2C = actionErrorC2C;
	}
	public String getCargoDcto() {
		return cargoDcto;
	}
	public void setCargoDcto(String cargoDcto) {
		this.cargoDcto = cargoDcto;
	}
	public String getCodigoPlan() {
		return codigoPlan;
	}
	public void setCodigoPlan(String codigoPlan) {
		this.codigoPlan = codigoPlan;
	}
	public String getNombrePlan2() {
		return nombrePlan2;
	}
	public void setNombrePlan2(String nombrePlan2) {
		this.nombrePlan2 = nombrePlan2;
	}
	public String getMensajeErrorC2C() {
		return mensajeErrorC2C;
	}
	public void setMensajeErrorC2C(String mensajeErrorC2C) {
		this.mensajeErrorC2C = mensajeErrorC2C;
	}
	public String getSubMensajeErrorC2C() {
		return subMensajeErrorC2C;
	}
	public void setSubMensajeErrorC2C(String subMensajeErrorC2C) {
		this.subMensajeErrorC2C = subMensajeErrorC2C;
	}

	public String getSmsPlan() {
		return smsPlan;
	}

	public void setSmsPlan(String smsPlan) {
		this.smsPlan = smsPlan;
	}

	public Boolean getTieneDescuento() {
		return tieneDescuento;
	}

	public void setTieneDescuento(Boolean tieneDescuento) {
		this.tieneDescuento = tieneDescuento;
	}

	public String getValorDelDescuento() {
		return valorDelDescuento;
	}

	public void setValorDelDescuento(String valorDelDescuento) {
		this.valorDelDescuento = valorDelDescuento;
	}

	public String getMensajeDespacho() {
		return mensajeDespacho;
	}

	public void setMensajeDespacho(String mensajeDespacho) {
		this.mensajeDespacho = mensajeDespacho;
	}

	public String getNombrePlanCompleto() {
		return nombrePlanCompleto;
	}

	public void setNombrePlanCompleto(String nombrePlanCompleto) {
		this.nombrePlanCompleto = nombrePlanCompleto;
	}

	public Boolean getEsMiPrimerPlan() {
		return esMiPrimerPlan;
	}

	public void setEsMiPrimerPlan(Boolean esMiPrimerPlan) {
		this.esMiPrimerPlan = esMiPrimerPlan;
	}

	public String getOrigenOferta() {
		return origenOferta;
	}

	public void setOrigenOferta(String origenOferta) {
		this.origenOferta = origenOferta;
	}

	public Boolean getMostrarBtnClickToCall() {
		return mostrarBtnClickToCall;
	}

	public void setMostrarBtnClickToCall(Boolean mostrarBtnClickToCall) {
		this.mostrarBtnClickToCall = mostrarBtnClickToCall;
	}

	public ParametrosURL getParamURL() {
		return paramURL;
	}

	public void setParamURL(ParametrosURL paramURL) {
		this.paramURL = paramURL;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public String getDuracionOferta() {
		return duracionOferta;
	}

	public void setDuracionOferta(String duracionOferta) {
		this.duracionOferta = duracionOferta;
	}

	public int getPorcentajeDsctoOferta() {
		return porcentajeDsctoOferta;
	}

	public void setPorcentajeDsctoOferta(int porcentajeDsctoOferta) {
		this.porcentajeDsctoOferta = porcentajeDsctoOferta;
	}

	public String getVigenciaOferta() {
		return vigenciaOferta;
	}

	public void setVigenciaOferta(String vigenciaOferta) {
		this.vigenciaOferta = vigenciaOferta;
	}

	public String getIconoErrorC2C() {
		return iconoErrorC2C;
	}

	public void setIconoErrorC2C(String iconoErrorC2C) {
		this.iconoErrorC2C = iconoErrorC2C;
	}

	public String getBtnErrorC2C() {
		return btnErrorC2C;
	}

	public void setBtnErrorC2C(String btnErrorC2C) {
		this.btnErrorC2C = btnErrorC2C;
	}

	public boolean isMostrarOferta() {
		return mostrarOferta;
	}

	public void setMostrarOferta(boolean mostrarOferta) {
		this.mostrarOferta = mostrarOferta;
	}



	public boolean isTieneDescuentoEspecifico() {
		return tieneDescuentoEspecifico;
	}



	public void setTieneDescuentoEspecifico(boolean tieneDescuentoEspecifico) {
		this.tieneDescuentoEspecifico = tieneDescuentoEspecifico;
	}



	public int getPorcentajeDsctoEspecifico() {
		return porcentajeDsctoEspecifico;
	}



	public void setPorcentajeDsctoEspecifico(int porcentajeDsctoEspecifico) {
		this.porcentajeDsctoEspecifico = porcentajeDsctoEspecifico;
	}

	public String getValorDctoEspecifico() {
		return valorDctoEspecifico;
	}



	public void setValorDctoEspecifico(String valorDctoEspecifico) {
		this.valorDctoEspecifico = valorDctoEspecifico;
	}



	public String getCargoPlanMeses() {
		return cargoPlanMeses;
	}



	public void setCargoPlanMeses(String cargoPlanMeses) {
		this.cargoPlanMeses = cargoPlanMeses;
	}



	public boolean isBuscarDescuentoEspecifico() {
		return buscarDescuentoEspecifico;
	}



	public void setBuscarDescuentoEspecifico(boolean buscarDescuentoEspecifico) {
		this.buscarDescuentoEspecifico = buscarDescuentoEspecifico;
	}



	public String getCargoPlanOferta() {
		return cargoPlanOferta;
	}



	public void setCargoPlanOferta(String cargoPlanOferta) {
		this.cargoPlanOferta = cargoPlanOferta;
	}



	public String getDescuentoPlanOferta() {
		return descuentoPlanOferta;
	}



	public void setDescuentoPlanOferta(String descuentoPlanOferta) {
		this.descuentoPlanOferta = descuentoPlanOferta;
	}



	public String getTipoError() {
		return tipoError;
	}


	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	
	
	
	
	

}
