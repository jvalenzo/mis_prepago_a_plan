package com.esa.ponline.appmobile.web.bean;

import java.io.Serializable;

public class DataEmailVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8995606364678304402L;
	private String data;
	private String nombre;
	private String tipo;
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
