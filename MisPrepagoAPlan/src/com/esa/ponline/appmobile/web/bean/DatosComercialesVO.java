package com.esa.ponline.appmobile.web.bean;

public class DatosComercialesVO {
	private String rut;
	private String segmentoCliente;
	private String grupoCliente;
	private String tipoCliente;
	private boolean esCliente;
	
	
	
	public DatosComercialesVO() {
		this.rut = "";
		this.segmentoCliente = "";
		this.grupoCliente = "";
		this.tipoCliente = "";
		this.esCliente = false;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getSegmentoCliente() {
		return segmentoCliente;
	}
	public void setSegmentoCliente(String segmentoCliente) {
		this.segmentoCliente = segmentoCliente;
	}
	public String getGrupoCliente() {
		return grupoCliente;
	}
	public void setGrupoCliente(String grupoCliente) {
		this.grupoCliente = grupoCliente;
	}
	public String getTipoCliente() {
		return tipoCliente;
	}
	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
	public boolean isEsCliente() {
		return esCliente;
	}
	public void setEsCliente(boolean esCliente) {
		this.esCliente = esCliente;
	}
	
	
	
}
