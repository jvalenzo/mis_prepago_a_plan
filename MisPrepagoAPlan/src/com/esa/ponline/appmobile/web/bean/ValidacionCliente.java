package com.esa.ponline.appmobile.web.bean;

public class ValidacionCliente {

	private String indicadorExito;
	private String descripcionValidacion;
	
	private String nombresCliente;
	private String apellidos;
	private String numeroNegocio;
	
	
	public String getIndicadorExito() {
		return indicadorExito;
	}
	public void setIndicadorExito(String indicadorExito) {
		this.indicadorExito = indicadorExito;
	}
	public String getDescripcionValidacion() {
		return descripcionValidacion;
	}
	public void setDescripcionValidacion(String descripcionValidacion) {
		this.descripcionValidacion = descripcionValidacion;
	}
	
	public String getNombresCliente() {
		return nombresCliente;
	}
	public void setNombresCliente(String nombresCliente) {
		this.nombresCliente = nombresCliente;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getNumeroNegocio() {
		return numeroNegocio;
	}
	public void setNumeroNegocio(String numeroNegocio) {
		this.numeroNegocio = numeroNegocio;
	}
	
	
	
}
