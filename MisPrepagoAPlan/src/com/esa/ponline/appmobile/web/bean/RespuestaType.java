package com.esa.ponline.appmobile.web.bean;

import java.io.Serializable;

public class RespuestaType implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String codigo;
    private String mensaje;
    private String mercado;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMercado() {
        return mercado;
    }

    public void setMercado(String mercado) {
        this.mercado = mercado;
    }
}
