package com.esa.ponline.appmobile.web.bean;
/***
 * Clase para comprobar que el flujo no haya sido vulnerado por atacantes. Emulando sus respuestas.
 * @author everis
 *
 */
public class Seguridad {
	private Boolean serie;
	private Boolean comercial;
	
	public Seguridad() {
		this.serie = false;
		this.comercial = false;
	}

	public Boolean getSerie() {
		return serie;
	}

	public void setSerie(Boolean serie) {
		this.serie = serie;
	}

	public Boolean getComercial() {
		return comercial;
	}

	public void setComercial(Boolean comercial) {
		this.comercial = comercial;
	}
	
	public Boolean checkSeguridad(){
		if(this.comercial.equals(true && this.serie.equals(true))){
			return true;
		}else{
			return false;
		}
	}
}
