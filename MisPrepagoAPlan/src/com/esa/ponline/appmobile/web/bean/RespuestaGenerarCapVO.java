package com.esa.ponline.appmobile.web.bean;

import java.io.Serializable;

public class RespuestaGenerarCapVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2160632816848445889L;
	private String ErrorCode;
	private String ErrorDescription;
	private String IdSolicitudGW;
	
	public String getErrorCode() {
		return ErrorCode;
	}
	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}
	public String getErrorDescription() {
		return ErrorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		ErrorDescription = errorDescription;
	}
	public String getIdSolicitudGW() {
		return IdSolicitudGW;
	}
	public void setIdSolicitudGW(String idSolicitudGW) {
		IdSolicitudGW = idSolicitudGW;
	}
	
	
}
