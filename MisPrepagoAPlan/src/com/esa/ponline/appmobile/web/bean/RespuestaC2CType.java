package com.esa.ponline.appmobile.web.bean;

/**
 * Clase generica para registrar respuestas estandar de servicios
 */
public class RespuestaC2CType {

    String codigo;
    String descripcion;
    String fechaOperacion;
    String idOperacion;
    String campoExtra;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(String fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }

    public String getIdOperacion() {
        return idOperacion;
    }

    public void setIdOperacion(String idOperacion) {
        this.idOperacion = idOperacion;
    }

    public String getCampoExtra() { return campoExtra; }

    public void setCampoExtra(String campoExtra) { this.campoExtra = campoExtra; }
}
