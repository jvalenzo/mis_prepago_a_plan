package com.esa.ponline.appmobile.web.bean;

public class DatosVolver {
	
	private boolean volvioPaso1;
	
	
	public boolean isVolvioPaso1() {
		return volvioPaso1;
	}

	public void setVolvioPaso1(boolean volvioPaso1) {
		this.volvioPaso1 = volvioPaso1;
	}


	DatosVolver(){
		this.volvioPaso1 = false;
	}
	
	
}
