package com.esa.ponline.appmobile.web.bean;

import java.io.Serializable;

public class ConsultaFactibilidadPortVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8546192903018843338L;
	private String idd;
	private String ani;
	private String imei;
	private int tipoServicio;
	private int tipoServicioTo;
	private String msisdn;
	public String getIdd() {
		return idd;
	}
	public void setIdd(String idd) {
		this.idd = idd;
	}
	public String getAni() {
		return ani;
	}
	public void setAni(String ani) {
		this.ani = ani;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public int getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(int tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public int getTipoServicioTo() {
		return tipoServicioTo;
	}
	public void setTipoServicioTo(int tipoServicioTo) {
		this.tipoServicioTo = tipoServicioTo;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	
	
}
