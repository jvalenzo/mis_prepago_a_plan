package com.esa.ponline.appmobile.web.bean;

import java.util.Date;

public class HorarioAtencionType {

    private Date fecha;
    private String tipofecha;
    private int horaInicio;
    private int horaFin;

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTipofecha() {
        return tipofecha;
    }

    public void setTipofecha(String tipofecha) {
        this.tipofecha = tipofecha;
    }

    public int getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(int horaInicio) {
        this.horaInicio = horaInicio;
    }

    public int getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(int horaFin) {
        this.horaFin = horaFin;
    }

    @Override
    public String toString() {
        return "HorarioAtencionType{" +
                "fecha=" + fecha +
                ", tipofecha='" + tipofecha + '\'' +
                ", horaInicio=" + horaInicio +
                ", horaFin=" + horaFin +
                '}';
    }
}
