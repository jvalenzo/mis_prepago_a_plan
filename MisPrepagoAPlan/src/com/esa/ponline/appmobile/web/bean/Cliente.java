package com.esa.ponline.appmobile.web.bean;

import java.io.Serializable;

public class Cliente implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3754731845683711593L;
	private String nombre;
	private String firstName;
	private String apellidos;
	private String lastName;
	private String rut;
	private String rutFormateado;
	private String nSerie;
	private String telefono;
	private String email;
	private String companiaOrigen;
	private String numeroNegocio;
	private String numeroPorta;
	private Integer mercadoOrigen;
	private String comunaDespacho;
	private String regionDespacho;
	private String calleDespacho;
	private String numeroDespacho;
	private String numeroCalle;
	private String direccionAdicional;
	private String imei;
	private String grupoCliente;
	private String lineasDisponibles;
	private String codigoCAP;
	private DatosVolver volver;
	private Seguridad seguridad;
	private String flujo;
	private Boolean c2cFueraHorario;
	private String paso;
	private Boolean mostrarOpcionChat;
	private Boolean finalizoFlujo;
	private Boolean esClienteEntel;
	private Boolean serieValida;
	
	private ValidacionCliente validacionCliente;
	
	public Cliente(){
		this.volver = new DatosVolver();
		this.validacionCliente = new ValidacionCliente();
		this.firstName = "";
		this.lastName = "";
		this.seguridad = new Seguridad();
		this.rut="";
		this.nSerie="";
		this.telefono="";
		this.flujo="";
		this.nombre="";
		this.apellidos="";
		this.regionDespacho="";
		this.comunaDespacho="";
		this.calleDespacho="";
		this.numeroDespacho="";
		this.numeroCalle="";
		this.direccionAdicional="";
		this.email="";
		this.rutFormateado="";
		this.numeroPorta="";
		this.mercadoOrigen=-1;
		this.c2cFueraHorario = false;
		this.mostrarOpcionChat = false;
		this.paso="";
		this.codigoCAP="";
		this.finalizoFlujo = false;
		this.esClienteEntel = false;
		this.serieValida= false;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getnSerie() {
		return nSerie;
	}
	public void setnSerie(String nSerie) {
		this.nSerie = nSerie;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCompaniaOrigen() {
		return companiaOrigen;
	}
	public void setCompaniaOrigen(String companiaOrigen) {
		this.companiaOrigen = companiaOrigen;
	}
	public String getNumeroNegocio(){
		return this.numeroNegocio;
	}
	public void setNumeroNegocio(String numeroNegocio){
		this.numeroNegocio = numeroNegocio;
	}
	public String getNumeroPorta() {
		return numeroPorta;
	}
	public void setNumeroPorta(String numeroPorta) {
		this.numeroPorta = numeroPorta;
	}
	public Integer getMercadoOrigen() {
		return mercadoOrigen;
	}
	public void setMercadoOrigen(Integer mercadoOrigen) {
		this.mercadoOrigen = mercadoOrigen;
	}
	public String getComunaDespacho() {
		return comunaDespacho;
	}
	public void setComunaDespacho(String comunaDespacho) {
		this.comunaDespacho = comunaDespacho;
	}
	public String getRegionDespacho() {
		return regionDespacho;
	}
	public void setRegionDespacho(String regionDespacho) {
		this.regionDespacho = regionDespacho;
	}
	public String getCalleDespacho() {
		return calleDespacho;
	}
	public void setCalleDespacho(String calleDespacho) {
		this.calleDespacho = calleDespacho;
	}
	public String getNumeroDespacho() {
		return numeroDespacho;
	}
	public void setNumeroDespacho(String numeroDespacho) {
		this.numeroDespacho = numeroDespacho;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getGrupoCliente() {
		return grupoCliente;
	}
	public void setGrupoCliente(String grupoCliente) {
		this.grupoCliente = grupoCliente;
	}
	public String getRutFormateado() {
		return rutFormateado;
	}
	public void setRutFormateado(String rutFormateado) {
		this.rutFormateado = rutFormateado;
	}
	public String getLineasDisponibles() {
		return lineasDisponibles;
	}
	public void setLineasDisponibles(String lineasDisponibles) {
		this.lineasDisponibles = lineasDisponibles;
	}
	public String getCodigoCAP() {
		return codigoCAP;
	}
	public void setCodigoCAP(String codigoCAP) {
		this.codigoCAP = codigoCAP;
	}
	public DatosVolver getVolver() {
		return volver;
	}
	public void setVolver(DatosVolver volver) {
		this.volver = volver;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public ValidacionCliente getValidacionCliente() {
		return validacionCliente;
	}

	public void setValidacionCliente(ValidacionCliente validacionCliente) {
		this.validacionCliente = validacionCliente;
	}

	public Seguridad getSeguridad() {
		return seguridad;
	}

	public void setSeguridad(Seguridad seguridad) {
		this.seguridad = seguridad;
	}

	public String getFlujo() {
		return flujo;
	}

	public void setFlujo(String flujo) {
		this.flujo = flujo;
	}

	public Boolean getC2cFueraHorario() {
		return c2cFueraHorario;
	}

	public void setC2cFueraHorario(Boolean c2cActivo) {
		this.c2cFueraHorario = c2cActivo;
	}
	
	public Boolean getMostrarOpcionChat() {
		return mostrarOpcionChat;
	}

	public void setMostrarOpcionChat(Boolean mostrarOpcionChat) {
		this.mostrarOpcionChat = mostrarOpcionChat;
	}
	
	public String getPaso() {
		return nombre;
	}
	public void setPaso(String paso) {
		this.paso = paso;
	}

	public String getNumeroCalle() {
		return numeroCalle;
	}

	public void setNumeroCalle(String numeroCalle) {
		this.numeroCalle = numeroCalle;
	}

	public String getDireccionAdicional() {
		return direccionAdicional;
	}

	public void setDireccionAdicional(String direccionAdicional) {
		this.direccionAdicional = direccionAdicional;
	}

	public Boolean getFinalizoFlujo() {
		return finalizoFlujo;
	}

	public void setFinalizoFlujo(Boolean finalizoFlujo) {
		this.finalizoFlujo = finalizoFlujo;
	}

	public Boolean getEsClienteEntel() {
		return esClienteEntel;
	}

	public void setEsClienteEntel(Boolean esClienteEntel) {
		this.esClienteEntel = esClienteEntel;
	}

	public Boolean getSerieValida() {
		return serieValida;
	}

	public void setSerieValida(Boolean serieValida) {
		this.serieValida = serieValida;
	}
	
	
}
