package com.esa.ponline.appmobile.web.bean;

public class ParametrosURL {
	private String idPlan;
		
	public ParametrosURL() {
		this.idPlan = "";
	}

	public String getIdPlan() {
		return idPlan;
	}

	public void setIdPlan(String idPlan) {
		this.idPlan = idPlan;
	}
	
	
}
