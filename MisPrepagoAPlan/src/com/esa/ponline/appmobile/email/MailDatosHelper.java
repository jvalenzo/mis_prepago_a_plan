package com.esa.ponline.appmobile.email;

import static com.esa.ponline.appmobile.web.actions.Constantes.DETALLE_PLAN;

import java.io.StringWriter;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.utils.Utils;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.opensymphony.xwork2.ActionContext;

import sun.net.www.content.text.plain;





public class MailDatosHelper {
	private final static Logger LOGGER = Logger.getLogger(MailDatosHelper.class);
	private static final int CargoFijo = 9990;
	public static MailType getDatosMail()throws ParseException{
		Cliente clienteSession = (Cliente) getSession().get("Cliente");
		Plan plan = (Plan) getSession().get("Plan");
		MailType mail = new MailType();
		String textoPlanConDescuento = "";
		int precioDescuento = 0;
		int precioNormal = 0;
		
		try {
			
			clienteSession.getEmail();			
			mail.setNombreCliente(clienteSession.getNombre());
			mail.setNombrePlan(plan.getNombrePlanCompleto());
			mail.setApellidoCliente(clienteSession.getApellidos());
			mail.setCalleDespacho(clienteSession.getCalleDespacho() + " " + clienteSession.getNumeroDespacho());
			mail.setComunaDespacho(clienteSession.getComunaDespacho());
			mail.setMinutosPlan(plan.getMinutosPlan());			
			mail.setPrecioConDescuento(plan.getCargoDcto());
			mail.setPrecioNormalPlan(plan.getCargoPlan());
			mail.setRegionDespacho(clienteSession.getRegionDespacho());
			mail.setRedesSocialesPlan(plan.getRrssPlan());
			mail.setNumeroPortabilidad(clienteSession.getNumeroPorta());
			mail.setDatosPlan(plan.getDatosPlan());
			mail.setSmsPlan(plan.getSmsPlan());
			mail.setEsMiPrimerPlan(plan.getEsMiPrimerPlan());
			mail.setDuracionDescuento(plan.getDuracionOferta());
			mail.setPorcentajeDescuento(plan.getPorcentajeDsctoOferta());
			mail.setValorDescuento(plan.getValorDelDescuento());
			if(plan.getTieneDescuento()){
				mail.setIsDescuento( Integer.parseInt(plan.getValorDelDescuento().replace(".", ""))>0 ? true : false);
			}
			mail.setIsDescuentoEspecifico(plan.isTieneDescuentoEspecifico());
			mail.setPorcentajeDescuentoEspecifico(plan.getPorcentajeDsctoEspecifico());
			mail.setValorDescuentoEspecifico(plan.getValorDctoEspecifico());
			
			if(Double.valueOf(plan.getCargoPlanOferta())>0){
				mail.setIsDescuentoOferta(true);
				mail.setValorDescuentoOferta(plan.getDescuentoPlanOferta());
			}

			
			precioNormal =    Integer.valueOf(plan.getCargoPlan().replace(".", ""));
			precioDescuento = Integer.valueOf(mail.getPrecioConDescuento().replace(".",""));
				
		}
		catch(Exception e) {
			LOGGER.error("Error al obtener los datos del cliente:" + clienteSession.getRut() +"para obtener los datos del EMAIL"+ " error: " + e.getMessage());
		}
		
		return mail;
	}
	/**
	 * Metodo que calcula el cargo fijo del plan menos el 10% de descuento 
	 * Si el cargo fijo es de 9990 no tiene descuento
	 *
	 * @return string con el cargo fijo con el descuento o sin dependiendo del cargo fijo
	 */
	private static String obtenerPlanConDescuento(String cargoPlan) {		
		String planConDescuento = "";
		int cargoPlanI = 0;
		cargoPlan = cargoPlan.replace(".", "");
		
		try{
			cargoPlanI = Integer.valueOf(cargoPlan);
		}catch (Exception e) {
			LOGGER.error("No se pudo convertir cargoPlan a Integer. [cargoPlan : " + cargoPlan +"]");
			return cargoPlan;
		}
		
		if(CargoFijo == cargoPlanI){
			planConDescuento = cargoPlan;
		}else{
			//Se consulta descuento para planes $9990, $19990, $21990 y $25990
			int cargoPlanConDescuento = 0;
			if(Arrays.asList(Config.getAppProperty("Planes.SinDescuento").split(",")).contains(cargoPlan)){	
				cargoPlanConDescuento = cargoPlanI;
			} else {
				cargoPlanConDescuento = (int) (cargoPlanI * 0.1) - cargoPlanI;
			    cargoPlanConDescuento = cargoPlanConDescuento * (-1);   
			}
			planConDescuento = String.valueOf(cargoPlanConDescuento);
		}
		return planConDescuento;
	}

	/**
	 * Metodo que crea el cuerpo del correo con libreria Velocity
	 *
	 * @return string con el cuerpo del correo
	 */
	public static String cuerpoEmailConfirmacion(MailType objMailType) {

		LOGGER.info(Utils.cabeceraLog() + "Ingresa a cuerpoEmailConfirmacion");
		
		VelocityEngine velMailConfirmacion = new VelocityEngine();
		Template temMailConfirmacion = null;
	    VelocityContext context = new VelocityContext();
	    StringWriter writer = new StringWriter();
	    VelocityContext velDatosMail = new VelocityContext();
	    StringWriter swSalida = new StringWriter();
		
		velMailConfirmacion.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		velMailConfirmacion.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath"); 
		velMailConfirmacion.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());	
		try {
			velMailConfirmacion.init();
		} catch (Exception e1) {
			LOGGER.error("Error en el metodo init() de Velocity");
			e1.printStackTrace();
		}

		try {
			
		    temMailConfirmacion = velMailConfirmacion.getTemplate("emailHTML.vm");
			temMailConfirmacion.merge(context, writer);
			
			LOGGER.info(Utils.cabeceraLog() + "Se crea template del archivo : emailHTML.vm ");

		} catch (ResourceNotFoundException e) {
			LOGGER.error("Error en la creacion del Template : ResourceNotFoundException "+e.getMessage());
			e.printStackTrace();
		} catch (ParseErrorException e) {
			LOGGER.error("Error en la creacion del Template : ParseErrorException "+e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			LOGGER.error("Error en la creacion del Template : Exception "+e.getMessage());
			e.printStackTrace();
		}

        //Datos Email
    	//Cabecera//
    	velDatosMail.put("nombreCliente", objMailType.getNombreCliente());    	
    	velDatosMail.put("nombrePla", objMailType.getNombrePlan());    	
    	//Resumen plan 
    	
    	velDatosMail.put("minutosPlan",    objMailType.getMinutosPlan());
    	velDatosMail.put("smsPlan",    objMailType.getSmsPlan());
    	velDatosMail.put("RRSSPlan",       objMailType.getRedesSocialesPlan());
    	velDatosMail.put("cuotaPlan",      objMailType.getPrecioNormalPlan());
    	velDatosMail.put("isDescuento", objMailType.getIsDescuento());
    	velDatosMail.put("cuotaPlanConDescuento",      objMailType.getPrecioConDescuento());
    	velDatosMail.put("porcentajeDescuento",      objMailType.getPorcentajeDescuento());
    	velDatosMail.put("duracionDescuento",      objMailType.getDuracionDescuento());
    	velDatosMail.put("valorDescuento",      objMailType.getValorDescuento());
    	velDatosMail.put("isDescuentoEspecifico", objMailType.getIsDescuentoEspecifico());
    	velDatosMail.put("valorDescuentoEspecifico",      objMailType.getValorDescuentoEspecifico());
    	velDatosMail.put("porcentajeDescuentoEspecifico",      objMailType.getPorcentajeDescuentoEspecifico());
    	//Direccion Entrega 
    	velDatosMail.put("apellidosCliente", objMailType.getApellidoCliente());
    	velDatosMail.put("calleDespacho",  objMailType.getCalleDespacho());
    	velDatosMail.put("numeroDespacho", objMailType.getNumeroDespacho());
    	velDatosMail.put("comunaDespacho", objMailType.getComunaDespacho());
    	velDatosMail.put("regionDespacho", objMailType.getRegionDespacho());
    	velDatosMail.put("textoPlanConDescuento", objMailType.getTextoPrecioConDescuento());
    	velDatosMail.put("esMiPrimerPlan", objMailType.getEsMiPrimerPlan());
    	velDatosMail.put("esPortabilidad", (null != objMailType.getNumeroPortabilidad() &&!objMailType.getNumeroPortabilidad().equals("")));
    	
    	
    	velDatosMail.put("isDescuentoOferta",objMailType.getIsDescuentoOferta());
    	velDatosMail.put("valorDescuentoOferta",objMailType.getValorDescuentoOferta());
    	
    	String nombrePlan ="";
    	if(StringUtils.isNumeric(objMailType.getDatosPlan())){
    		nombrePlan += objMailType.getNombrePlan();
    	}else{
    		nombrePlan += objMailType.getNombrePlan();
    	}
    	velDatosMail.put("nombrePlan",  nombrePlan);    	

		LOGGER.info(Utils.cabeceraLog() + "cuerpoEmailConfirmacion : Se crea Template para envio de correo" );
		
		try {
			temMailConfirmacion.merge(velDatosMail, swSalida);
		} catch (ResourceNotFoundException e) {
			LOGGER.error("Error en el merge del correo : ResourceNotFoundException"+ e.getMessage());
			e.printStackTrace();
		} catch (ParseErrorException e) {
			LOGGER.error("Error en el merge del correo : ParseErrorException"+ e.getMessage());
			e.printStackTrace();
		} catch (MethodInvocationException e) {
			LOGGER.error("Error en el merge del correo : MethodInvocationException"+ e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			LOGGER.error("Error en el merge del correo : Exception"+ e.getMessage());
			LOGGER.error("Retornamos cuerpo vac�o");
		}
	
		return swSalida.toString();

	}
	private static Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}

}
