package com.esa.ponline.appmobile.email;

import java.io.Serializable;

/*
 * Type Para asignar los parametros que se deseen colocar en el correo
 */
public class MailType implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5554027721992358074L;
	//Resumen Plan
	private String nombrePlan;
	private String minutosPlan;
	private String redesSocialesPlan;
	private String precioNormalPlan;
	private Boolean isDescuento;
	private Boolean isDescuentoEspecifico;
	private String precioConDescuento;
	private String valorDescuento;
	private String valorDescuentoEspecifico;
	private String textoPrecioConDescuento;
	private String DatosPlan;
	//Direccion de Entrega
	private String nombreCliente;
	private String apellidoCliente;
	private String calleDespacho;
	private String numeroDespacho;
	private String comunaDespacho;
	private String regionDespacho;
	private String numeroPortabilidad;
	private String smsPlan;
	private Boolean esMiPrimerPlan;
	private String duracionDescuento;
	private int porcentajeDescuento;
	private int porcentajeDescuentoEspecifico;
	
	private Boolean isDescuentoOferta;
	private String valorDescuentoOferta;
	
	public String getMinutosPlan() {
		return minutosPlan;
	}
	public void setMinutosPlan(String minutosPlan) {
		this.minutosPlan = minutosPlan;
	}
	public String getRedesSocialesPlan() {
		return redesSocialesPlan;
	}
	public void setRedesSocialesPlan(String redesSocialesPlan) {
		this.redesSocialesPlan = redesSocialesPlan;
	}
	public String getPrecioNormalPlan() {
		return precioNormalPlan;
	}
	public void setPrecioNormalPlan(String precioNormalPlan) {
		this.precioNormalPlan = precioNormalPlan;
	}
	public String getPrecioConDescuento() {
		return precioConDescuento;
	}
	public void setPrecioConDescuento(String precioConDescuento) {
		this.precioConDescuento = precioConDescuento;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public String getApellidoCliente() {
		return apellidoCliente;
	}
	public void setApellidoCliente(String apellidoCliente) {
		this.apellidoCliente = apellidoCliente;
	}
	public String getCalleDespacho() {
		return calleDespacho;
	}
	public void setCalleDespacho(String calleDespacho) {
		this.calleDespacho = calleDespacho;
	}
	public String getNumeroDespacho() {
		return numeroDespacho;
	}
	public void setNumeroDespacho(String numeroDespacho) {
		this.numeroDespacho = numeroDespacho;
	}
	public String getComunaDespacho() {
		return comunaDespacho;
	}
	public void setComunaDespacho(String comunaDespacho) {
		this.comunaDespacho = comunaDespacho;
	}
	public String getRegionDespacho() {
		return regionDespacho;
	}
	public void setRegionDespacho(String regionDespacho) {
		this.regionDespacho = regionDespacho;
	}
	public String getTextoPrecioConDescuento() {
		return textoPrecioConDescuento;
	}
	public void setTextoPrecioConDescuento(String textoPrecioConDescuento) {
		this.textoPrecioConDescuento = textoPrecioConDescuento;
	}
	public String getNumeroPortabilidad() {
		return numeroPortabilidad;
	}
	public void setNumeroPortabilidad(String numeroPortabilidad) {
		this.numeroPortabilidad = numeroPortabilidad;
	}
	public String getDatosPlan() {
		return DatosPlan;
	}
	public void setDatosPlan(String datosPlan) {
		DatosPlan = datosPlan;
	}
	public String getSmsPlan() {
		return smsPlan;
	}
	public void setSmsPlan(String smsPlan) {
		this.smsPlan = smsPlan;
	}
	public Boolean getEsMiPrimerPlan() {
		return esMiPrimerPlan;
	}
	public void setEsMiPrimerPlan(Boolean esMiPrimerPlan) {
		this.esMiPrimerPlan = esMiPrimerPlan;
	}
	public String getDuracionDescuento() {
		return duracionDescuento;
	}
	public void setDuracionDescuento(String duracionDescuento) {
		this.duracionDescuento = duracionDescuento;
	}
	public int getPorcentajeDescuento() {
		return porcentajeDescuento;
	}
	public void setPorcentajeDescuento(int porcentajeDescuento) {
		this.porcentajeDescuento = porcentajeDescuento;
	}
	public String getValorDescuento() {
		return valorDescuento;
	}
	public void setValorDescuento(String valorDescuento) {
		this.valorDescuento = valorDescuento;
	}
	public Boolean getIsDescuento() {
		return isDescuento;
	}
	public void setIsDescuento(Boolean isDescuento) {
		this.isDescuento = isDescuento;
	}
	public Boolean getIsDescuentoEspecifico() {
		return isDescuentoEspecifico;
	}
	public void setIsDescuentoEspecifico(Boolean isDescuentoEspecifico) {
		this.isDescuentoEspecifico = isDescuentoEspecifico;
	}
	public String getValorDescuentoEspecifico() {
		return valorDescuentoEspecifico;
	}
	public void setValorDescuentoEspecifico(String valorDescuentoEspecifico) {
		this.valorDescuentoEspecifico = valorDescuentoEspecifico;
	}
	public int getPorcentajeDescuentoEspecifico() {
		return porcentajeDescuentoEspecifico;
	}
	public void setPorcentajeDescuentoEspecifico(int porcentajeDescuentoEspecifico) {
		this.porcentajeDescuentoEspecifico = porcentajeDescuentoEspecifico;
	}
	public Boolean getIsDescuentoOferta() {
		return isDescuentoOferta;
	}
	public void setIsDescuentoOferta(Boolean isDescuentoOferta) {
		this.isDescuentoOferta = isDescuentoOferta;
	}
	public String getValorDescuentoOferta() {
		return valorDescuentoOferta;
	}
	public void setValorDescuentoOferta(String valorDescuentoOferta) {
		this.valorDescuentoOferta = valorDescuentoOferta;
	}
	public String getNombrePlan() {
		return nombrePlan;
	}
	public void setNombrePlan(String nombrePlan) {
		this.nombrePlan = nombrePlan;
	}
	
	
	
	
	
	
	
	
	
}
