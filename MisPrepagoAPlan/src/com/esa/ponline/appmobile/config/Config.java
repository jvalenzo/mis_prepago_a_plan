package com.esa.ponline.appmobile.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.esa.ponline.appmobile.utils.Utils;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class Config implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2045920596434424294L;
	private static final Logger LOGGER                             = Logger.getLogger(Config.class);
	private static final String CONFIG_PATH_COSERVICES            	= "./aplsEPCS/MisPrepagoAPlan/config/" + "MisServices.properties";
	private static final String CONFIG_PATH_CO          			= "./aplsEPCS/MisPrepagoAPlan/config/" + "MisPrepagoAPlan.properties";
	private static final String CONFIG_PATH_CONTRATACION_TERMINOS	= "./aplsEPCS/MisPrepagoAPlan/config/" + "MisPrepagoAPlan_Terminos.properties";
	private static Map<String, String> configCOServices;
	public static  Map<String, String> configApp;
	private static Map<String, String> configContratacionOnline_Terminos;
    private static final String KEY                                = "Key: ";
    
	public Config(){
		//dejado vacio intencionalmente
	}
	static{
		setConfigCOServices(getConfigProperties(CONFIG_PATH_COSERVICES));
		setConfigApp(getConfigProperties(CONFIG_PATH_CO));
		setConfigContratacionOnline_Terminos(getConfigProperties(CONFIG_PATH_CONTRATACION_TERMINOS));
	}
	public static String getCOServices(String key) {
		String result = configCOServices.get(key);
		if (result==null){
			LOGGER.error("Key:"+key+"NO_ENCONTRADA");
		}
		return result;
	}
	static HashMap<String,String> getConfigProperties(String configPath) {
		
        HashMap<String,String> hmProperties = new HashMap<String,String>();
        InputStream inStream                = null;
        InputStreamReader isr               = null;

		try {
            inStream              = new FileInputStream( configPath );
            isr                   = new InputStreamReader(inStream, "UTF-8");
            Properties properties = new Properties();
            
            properties.load(isr);
            hmProperties = new HashMap( properties );
			
			LOGGER.info(Utils.cabeceraLog() + "Archivo de configuracion flags de funcionalidaes cargado correctamente desde: " + configPath );

		} catch (Exception e) {
			LOGGER.error("Error al cargar datos de configuracion de funcionalidades", e);
		} finally {
			try {
				isr.close();
			} catch (NullPointerException e) {
				LOGGER.error("isr es null", e);
			} catch (IOException e) {
				LOGGER.error("Error al cerrar isr", e);
			} try {
				inStream.close();
			} catch (NullPointerException e) {
				LOGGER.error("inStream es null", e);
			} catch (IOException e) {
				LOGGER.error("Error al cerrar inStream", e);
			}
		}
		
		return hmProperties;
	}
	
	public static String getAppProperty(String key) {
		String result = configApp.get(key);
		
		LOGGER.debug("## key: "+key);
		LOGGER.debug("## result: "+result);
		
		if (result==null){
			LOGGER.error(KEY + key + " no encontrada.");
		}
		return result;
	}

	public static Map<String, String> getConfigCOServices() {
		return configCOServices;
	}
	public static void setConfigCOServices(Map<String, String> configCOServices) {
		Config.configCOServices = configCOServices;
	}
	
	public static Map<String, String> getConfigApp() {
		return configApp;
	}

	public static void setConfigApp(Map<String, String> configApp) {
		Config.configApp = configApp;
	}

	public static String getContratacionOnline_TerminosProperty(String key) 
	{
		String result = configContratacionOnline_Terminos.get(key);
		
		if (result==null) 
		{
			LOGGER.error(KEY + key + " no encontrada.");
		}
		
		return result;
	}
	
	public static Map<String, String> getConfigContratacionOnline_Terminos()
	{
		return configContratacionOnline_Terminos;
	}

	public static void setConfigContratacionOnline_Terminos(Map<String, String> configContratacionOnline_Terminos)
	{
		Config.configContratacionOnline_Terminos = configContratacionOnline_Terminos;
	}

	public static boolean checkCodeService(String key, String code) {
        return Arrays.asList(configCOServices.get(key).split(",")).contains(code);
    }
}
