var verNombre=false;
var verApellidos = false;
var verNumPortar=false;
var verCompania=false;
var verMercado=false;

$( document ).ready(function() {
	
	numeroContactoCliente = $("#telefonoContactoGTM").val();
	
	//$("#inbound").load("pages/InboundMovil.html");
	$('.jqClean').blur(function(){
	    this.value = (this.value + '').replace(/[\<\%\/\(\)\,\>\"\#\$\!\¡\?\¿\\\:\;\]\[\{\}\*\+\&\°\|\~\=\¬\´]/g, '');
	});
	$('.jqClean').keyup(function(){
	    this.value = (this.value + '').replace(/[\<\%\/\(\)\,\>\"\#\$\!\¡\?\¿\\\:\;\]\[\{\}\*\+\&\°\|\~\=\¬\´]/g, '');
	});
	
	$('.jqBtnDescuentoPortabilidad').click(function(){
		$('.jqContainer').toggleClass("ocultar");
		$('.jqBtnDescuentoPortabilidadMensaje').removeClass("ocultar");
		$('.jqBtnDescuentoPortabilidadMensaje').addClass("fadeInDown");
	});
	
	if($("#nombreCliente").val() != ''){
		verNombre=true;
	}
	
	if($("#apellidosCliente").val() != ''){
		verApellidos=true;
	}

	$("#nombreCliente").blur(function() {
    	var element = $(this).val().trim();
    	if(element != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input normal");
    		verNombre=true;
    		//habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input normal");
    		 $(this).parent().removeClass("iscorrect");
    		 verNombre=false;
    	}
    });
	
	$("#apellidosCliente").blur(function() {
    	var element = $(this).val().trim();
    	if(element != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input normal");
    		verApellidos=true;
    		//habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input normal");
    		 $(this).parent().removeClass("iscorrect");
    		 verApellidos=false;
    	}
    });
	
	$("#numeroPortar").keydown(function(event) {
		   if(event.shiftKey)
		   {
		        event.preventDefault();
		   }

		   if (event.keyCode == 46 || event.keyCode == 8)    {
		   }
		   else {
		        if (event.keyCode < 95) {
		        	//Se acepta tab (KeyCode = 9), control-c y control-v (86 y 67 respectivamente)
		          if ( (event.keyCode < 48 || event.keyCode > 57) && ( event.keyCode != 9 ) && ( event.keyCode != 67 ) && ( event.keyCode != 86 ) ){
		                event.preventDefault();
		          }
		        } 
		        else {
		              if ( (event.keyCode < 96 || event.keyCode > 105) && ( event.keyCode != 9 ) && ( event.keyCode != 67 ) && ( event.keyCode != 86 )) {
		                  event.preventDefault();
		              }
		        }
		      }
		   });
	
	$("#numeroPortar").blur(function() {
    	var element = $(this).val().trim();
    	if(element != '' && element.length == 9 && element.charAt()>=9){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input normal");
    		verNumPortar=true;
    		//habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input normal");
    		$(this).parent().removeClass("iscorrect");
    		verNumPortar=false; 
    	}
    });
	
	$("#nombreCliente").blur(function() {
    	var element = $(this).val().trim();
    	if(element != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input normal");
    		//habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input normal");
    		$(this).parent().removeClass("iscorrect"); 
    	}
    });
	
	$("#apellidoCliente").blur(function() {
    	var element = $(this).val().trim();
    	if(element != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input normal");
    		//habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input normal");
    		$(this).parent().removeClass("iscorrect"); 
    	}
    });
	
	
	//Se omite manejo visual de manejo de origen de compañia
	/* $("#companiaMovil").change(function() {
		var comp = $(this).val();
    	if(comp != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input normal");
    		verCompania=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input normal");
    		$(this).parent().removeClass("iscorrect");
    		verCompania=false; 
    	}
    }); */
	
/*	$("#test1").click(function(e){
		$("#test1").parents('.form-input').addClass("iscorrect");
		$("#test1").parents('.form-input').removeClass("error-input normal");
		verMercado=true;
		//habilitaBoton()
	});
	if($('#portabilidadMercadoOrigenGTM').val()=="PREPAGO"){
		$('.jqPrepago').click();
		$("#test1").parents('.form-input').addClass("iscorrect");
		$("#test1").parents('.form-input').removeClass("error-input normal");
		verMercado=true;
		//habilitaBoton()
	}
	
	$("#test2").click(function(e){
		$("#test1").parents('.form-input').addClass("iscorrect");
		$("#test1").parents('.form-input').removeClass("error-input normal");
		verMercado=true;
		//habilitaBoton()
	});
	if($('#portabilidadMercadoOrigenGTM').val()=="PLAN"){
		$('.jqPlan').click();
		$("#test1").parents('.form-input').addClass("iscorrect");
		$("#test1").parents('.form-input').removeClass("error-input normal");
		verMercado=true;
		//habilitaBoton()
	}
	*/
	$("#btnContinuar").click(function(e){
		toggleButtonLoader('btnContinuar','id');
    	if(verificaForm()){
    		$("#formPaso2A").submit();
    	}else{
    		$('.jqInputForm').trigger('blur');
    		toggleButtonLoader('btnContinuar','id');
    	}
    });
	
	
	
});

function habilitaBoton(){
	//Se omite manejo visual de manejo de origen de compañia
	//if(verNombre && verApellidos && verNumPortar&&verCompania&&verMercado){
	if(verNombre && verApellidos && verNumPortar){
		$("#btnContinuar").removeClass("disabled");		
	}
}

function verificaForm(){
	try {
		/*if(!verMercado){
    		$("#test1").parents('.form-input').addClass("error-input normal");
    		$("#test1").parents('.form-input').removeClass("iscorrect");
    		throw BreakException;
    	}*/
		
        $(".jqInputForm").each(function(index) {
            if(!($(this).closest(".form-input").hasClass("iscorrect")) || $(this).val().length == 0){
                throw BreakException;
            }
        });
        formValid=true;
    } catch (e) {
    	formValid=false;
    
    }
    return formValid;
}