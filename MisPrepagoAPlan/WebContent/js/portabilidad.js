$(window).on( "load", function() {
	setTimeout(function(){ 
		$("#volver").val(true);
	}, 1000);
	
	$(".jqInputForm").each(function(index) {
        if($(this).val()!='' && !$(this).is(":focus")){
        	$(this).trigger('blur');
        }
    });
});
$( document ).ready(function() {
	history.pushState(null, null, location.href); 
		history.back(); 
	history.forward(); 
	window.onpopstate = function () { 
			history.go(1);
		if($("#volver").val()==="true"){
				$("#volver").val(false);
				if ($(".jqVolverSubmit").length){
			        $(".volverSubmitForm").submit();
			    }else{
			    	window.location.href = "https://entel.cl/planes/";
			    }
		}
	};
	
	$('.jqVolverPaso1').on("click",function(){
		toggleButtonLoader('jqVolverPaso1','class');
		$('#volverPaso1').submit();
	});
	$('.jqVolverPaso2').on("click",function(){
		toggleButtonLoader('jqVolverPaso2','class');
		$('#volverPaso2').submit();
	});
	$('.jqVolverPaso3').on("click",function(){
		toggleButtonLoader('jqVolverPaso3','class');
		$('#volverPaso3').submit();
	});
	$('.jqVolverPaso4').on("click",function(){
		toggleButtonLoader('jqVolverPaso4','class');
		$('#volverPaso4').submit();
	});
	
});
function pushDatalayer(){
	var paso=(parseInt($('#nPasoGTM').val()) - 1);
	if(paso=="0"){
		dataLayer.push({		
			"mercado": "personas"
			,"segmento": "suscripcion"
			,"page_owner": "publico"
			,"product":"plan"
			,"form_type":"flujo digital"
			,"journey_name":"portabilidad"
			,"journey_type":"solicitud"
			
		    ,"form_name" : $('#nombrePasoGTM').val()
		    ,"form_step_name" : "paso "+paso
		    
		    ,"origen" : $('#origenGTM').val().toLowerCase().trim()
		    ,"eventPlan" : $('#planGTM').val().toLowerCase().trim()
		    ,"price" : $('#precioGTM').val().toLowerCase().replace(".", "").trim()
		    ,"rut" : btoa($('#rutGTM').val().toLowerCase().trim())						
			,"email" : btoa($('#emailGTM').val().toLowerCase().trim())
			,"region" : $('#regionGTM').val().toLowerCase().trim()
			,"comuna" : $('#comunaGTM').val().toLowerCase().trim()
			,"direccion" : btoa(($('#calleGTM').val() + " " + $('#numMunicipalGTM').val()).trim())
			,"nombre" : btoa(($('#nombreGTM').val().toLowerCase() + " " + $('#apellidosGTM').val().toLowerCase()).trim())
			,"telefono_cliente" : btoa($('#telefonoContactoGTM').val().toLowerCase().trim())
			,"telefono_portar" : btoa($('#numeroPortaGTM').val().toLowerCase().trim())
			,"variant2" : $('#portabilidadMercadoOrigenGTM').val().toLowerCase().trim()
		});
	}else{
		dataLayer.push({		
			"event" : "form_steps",
			"mercado": "personas"
			,"segmento": "suscripcion"
			,"page_owner": "publico"
			,"product":"plan"
			,"form_type":"flujo digital"
			,"journey_name":"portabilidad"
			,"journey_type":"solicitud"
			
		    ,"form_name" : $('#nombrePasoGTM').val()
		    ,"form_step_name" : "paso "+paso
		    
		    ,"origen" : $('#origenGTM').val().toLowerCase().trim()
		    ,"eventPlan" : $('#planGTM').val().toLowerCase().trim()
		    ,"price" : $('#precioGTM').val().toLowerCase().replace(".", "").trim()
		    ,"rut" : btoa($('#rutGTM').val().toLowerCase().trim())						
			,"email" : btoa($('#emailGTM').val().toLowerCase().trim())
			,"region" : $('#regionGTM').val().toLowerCase().trim()
			,"comuna" : $('#comunaGTM').val().toLowerCase().trim()
			,"direccion" : btoa(($('#calleGTM').val() + " " + $('#numMunicipalGTM').val()).trim())
			,"nombre" : btoa(($('#nombreGTM').val().toLowerCase() + " " + $('#apellidosGTM').val().toLowerCase()).trim())
			,"telefono_cliente" : btoa($('#telefonoContactoGTM').val().toLowerCase().trim())
			,"telefono_portar" : btoa($('#numeroPortaGTM').val().toLowerCase().trim())
			,"variant2" : $('#portabilidadMercadoOrigenGTM').val().toLowerCase().trim()
		});
	}
}

function pushDatalayerInteraccion(event,variant){
	dataLayer.push({
        "event" : "GARDevent",
        "eventCategory": "interacciones flujo digital",
        "segmento": "suscripcion",
        "page_owner": "publico",
        "product":"plan",
        "mercado": "personas",
        "form_type":"flujo digital",
            "journey_name": "portabilidad",
        "journey_type":"solicitud",
                "eventLabel" : event,
                "variant" : variant,
                "variant2" : sessionStorage.getItem("inactividadC2CMostradoGTM"),
            "origen": $('#origenGTM').val().toLowerCase().trim(),
            "eventPlan": $('#planGTM').val().toLowerCase().trim(),
            "rut": btoa($('#rutGTM').val().toLowerCase().trim()),
            "email":btoa($('#emailGTM').val().toLowerCase().trim()),
            "form_name": $('#nombrePasoGTM').val(),
            "form_step_name": "paso "+$('#nPasoGTM').val()
	});
}