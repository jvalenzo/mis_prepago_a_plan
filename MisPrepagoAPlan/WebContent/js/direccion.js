var callesPorComuna = [];
var callesPorComunaConID = [];
var numMunicipalesByCalle = [];
var calleSeleccionada = "";
var numMunicipalesByCalleConID = [];
var numMunicipalesByCalleInfo = [];

$(document).ready(function(){
	
	$('.jqClean').blur(function(){
	    this.value = (this.value + '').replace(/[\<\%\/\(\)\,\>\"\#\$\!\¡\?\¿\\\:\;\]\[\{\}\*\+\&\°\|\~\=\¬\´]/g, '');
	});
	
	$('.jqCleanUp').keyup(function(){
		this.value = (this.value + '').replace(/[\<\%\/\(\)\,\>\"\#\$\!\¡\?\¿\\\:\;\]\[\{\}\*\+\&\°\|\~\=\¬\´]/g, '');
	});

	$('.jqCleanWarning').focus(function(){
		var div=this;
	    $(div).closest('div').removeClass('error-input');
	});
	
	$('.jqMayuscula').keyup(function(){
		this.value=this.value.toUpperCase();
	});
	
    cargarRegiones();
    

    //Evento que captura el ingreso de un caracter en el input del numero municipal de una calle
    $("#numCalle").keyup(function(){
        setTimeout(asignarLista, 0);
     });

    $("#localidad").keyup(function () {
        $("#localidad").autocomplete({
            source: localidades,
            open: function( event, ui ) {
                if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
                    $('.ui-autocomplete').off('mouseenter');
                }
            }
        });
        $('#buttonDireccion').attr("disabled", !($('#localidad').val().length >= 1));
    });
    
    $("#selRegion").change(function() {
    	$("#selComuna").val("");
    	$("#calle").val("");
    	$("#calle").attr("disabled",true);
    	$('#calle').closest('div').removeClass('iscorrect');
    	$('#calle').closest('div').removeClass('error-input');
    	
    	$("#numCalle").val("");
    	$("#numCalle").attr("disabled",true);
    	$('#numCalle').closest('div').removeClass('iscorrect');
    	$('#numCalle').closest('div').removeClass('error-input');
    	
    	$("#depto").val("");
    	cargarComunas();
    	cargaEnvio();
    	
    	desactivarBoton();
    	
    });
    
    $("#selComuna").change(function() {
    	$("#calle").val("");
    	$("#depto").val("");
    	$('#calle').closest('div').removeClass('iscorrect');
    	
    	$("#numCalle").val("");
    	$("#numCalle").attr("disabled",true);
    	$('#numCalle').closest('div').removeClass('iscorrect');
    	$('#numCalle').closest('div').removeClass('error-input');
    	
    	var com = $('#selComuna option:selected').text();
    	var reg = $('#selRegion option:selected').text();
		ajaxmsg(com, reg);
    	if(com != ''){
    		$(this).parent().removeClass("error-input normal");
    		verComuna=true;
    		//habilitaBoton();
    	}else{
    		$(this).parent().addClass("error-input normal");
    		$(this).parent().removeClass("iscorrect");
    		verComuna=false; 
    	}
    	
    	cargarCalles();
    	cargaEnvio();
    	desactivarBoton();
    });
    
    $("#calle").focusout(function() {
    	$("#numCalle").val("");
    	$("#depto").val("");

    	$('#numCalle').closest('div').removeClass('iscorrect');
    	$('#numCalle').closest('div').removeClass('error-input');
    	cargarNumeros();
    	cargaEnvio();
    	desactivarBoton();
    });
    
    $('#calle').keyup(function(){
    	$('#numCalle').closest('div').removeClass('iscorrect');
    	$('#numCalle').closest('div').removeClass('error-input');
    	$('#numCalle').attr('disabled', false);
	});
    
    $("#numCalle").blur(function() {
    	$("#depto").val("");
    	
    	//Seleccionamos la direccion adicional que existe en la session
    	if($('#deptoVal').val()!=''){
        	$('#depto').val($('#deptoVal').val().toUpperCase());
			$('#depto').trigger("blur");
		}
		
    	cargaEnvio();
    	if(numMunicipalesByCalle.indexOf($("#numCalle").val()) >= 0){
    		$("#numCalle").parent().addClass("iscorrect");
    		$("#numCalle").parent().removeClass("error-input normal");
    		$("#depto").attr('disabled', false);
    		activarBoton();
    	}else{
    		desactivarBoton();
    		$('#numCalle').closest('div').addClass('error-input');
    	}
    	
    });
    
    $("#depto").change(function() {
    	cargaEnvio();
    });
    

});

//funcion se ejuta al momento de ingresar un neuvo caracter en el input direccion
function ingesoCaracterEnCalle() {
    if($('#calle').val().length >= 1){
        //Se habilidata el campo
        $('#numCalle').attr('disabled', false);
    }
}

function cargarRegiones(){

	 $.ajax({
         type: "POST",
         url: "AjaxGetRegion.action",
         cache: false,
         contentType : "application/json",
         dataType : "json",
         beforeSend: function() {             
             $('#alertaDireccion').hide();
             $('#alertaNum').hide();
         },
         complete: function() {
             $('#selRegion').prop('disabled', false);
             $('#loaderSerieRegion').hide();
         },
         success: function (res) {
				var select = $('#selRegion');
				var options = $('#selRegion > options');
				$('option', select).remove();
				$('#selRegion').append('<option value="" disabled selected>Seleccione</option>');
				var obj = jQuery.parseJSON(res.jsonRegiones);
				for (var i = 0; i < obj.length; i++) {
					$('#selRegion').append(new Option(obj[i].nombreRegion, obj[i].id));
				}
				
				//Seleccionamos la region que existe en la session
				if($('#selRegionVal').val()!=''){
					var selectVal=$('#selRegionVal').val().toUpperCase();
					$('#selRegion option').filter(function() { 
					    return ($(this).text() == selectVal);
					}).prop('selected', true); 
					$('#selRegion').trigger("change");
				}
			},
		error: function(jqXHR, textStatus){+
			$("#formErrorDireccion").submit();
		}
	 });      
}

function cargarComunas(){
	var objetoDireccion = {idPais:56, idRegion:"", nombreRegion:""};
    var region = $('#selRegion').val();
    var nombreRegion = $('#selRegion option:selected').text();
    
    objetoDireccion.idRegion = region;
    objetoDireccion.nombreRegion = nombreRegion;
    if(objetoDireccion.idRegion!=null){
		 $.ajax({
	         type: "GET",
	         url: "AjaxGetComuna.action",
	         cache: false,
	         data : "idRegion="+objetoDireccion.idRegion,
	         contentType : "application/json",
	         dataType : "json",
	         beforeSend: function() {
	                $('#selComuna').prop('disabled', true);
	                $('#loaderSerieComuna').show();
	                $('#alertaDireccion').hide();
	                $('#alertaNum').hide();
	            },
	            complete: function() {
	                $('#selComuna').prop('disabled', false);
	                $('#loaderSerieComuna').hide();
	            },
	         success: function (res) {
			     var select = $('#selComuna');
				 var options = $('#selComuna > options');
				 	$('option', select).remove();
				 	$('#selComuna').append('<option value="" disabled selected>Seleccione</option>');
					var obj = jQuery.parseJSON(res.jsonComunas);
			        for (var i = 0; i < obj.length; i++) {
						$('#selComuna').append(new Option(obj[i].nombreComuna, obj[i].id));
			        }
			        
			      //Seleccionamos la Comuna que existe en la session
					if($('#selComunaVal').val()!=''){
						var selectVal=$('#selComunaVal').val().toUpperCase();
						$('#selComuna option').filter(function() { 
						    return ($(this).text() == selectVal);
						}).prop('selected', true); 
						$('#selComuna').trigger("change");
					}
	         },
	 		error: function(jqXHR, textStatus){+
				$("#formErrorDireccion").submit();
			}
	    });   
    }
	
}

function cargarCalles(){
	var objetoDireccion = {idPais:56, idRegion:"", nombreRegion:""};
    var region = $('#selRegion').val();
    var nombreRegion = $('#selRegion option:selected').text();
    var comuna = $('#selComuna').val();
    var nombreComuna = $('#selComuna option:selected').text();
    
    objetoDireccion.idRegion = region;
    objetoDireccion.nombreRegion = nombreRegion;
    objetoDireccion.idComuna = comuna;
    objetoDireccion.nombreComuna = nombreComuna
    if(objetoDireccion.idRegion!=null && objetoDireccion.idComuna!=null){
		 $.ajax({
	         type: "GET",
	         url: "AjaxGetCalle.action",
	         cache: false,
	         data : {idRegion:objetoDireccion.idRegion, idComuna:objetoDireccion.idComuna},
	         contentType : "application/json",
	         dataType : "json",
	         beforeSend: function() {
                 $('#calle').attr('disabled', true);
                 $('#loaderSerieDireccion').show();
                 $('#alertaDireccion').hide();
                 $('#alertaNum').hide();
             },
             complete: function() {
                 $('#calle').attr('disabled', false);
                 $('#loaderSerieDireccion').hide();
             },
	         success: function (res) {
	        	 callesPorComuna = [];
	             callesPorComunaConID = [];
	             
				 var obj = jQuery.parseJSON(res.jsonCalles);
				 if(obj[0].resultado.codigo == 1){
			        for (var i = 0; i < obj.length; i++) {
			        	 callesPorComuna.push(obj[i].nombreCalle);
		                 callesPorComunaConID.push(obj[i].nombreCalle, obj[i].id);
			        }
		            $("#calle").autocomplete({
		            	maxResults: 7,
		            	source: function(request, response) {
		                    var results = $.ui.autocomplete.filter(callesPorComuna, request.term);
		                    
		                    response(results.slice(0, this.options.maxResults));
		                },
		            	open: function (event, ui) {
                            if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
                                $('.ui-autocomplete').off('mouseenter');
                            }
                        }
		            });
		            
		            if($('#calleVal').val()!=''){
		            	$('#calle').val($('#calleVal').val().toUpperCase());
						$('#calle').trigger("blur");
					}
		            
		            
				 }else{
					$("#calle").val("NO EXISTEN REGISTROS PARA ESTA COMUNA");
					$("#calle" ).prop( "disabled", true );
					$("#numCalle" ).prop( "disabled", true );
					desactivarBoton();
				 }
	         },
	 		error: function(jqXHR, textStatus){+
				$("#formErrorDireccion").submit();
			}
	    });  
    }
	
}

function cargarNumeros(){
	var objetoDireccion = {idPais:56, idRegion:"", nombreRegion:"",idComuna:"",nombreComuna:"", idCalle:"", nombreCalle:"",numeroCalle:""};
    var region = $('#selRegion').val();
    var nombreRegion = $('#selRegion option:selected').text();
    var comuna = $('#selComuna').val();
    var nombreComuna = $('#selComuna option:selected').text();
    var nombreCalle = $('#calle').val();
    var idx = callesPorComunaConID.indexOf(nombreCalle);
    var calle = callesPorComunaConID[idx + 1];
    objetoDireccion.idRegion = region;
    objetoDireccion.nombreRegion = nombreRegion;
    objetoDireccion.idComuna = comuna;
    objetoDireccion.nombreComuna = nombreComuna
    objetoDireccion.idCalle = calle;
    objetoDireccion.nombreCcalle = nombreCalle;
    if($.isNumeric( objetoDireccion.idCalle )){
		 $.ajax({
	         type: "GET",
	         url: "AjaxGetNumero.action",
	         cache: false,
	         data : {idCalle:objetoDireccion.idCalle},
	         contentType : "application/json",
	         dataType : "json",
	         beforeSend: function() {
                 $('#numCalle').attr('disabled', true);
                 $('#loaderSerieNumero').show();
                 $('#alertaNum').hide();
             },
             complete: function() {
                 $('#numCalle').attr('disabled', false);
                 $('#loaderSerieNumero').hide();
                 $('#numCalle').focus();
             },
	         success: function (res) {
	        	 $("#calle").parent().addClass("iscorrect");
	        	 $("#calle").parent().removeClass("error-input normal");
	        	 numMunicipalesByCalle=[];
	        	 numMunicipalesByCalleConID=[];
	        	 numMunicipalesByCalleInfo=[];
				
				 var obj = jQuery.parseJSON(res.jsonNumeros);
				 if(obj[0].resultado.codigo == 1){
					 $("#numCalle" ).prop( "disabled", false );
					 desactivarBoton();
			        for (var i = 0; i < obj.length; i++) {
			        	var numMunicipalConCPNyCAN=obj[i].numeroMunicipal;
			        	if(null!=obj[i].can && obj[i].can!=""){
			        		numMunicipalConCPNyCAN=obj[i].can+numMunicipalConCPNyCAN;
			        	}
			        	if(null!=obj[i].cpn && obj[i].cpn!=""){
			        		numMunicipalConCPNyCAN=numMunicipalConCPNyCAN+"-"+obj[i].cpn;
			        	}
			        	 numMunicipalesByCalle.push(numMunicipalConCPNyCAN);
		                 numMunicipalesByCalleConID.push(obj[i].numeroMunicipal, obj[i].id);
		                 numMunicipalesByCalleInfo.push(obj[i].numeroMunicipal, obj[i].id, obj[i].numeroCompleto, obj[i].cpn, obj[i].can, obj[i].idTipoEdificacion);
			        }
		            $("#numCalle").autocomplete({
		            	maxResults: 7,
		            	source: function(request, response) {
		                    var results = $.ui.autocomplete.filter(numMunicipalesByCalle, request.term);
		                    
		                    response(results.slice(0, this.options.maxResults));
		                },
		            	open: function( event, ui ) {
		                    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
		                        $('.ui-autocomplete').off('mouseenter');
		                    }
		                }
		            });
		            
		            if($('#numCalleVal').val()!=''){
		            	$('#numCalle').val($('#numCalleVal').val().toUpperCase());
						$('#numCalle').trigger("blur");
					}
		            
				 }else{
						$("#numCalle" ).prop( "disabled", true );
					 }
	         }
             ,
 	 		error: function(jqXHR, textStatus){+
 				$("#formErrorDireccion").submit();
 			}
	    });  
    }else{
    	$('#numCalle').attr('disabled', true);
    	$('#calle').closest('div').addClass('error-input');
    }
}

function cargaEnvio(){
    var region = $('#selRegion').val();
    var nombreRegion = $('#selRegion option:selected').text();
    var comuna = $('#selComuna').val();
    var nombreComuna = $('#selComuna option:selected').text();
    var nombreCalle = $('#calle').val();
    var numCalle=$('#numCalle').val();
    var depto=$('#depto').val();
    
    
    $("#selComunaH").val(nombreComuna);
    $("#selRegionH").val(nombreRegion);
    $("#calleH").val(nombreCalle);
    $("#numCalleH").val(numCalle);
    $("#deptoH").val(depto);
    
}

function desactivarBoton(){
	$('#btnContinuar').addClass('disabled');
	$('#btnContinuar').attr('disabled',true);
}

function activarBoton(){
	$('#btnContinuar').removeClass('disabled');
	$('#btnContinuar').attr('disabled',false);
}

function asignarLista() {
	
	if($('#numCalle').val().length >= 1){
		activarBoton();
	}else{
		desactivarBoton();
	}
	
}


