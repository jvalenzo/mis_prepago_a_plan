var verRegion =false;
var verComuna = false;
var verCalle = false;
var verNumero = false;


$( document ).ready(function() {
	var select = $('#selComuna');
	 var options = $('#selComuna > options');
	 	$('option', select).remove();
	 	$('#selComuna').append('<option value="" disabled selected>Seleccione</option>');
	
	//$("#inbound").load("pages/InboundMovil.html");
	$('.jqClean').blur(function(){
	    this.value = (this.value + '').replace(/[\<\%\/\(\)\,\>\"\#\$\!\¡\?\¿\\\:\;\]\[\{\}\*\+\&\°\|\~\=\¬\´]/g, '');
	});
	$('.jqClean').keyup(function(){
	    this.value = (this.value + '').replace(/[\<\%\/\(\)\,\>\"\#\$\!\¡\?\¿\\\:\;\]\[\{\}\*\+\&\°\|\~\=\¬\´]/g, '');
	});
	
	$("#btnContinuar").click(function(e){
		toggleButtonLoader('btnContinuar','id');
		$("#formPaso3").submit();
	});
	
	$("#calle").blur(function() {
    	var element = $(this).val().trim();
    	if(element != ''){
//    		$(this).parent().addClass("iscorrect");
//    		$(this).parent().removeClass("error-input normal");
    		verCalle=true;
    		//habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input normal");
    		 $(this).parent().removeClass("iscorrect");
    		 verCalle=false;
    	}
    });
	
	$("#numCalle").blur(function() {
    	var element = $(this).val().trim();
    	if(element != ''){
//    		$(this).parent().addClass("iscorrect");
//    		$(this).parent().removeClass("error-input normal");
    		verNumero=true;
//    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input normal");
    		 $(this).parent().removeClass("iscorrect");
    		 verNumero=false;
    	}
    });
	
	
	$("#calle").change(function() {
		verComuna=true;
		//habilitaBoton();
    });
});

function ajaxComunas(region){
	var reg1 = region.replace(" ","_");
	reg1 = reg1.replace(" ","_");
	reg1 = normalize(reg1)
	verComuna=false
	if(region != ""){
		
//		$.ajax({
//			url:"msgComAjax",
//			async:true,
//			data:"region="+reg1,
//			success: function(data){
//				var comunas = data.split(",");
//				var listCom = "<option value=\"\">Seleccionar</option>";
//				for(var i=0;i<comunas.length;i++){
//					listCom+="<option value=\""+comunas[i]+"\">"+comunas[i]+"</option>"
//				}
//				//$("#selComuna" ).prop( "disabled", false );
//				$("#selComuna").html(listCom);
//			}
//		});
	}else{
		$("#selComuna" ).prop( "disabled", true );
		
	}
}

var normalize = (function() {
	  var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
	      to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
	      mapping = {};
	 
	  for(var i = 0, j = from.length; i < j; i++ )
	      mapping[ from.charAt( i ) ] = to.charAt( i );
	 
	  return function( str ) {
	      var ret = [];
	      for( var i = 0, j = str.length; i < j; i++ ) {
	          var c = str.charAt( i );
	          if( mapping.hasOwnProperty( str.charAt( i ) ) )
	              ret.push( mapping[ c ] );
	          else
	              ret.push( c );
	      }      
	      return ret.join( '' );
	  }
	 
	})();

function ajaxmsg(comuna, region){
	$.ajax({
		type:"POST",
		url:"msgComAjax.action",
		async:true,
		cache: false,
		data:"comuna="+comuna+"&region="+region,
		success: function(data){
			var obj = jQuery.parseJSON(data.jsonDeliveryComunas);
			
			if(obj.tieneDelivery == "false"){
				if(!$("#mensajeComSinCobertura").is(':visible')){
					$(".jqDetalleDireccion").toggleClass('ocultar');
				}				
			}else{
				$("#msgAjax").html(obj.mensaje);
				if($("#mensajeComSinCobertura").is(':visible')){
					$(".jqDetalleDireccion").toggleClass('ocultar');
				}
			}
			
		}
	});
}



function verificaForm(){
	if(!verRegion){
		$("#selRegion").parent().addClass("error-input normal");
		$("#selRegion").parent().removeClass("iscorrect");
	}
	if(!verComuna){
		$("#selComuna").parent().addClass("error-input normal");
		$("#selComuna").parent().removeClass("iscorrect");
	}
	if(!verCalle){
		$("#calle").parent().addClass("error-input normal");
		$("#calle").parent().removeClass("iscorrect");
	}
	if(!verNumero){
		$("#numCalle").parent().addClass("error-input normal");
		$("#numCalle").parent().removeClass("iscorrect");
	}
}