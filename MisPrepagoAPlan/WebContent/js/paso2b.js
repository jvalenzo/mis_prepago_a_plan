var verCap=false;
var verImei=false;
var verCond = false;

$( document ).ready(function() {
	
	//$("#inbound").load("pages/InboundMovil.html");
	$('.jqClean').blur(function(){
	    this.value = (this.value + '').replace(/[\<\%\/\(\)\,\>\"\#\$\!\¡\?\¿\\\:\;\]\[\{\}\*\+\&\°\|\~\=\¬\´]/g, '');
	});
	$('.jqClean').keyup(function(){
	    this.value = (this.value + '').replace(/[\<\%\/\(\)\,\>\"\#\$\!\¡\?\¿\\\:\;\]\[\{\}\*\+\&\°\|\~\=\¬\´]/g, '');
	});
	
	$("#btnContinuar").click(function(e){
		toggleButtonLoader('btnContinuar','id');
		if(verificaForm()){
			$("#formPaso2B").submit();
		}else{
			$('.jqInputForm').trigger('blur');
			toggleButtonLoader('btnContinuar','id');
		}
	});
	
	$("#codCap").blur(function() {
		this.value = (this.value + '').replace(/[^0-9]/g, '');
    	if($("#codCap").val().length == $("#codCap").attr("maxlength")){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input normal");
    		verCap=true;
    		//habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input normal");
    		 $(this).parent().removeClass("iscorrect");
    		 verNombre=false;
    		 //$("#btnContinuar").addClass("disabled");
    	}
    });
	
	$("#checkPorta").click(function(e){
		verCond = $(this).is(':checked');
		//habilitaBoton()
	});
	
	$("#codCap").keyup(function() {
	    this.value = (this.value + '').replace(/[^0-9]/g, '');
		if($("#codCap").val().length == $("#codCap").attr("maxlength")){
			$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input normal");
    		verCap=true;
    		//habilitaBoton();
			$( "#btnContinuar" ).focus();
		}
	});
});

function verificaForm(){

	try {
		if(!verCap){
			$("#codCap").parent().addClass("error-input normal");
			$("#codCap").parent().removeClass("iscorrect");
		}
		
        $(".jqInputForm").each(function(index) {
            if(!($(this).closest(".form-input").hasClass("iscorrect")) || $(this).val().length == 0){
                throw BreakException;
            }
        });
        formValid=true;
    } catch (e) {
    	formValid=false;
    
    }
    return formValid;
}
function habilitaBoton(){
	if(verCap){
		$("#btnContinuar").removeClass("disabled");		
	}else{
		$("#btnContinuar").addClass("disabled");
	}
}