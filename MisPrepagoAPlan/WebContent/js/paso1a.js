var verRut=false;
var verSerie = false;
var verNumero=false;
var verMail=false;
var verRecapcha=false;
var numSerieIngresado = "";
var numrutIngresado = "";
var numSerievalidado = false;
var captchaDisplay = "";
var responseServicioSerie = "0";

var formValid=false;

function validarNumSerie(){
	 $.ajax({
        type: "POST",
        url: "validarSerie.action",
        cache: false,
        data : {rut:$("#rut").val(), nSerie:$("#nSerie").val()},

        beforeSend: function() {
               $('#loaderNumSerie').show();
           },
        complete: function() {
           	$('#loaderNumSerie').hide();
           },
        success: function (data) {

        	if(data.numSerieValido){
				cambiarEstadoInputNumSerie(0);
			}else{
				responseServicioSerie = data.code;
				if(data.code === "2"){
					cambiarEstadoInputNumSerie(3);
				}else{
					cambiarEstadoInputNumSerie(1);
				}
				
			}
		        
        },
		error: function(jqXHR, textStatus){+
			 console.log("error al validar serie: " + textStatus);
		}
   });   	
}
    	
$( document ).ready(function() {
	    
	//$("#inbound").load("pages/InboundMovil.html");	
	$('.jqClean').blur(function(){
	    this.value = (this.value + '').replace(/[\<\%\/\(\)\,\>\"\#\$\!\¡\?\¿\\\:\;\]\[\{\}\*\+\&\°\|\~\=\¬\´]/g, '');
	});
	$('.jqClean').keyup(function(){
	    this.value = (this.value + '').replace(/[\<\%\/\(\)\,\>\"\#\$\!\¡\?\¿\\\:\;\]\[\{\}\*\+\&\°\|\~\=\¬\´]/g, '');
	});
	
	$('.jqBtnDescuentoPortabilidad').click(function(){
		$('.jqContainer').toggleClass("ocultar");
		$('.jqBtnDescuentoPortabilidadMensaje').removeClass("ocultar");
		$('.jqBtnDescuentoPortabilidadMensaje').addClass("fadeInDown");
	});
	
	$('#rut').keyup(function (){
		this.value = (this.value + '').replace(/[^0-9Kk\.\-]/g, '');
	})
	
    $("#rut").blur(function() {
    	if($("#rut").val() != numrutIngresado){
    		//$("#nSerie").val("");
    		numSerieIngresado = "";
    		cambiarEstadoInputNumSerie(2)
    	}
    	 var element = $(this).val().trim();
    	 var mystring = element.replace(/\./g,'');
    	 var iscorrect = validateRut(mystring);
    	 if(iscorrect){
    		 $(this).parent().addClass("iscorrect");
    		 $(this).parent().removeClass("error-input normal");
    		 verRut=true;
    	 }else{
    		 $(this).parent().addClass("error-input normal");
    		 $(this).parent().removeClass("iscorrect");
    		 verRut=false;
    	 }
    	 $(this).val(formateaRut(element));
    	 verificaForm();
    });
    
    $("#telefono").keyup(function() {
    	this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    
    $("#telefono").blur(function() {
    	this.value = (this.value + '').replace(/[^0-9]/g, '');
    	var element = $(this).val().trim();
    	if(element != '' && element.length == 9 && element.charAt()>=2){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input normal");
    		verNumero=true;
    	}else{
    		$(this).parent().addClass("error-input normal");
    		$(this).parent().removeClass("iscorrect");
    		verNumero=false; 
    	}
    	verificaForm();
    });
    
    $("#nSerie").blur(function() {  
    	var element = $(this).val().trim();

    	var rutSinFormato = $("#rut").val().replace(/[\\.-]/g, '').trim();
		if($("#nSerie").val().trim() === rutSinFormato && rutSinFormato.trim() != ""){
			cambiarEstadoInputNumSerie(4);
			$("#informativoSerie").click();
		}else{
			if(numSerieIngresado != element){
	    		numSerieIngresado = element;
	    		validarNumSerie();
	    	}else{
				if(numSerievalidado){
	    			cambiarEstadoInputNumSerie(0)
	    		}else{
	    			if(responseServicioSerie === "2"){
						cambiarEstadoInputNumSerie(3);
					}else{
						cambiarEstadoInputNumSerie(1);
					}
	    		}
			}
		}
    });
    
	 $("#nSerie").focus(function(){
    	numSerievalidado = $('#nSerie').parent().hasClass("iscorrect");
    	cambiarEstadoInputNumSerie(2);
	});

    
    $('#nSerie').keyup(function (){
        this.value = (this.value + '').replace(/[^0-9aA]/g, '');
    })

    
    $("#email").blur(function() {
    	var element = $(this).val().trim();
    	var ismail = isEmail(element);
    	if(ismail){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input normal");
    		verMail=true;
    	}else{
    		$(this).parent().addClass("error-input normal");
    		 $(this).parent().removeClass("iscorrect");
    		 verMail=false;
    	}
    	verificaForm();
    });
    
    $(".jqbtnContinuarPorta").on("click", function(){
    	captchaDisplay = "porta";
    	$("#btnCaptchaBalancer").click();
    });
});

function verificaForm(){
	try {
        $(".jqInputForm").each(function(index) {
            if(!($(this).closest(".form-input").hasClass("iscorrect")) || $(this).val().length == 0){
                throw BreakException;
            }
        });
        formValid=true;
    } catch (e) {
    	formValid=false;
    
    }
    return formValid;
}  

function onSubmit() {
	if(verificaForm()){
		toggleButtonLoader('btnContinuar','id');
		$("#formPaso1A").submit();
	}else{
		$('.jqInputForm').trigger('blur');
		grecaptcha.reset();
	}
}
	    
function formateaRut(rut) {
	
    var actual = rut.replace(/^0+/, "");
    if (actual != '' && actual.length > 1) {
        var sinPuntos = actual.replace(/\./g, "");
        var actualLimpio = sinPuntos.replace(/-/g, "");
        var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
        var rutPuntos = "";
        var i = 0;
        var j = 1;
        for (i = inicio.length - 1; i >= 0; i--) {
            var letra = inicio.charAt(i);
            rutPuntos = letra + rutPuntos;
            if (j % 3 == 0 && j <= inicio.length - 1) {
                rutPuntos = "." + rutPuntos;
            }
            j++;
        }
        var dv = actualLimpio.substring(actualLimpio.length - 1);
        rutPuntos = rutPuntos + "-" + dv;
    }
    return rutPuntos;
}

function validateRut(_value) {
    if(typeof _value !== 'string') return false;
    var t = parseInt(_value.slice(0,-1), 10), m = 0, s = 1;
    while(t > 0) {
      s = (s + t%10 * (9 - m++%6)) % 11;
      t = Math.floor(t / 10);
    }
    var v = (s > 0) ? (s-1)+'' : 'K';
    return (v.toLowerCase() === _value.slice(-1).toLowerCase());
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function cambiarEstadoInputNumSerie(estado){
	if(estado == 0){
		$("#nSerie").parent().addClass("iscorrect");
		$("#nSerie").parent().removeClass("error-input normal");
		verSerie=true;
	}else if(estado == 1){
		$("#nSerie").parent().addClass("error-input normal");
		 $("#nSerie").parent().removeClass("iscorrect");
		 verSerie=false;
	}else if(estado == 2){
		$("#nSerie").parent().removeClass("error-input normal");
		$("#nSerie").parent().removeClass("error-input bloqueo");
		$("#nSerie").parent().removeClass("error-input rut");
	 	$("#nSerie").parent().removeClass("iscorrect");
	 	verSerie=false;
	}else if(estado == 3){
		//Numero de serie bloqueado
		$("#nSerie").parent().addClass("error-input bloqueo");
	 	$("#nSerie").parent().removeClass("iscorrect");
	 	verSerie=false;
	}else if(estado == 4){
		//Numero de serie bloqueado
		$("#nSerie").parent().addClass("error-input rut");
	 	$("#nSerie").parent().removeClass("iscorrect");
	 	verSerie=false;
	}
}

function onSubmitBalancer(response) {
	if(captchaDisplay === "porta"){
		onSubmit();
	}else if(captchaDisplay === "teAyudo"){
		onSubmitC2C();
	}

}