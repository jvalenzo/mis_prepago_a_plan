var verFront=false;
var verBack=false;

$( document ).ready(function() {
	
	//$("#inbound").load("pages/InboundMovil.html");

	 $("#cedulaFront").change(function (){
		 var sizeByte = this.files[0].size;
		 var nameFile = this.files[0].name;
		 var sizeKb = parseInt(sizeByte / 1024);
		 console.log("sizeKb: "+sizeKb);
		 var sizeMb = 0;
		 if(sizeKb>1024){
			 sizeMb = parseInt(sizeKb / 1024);
		 }
		 
		 if(sizeMb<3){
			 console.log("sizeMb: "+sizeMb+" nameFile: "+nameFile);
			 $("#imgFrontOK").show();
			 $("#imgFrontFail").hide();
			 $("#nameFront").html(nameFile);
			 verFront=true;
			 habilitaBoton()
		 }else{
			 $("#imgFrontFail").show();
			 $("#imgFrontOK").hide();
			 $(this).val(''); 
			 $("#nameFront").html('');
			 verFront=false;
			 habilitaBoton()
		 }
	 });
	 
	 $("#cedulaBack").change(function (){
		 var sizeByte = this.files[0].size;
		 var nameFile = this.files[0].name;
		 var sizeKb = parseInt(sizeByte / 1024);
		 console.log("sizeKb: "+sizeKb);
		 var sizeMb = 0;
		 if(sizeKb>1024){
			 sizeMb = parseInt(sizeKb / 1024);
		 }
		 
		 if(sizeMb<3){
			 console.log("sizeMb: "+sizeMb+" nameFile: "+nameFile);
			 $("#imgBackOK").show();
			 $("#imgBackFail").hide();
			 $("#nameBack").html(nameFile);
			 verBack=true;
			 habilitaBoton()
		 }else{
			 $("#imgBackFail").show();
			 $("#imgBackOK").hide();
			 $(this).val(''); 
			 $("#nameBack").html('');
			 verBack=false;
			 habilitaBoton()
		 }
	 });
	 
	 $("#btnContinuar").click(function(e){
		 	toggleButtonLoader('btnContinuar','id');
	    	e.preventDefault();
	    	if(verificaForm()){
	    		$("#formPaso2A").submit();
	    	}else{
	    		toggleButtonLoader('btnContinuar','id');
	    	}
	    });
});

function habilitaBoton(){
	
	if(verFront&&verBack){
		$("#btnContinuar").removeClass("disabled");		
	}else{
		$("#btnContinuar").addClass("disabled");
	}
}

function verificaForm(){
	if(!verFront){
		$("#imgFrontFail").show();
		$("#imgFrontOK").hide();
	}
	if(!verBack){
		$("#imgBackOK").hide();
		$("#imgBackFail").show();
	}
	
	
	if(verFront&&verBack){
		$("#btnContinuar").removeClass("disabled");
		return true;
	}else{
		$("#btnContinuar").addClass("disabled");
		return false;
	}
}