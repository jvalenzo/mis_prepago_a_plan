var numeroContactoCliente=-1;
var inactividadC2CMostrado;
var t3;
sessionStorage.setItem("inactividadC2CMostradoGTM", false);


function TimeResetInactividad(){
    clearTimeout(t3);
    if(sessionStorage.getItem("inactividadC2CMostrado") != 'true'){
    	t3=setTimeout(inactividadModalC2C, 30000);
    }
    
} 

function inactividadC2C() {
	try{
		if(sessionStorage.getItem("inactividadC2CMostrado") != 'true'){
			// = 1 min
	    	//30000 = 30 seg
	    	//5000 = 5 seg 
	        t3=setTimeout(inactividadModalC2C, 30000);
	        
	        $(this).mousemove(function (e) {
	            TimeResetInactividad();
	        });
	        $(this).keypress(function (e) {
	            TimeResetInactividad();
	        });
	        $(this).mousedown(function (e) {
	            TimeResetInactividad();
	        });
	        
	    }else{
	        clearTimeout(t3);
	    }
	}catch(e){
        clearTimeout(t3);
	}
	
}

function inactividadModalC2C(){
    
    if($(".jqc2cFueraHorario").val() === "false"){
        if(numeroContactoCliente!=-1){
            $('.jqNumContactoCliente').val(numeroContactoCliente);
            $('.jqNumContactoCliente').trigger('blur');
        }
        $("#modalSeleccionFlujo").modal("show");
        sessionStorage.setItem("inactividadC2CMostradoGTM", true);
        pushDatalayerInteraccion("te ayudo", "te llamamos");
        sessionStorage.setItem("inactividadC2CMostrado", true);
        
        clearTimeout(t3);
    }else{
        clearTimeout(t3);
    }                       
}

