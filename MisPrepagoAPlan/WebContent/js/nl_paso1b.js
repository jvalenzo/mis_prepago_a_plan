var verNombre=false;
var verApellidos = false;
var verMercado=false;

$( document ).ready(function() {
	//$("#inbound").load("pages/InboundMovil.html");
	
	if($("#nombreCliente").val() != ''){
		verNombre=true;
	}
	
	if($("#apellidosCliente").val() != ''){
		verApellidos=true;
	}

	$("#nombreCliente").blur(function() {
    	var element = $(this).val().trim();
    	if(element != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input normal");
    		verNombre=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input normal");
    		 $(this).parent().removeClass("iscorrect");
    		 verNombre=false;
    	}
    });
	
	$("#apellidosCliente").blur(function() {
    	var element = $(this).val().trim();
    	if(element != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input normal");
    		verApellidos=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input normal");
    		 $(this).parent().removeClass("iscorrect");
    		 verApellidos=false;
    	}
    });

	
	$("#test1").click(function(e){
		$("#test1").parents('.form-input').addClass("iscorrect");
		$("#test1").parents('.form-input').removeClass("error-input normal");
		verMercado=true;
		habilitaBoton()
	});
	
	$("#test2").click(function(e){
		$("#test1").parents('.form-input').addClass("iscorrect");
		$("#test1").parents('.form-input').removeClass("error-input normal");
		verMercado=true;
		habilitaBoton()
	});
	
	$("#btnContinuar").click(function(e){
		toggleButtonLoader('btnContinuar','id');
    	e.preventDefault();
    	if(verificaForm()){
    		$("#formPaso2A").submit();
    	}else{
    		toggleButtonLoader('btnContinuar','id');
    	}
    });
	
	
	
});

function habilitaBoton(){
	
	if(verNombre && verApellidos){
		$("#btnContinuar").removeClass("disabled");		
	}
}

function verificaForm(){
	if(!verNombre){
		$("#nombreCliente").parent().addClass("error-input normal");
		$("#nombreCliente").parent().removeClass("iscorrect");
	}
	if(!verApellidos){
		$("#apellidosCliente").parent().addClass("error-input normal");
		$("#apellidosCliente").parent().removeClass("iscorrect");
	}
	
	if(verNombre && verApellidos){
		$("#btnContinuar").removeClass("disabled");
		return true;
	}else{
		$("#btnContinuar").addClass("disabled");
		return false;
	}
}